import sys
import os, os.path
import socket
import random
import itertools
from urllib.parse import quote_plus
from twisted.internet import reactor
from twisted.python import threadable; threadable.init(1)
from twisted.logger import Logger
from twisted.internet.threads import deferToThread

import hostplugin.actions as act

import octofuss
from octofuss import desktopcontrol
import octofussd.data.models as om

_ = lambda x:x
mydeferred = deferToThread.__get__
log = Logger()

# Host pool tracking all the known hosts
hostPool = None

tree = None
AVAHI_BROWSE = "/usr/bin/avahi-browse"
WOL = "/usr/bin/wakeonlan"

class HostPool:
    def __init__(self, mock=False):
        self.pool = dict()
        self.clusterAddListeners = []
        self.seenClusters = set()
        if mock:
            self.create = MockHost
        else:
            self.create = Host
        self.mock = mock
        self.loading = False

    def get(self, hostname):
        return self.pool.get(hostname, None)

    def obtain(self, hostname):
        res = self.pool.get(hostname, None)
        if res == None:
            self.pool[hostname] = res = self.create(hostname)
        return res

    def hosts(self, filter_func = None):
        """
        Generate all the hosts in the pool
        """
        if filter:
            return filter(filter_func, iter(self.pool.values()))
        else:
            return iter(self.pool.values())

    def clusters(self):
        return self.seenClusters

    def scan_avahi_hosts(self):
        # temporary method to get a list
        # of avahi-discovered hosts.
        found = set([])
        if os.path.isfile(AVAHI_BROWSE):
            p = os.popen("avahi-browse -tvak 2>&1 | grep _workstation._tcp | awk '{print $4;}' | sort | uniq")
            found = set(p.readlines())
        # obtain will initialize by default with 0 clusters.  If a host is
        # found that is already in a cluster, then obtain will reuse it and no
        # changes will happen.
        return [self.obtain(quote_plus(x.strip())) for x in found]

    def update_status(self):
        self.scan_avahi_hosts();

        for h in self.pool.values():
            h.update_status()

    def registerClusterAddListener(self, listener):
        self.clusterAddListeners.append(listener)

    def notifyClusterAdded(self, name):
        if self.loading: return
        self.seenClusters.add(name)
        for l in self.clusterAddListeners:
            l.notifyClusterAdded(name)
        self.save()

    def notifyClusterRemoved(self, name):
        if self.loading: return
        self.save()

    def load(self):
        self.loading = True
        try:
            if self.mock:
                return self.loadMock()
            else:
                return self.loadReal()
        finally:
            self.loading = False

    def loadReal(self):
        """
        Load the cluster/host configuration.  Return the set of cluster names
        found.
        """
        # load clusters from custom cluster file (clusterssh configuration)
        # if found, don't load the main one
        for file in [
                #os.path.join(os.getenv("HOME"), ".clusters"),
                "/etc/clusters"]:
            if not os.path.exists(file): continue
            for line in open(file):
                spline = line.split()
                if len(spline) == 0: continue
                cname = quote_plus(spline.pop(0))
                self.seenClusters.add(cname)
                for hostname in spline:
                    h = self.obtain(quote_plus(hostname))
                    h.addCluster(cname)
            break

        return self.seenClusters

    def loadMock(self):
        def invent(cname, hname):
            cname = quote_plus(cname)
            h = self.obtain(quote_plus(hname))
            h.addCluster(cname)
            self.seenClusters.add(cname)

        invent("Amici Miei", "antani")
        invent("Amici Miei", "blinda")
        invent("Amici Miei", "supercazzola")
        invent("Amici Miei", "tapioca")

        invent("paperopoli", "pippo")
        invent("paperopoli", "pluto")
        invent("paperopoli", "paperino")
        invent("paperopoli", "basettoni")
        invent("paperopoli", "manetta")
        invent("paperopoli", "localhost")
        invent("paperopoli", "evans.fi.trl")
        invent("paperopoli", "davis.fi.trl")

        invent("Test / Prove", "prova")
        invent("Test / Prove", "test")
        invent("Test / Prove", "testo")
        invent("Test / Prove", "testone")
        for x in range(20): invent("Test / Prove", "computers-%s" % x)

        return self.seenClusters

    def save(self):
        if self.mock:
            filename = "(stderr)"
            file = sys.stderr
            print("SAVED CLUSTER CONFIGURATION", file=file)
        else:
            #filename = os.path.join(os.getenv("HOME"), ".clusters")
            filename = "/etc/clusters"
            # Using a fixed extension is a risk of a symlink attack, but only
            # if we were writing to a world-writable directory; here we are
            # writing to a user's home directory, which is ok unless the user
            # has the home directory world-writable or in /tmp
            file = open(filename+".tmp", "w")

        # Aggregate the cluster configuration
        data = dict()
        for cluster in self.seenClusters:
            data[cluster] = set()
        for name, host in self.pool.items():
            for cluster in host.clusters:
                data[cluster].add(name)

        for cluster, hosts in data.items():
            print(cluster, " ".join(hosts), file=file)

        file.flush()
        if not self.mock:
            # Atomically update the file
            os.rename(filename + ".tmp", filename)


class HostGroup(octofuss.DynamicView):
    def __init__(self, name, predicate, doc="Group of computers"):
        super(HostGroup, self).__init__(name, doc=doc)
        self.predicate = predicate

    def hosts(self):
        """
        Generate a sequence of hosts
        """
        return hostPool.hosts(self.predicate)

    def elements(self):
        return self.hosts()

    def element(self, name):
        host = hostPool.get(name)
        if self.predicate(host):
            return host
        return None

    def _create(self, host, value):
        raise AttributeError("cannot add host %s into group %s" % (host["hostname"], self._name))

    def _delete(self, host):
        raise AttributeError("cannot remove host %s from group %s" % (host["hostname"], self._name))

    def ldelete(self, path):
        if len(path) == 0 or path[0] in self.specials:
            return super(HostGroup, self).ldelete(path)
        host = hostPool.get(path[0])
        if host == None or not self.predicate(host):
            return super(HostGroup, self).ldelete(path)
        if len(path) == 1: return self._delete(host)
        return host.ldelete(path[1:])

    def lcreate(self, path, value):
        if len(path) == 0 or path[0] in self.specials:
            return super(HostGroup, self).lcreate(path, value)
        host = hostPool.get(path[0])
        if host == None:
            return super(HostGroup, self).lcreate(path, value)
        if len(path) == 1: return self._create(host, value)
        return host.lcreate(path[1:], value)

    def _create_new(self, name):
        host = hostPool.obtain(name)
        return self._create(host, None)


def host_is_up(hostname):
    # try a tcp connection to a port that
    # we're almost sure we don't find any
    # service listening on it.
    # if we got a connection refused, is up!
    port = 43
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(0.5)
    try:
        s.connect((hostname, port))
    except Exception as e:
        if e.errno == 111:
            return True
        else:
            return False

class BaseHost(octofuss.ReadonlyDictTree):
    def __init__(self, hostname, clusters = []):
        super(BaseHost, self).__init__(hostname, doc="Host "+hostname)

        self.clusters = set(clusters)

        self.__setitem__('hostname', hostname)
        self.__setitem__('isup', False)
        self.__setitem__('logged_user', None)
        self.__setitem__('screen_locked', False)
        self.__setitem__('net_disabled', False)
        self.__setitem__('last_seen', None)

        self.register(act.HostActions(":actions:", self))

    def pool(self):
        return hostPool

    def available_scripts(self):
        available_scripts = []
        runs = om.ScriptRun.objects.filter(scheduled__isnull=True)
        for r in runs:
            try:
                hosts = om.ScriptRunHost.objects.filter(script_run=r)
                for x in hosts:
                    if x.name == self._name:
                        raise Exception("found")
                # not in this run, can be added
                available_scripts.append(r.name)
            except:
                pass

        return available_scripts

    def addScriptRun(self, name):
        try:
            run = om.ScriptRun.objects.get(name=name)
            om.ScriptRunHost.objects.create(script_run=run, name=self._name)
        except Exception as e:
            print(e)

    def available_upgrades(self):
        available_upgrades = []
        upgrades = om.Upgrade.objects.filter(scheduled__isnull=True)
        for u in upgrades:
            try:
                for x in u.hosts:
                    if x.name == self._name:
                        raise Exception("found")
                # not in this upgrade, can be added
                available_upgrades.append(u.name)
            except:
                pass
        return available_upgrades

    def addUpgradeCampaign(self, name):
        try:
            upgrade = om.Upgrade.objects.get(name=name)
            om.UpgradeHost.objects.create(upgrade=upgrade, name=self._name)
        except Exception as e:
            print(e)

    def addCluster(self, name):
        self.clusters.add(name)
        hostPool.notifyClusterAdded(name)

    def delCluster(self, name):
        self.clusters.remove(name)
        hostPool.notifyClusterRemoved(name)

    def _check_net_disabled(self):
        value = tree.has("/firewall/conf/denied_lan_hosts/" + self._name)
        self.__setitem__("net_disabled", value)
        return value

    def _check_last_seen(self):
        ls = om.ClientActivity.objects.filter(hostname=self._name).order_by("-timestamp")
        if ls.count() > 0:
            value = ls[0].timestamp.ctime()
        else:
            value = "None"
        self.__setitem__("last_seen", value)
        return value

    def setNetworkLock(self, value):
        if value:
            tree.create("/firewall/conf/denied_lan_hosts/" + self._name, _("Blocked by system administrator"))
        else:
            tree.delete("/firewall/conf/denied_lan_hosts/" + self._name)
        tree.set("/services/firewall", "restart")
        self._check_net_disabled()

class Host(BaseHost):
    def _check_logged_user(self):
        self.__setitem__('logged_user', desktopcontrol.logged_user(self.__getitem__('hostname')))
        return self.__getitem__('logged_user')

    def _check_screen_lock(self):
        self.__setitem__('screen_locked', desktopcontrol.screen_is_locked(self.__getitem__('hostname')))
        return self.__getitem__('screen_locked')

    def _check_up(self):
        """Verify if the computer is on or off"""
        self.__setitem__('isup', host_is_up(self.__getitem__('hostname')))
        return self.__getitem__('isup')

    def update_status(self):
        self._check_logged_user()
        self._check_screen_lock()
        self._check_up()
        self._check_net_disabled()
        self._check_last_seen()

    def wakeup(self):
        if os.path.isfile(WOL):
            os.system("%s %s" % (WOL, self['hostname']))

    def shutdown(self):
        os.system("ssh root@%s shutdown -h now" % self['hostname'])

    def setScreenLock(self, value):
        if value:
            desktopcontrol.screen_lock(self['hostname'], "An administrator requires your attention")
        else:
            desktopcontrol.screen_release(self['hostname'])
        self.__setitem__("screen_locked", bool(value))

    def sendMessage(self, message):
        desktopcontrol.popup_message(self['hostname'], message)

    def installPackage(self, package):
        om.InstallQueue.objects.create(hostname=self._name,
                                        package=package,
                                        lastlog=None,
                                        lastlog_time=None)

    def last_seen(self):
        """Get the last boot of the computer"""
        pass

    def __str__(self):
        return self.__getitem__('hostname')

    def __repr__(self):
        return self.__getitem__('hostname')


class MockHost(BaseHost):
    loggedUsers = [None, "antani", "christopher", "enrico", "pippo"]

    def _check_logged_user(self):
        self.__setitem__('logged_user', random.choice(self.loggedUsers))
        return self.__getitem__('logged_user')

    def _check_screen_lock(self):
        self.__setitem__('screen_locked', bool(random.randint(0, 1)))
        return self.__getitem__('screen_locked')

    def _check_up(self):
        """Verify if the computer is on or off"""
        self.__setitem__('isup', bool(random.randint(0, 1)))
        return self.__getitem__('isup')

    def update_status(self):
        self._check_logged_user()
        self._check_screen_lock()
        self._check_up()
        self._check_net_disabled()
        self._check_last_seen()

    def setScreenLock(self, value):
        print((value and "LOCK" or "UNLOCK"), "SCREEN OF", self["hostname"])
        self.__setitem__("screen_locked", bool(value))

    def sendMessage(self, message):
        print("SEND MESSAGE", message, "TO", self["logged_user"], "ON", self["hostname"])

    def installPackage(self, package):
        print("INSTALL PACKAGE", package, "ON", self["hostname"])

    def wakeup(self):
        print("WAKING UP", self['hostname'])

    def shutdown(self):
        print("SHUTDOWN", self["hostname"])

    def last_seen(self):
        """Get the last boot of the computer"""
        pass

    def __repr__(self):
        return self.__getitem__('hostname')

class AvahiComputers(HostGroup):
    def __init__(self, name):
        super(AvahiComputers, self).__init__(name, lambda h: len(h.clusters) == 0, doc="Computers autodiscovered on the network.\n\nThis group lists those computers that are not part of any group.")
        self.specials[":actions:"] = act.ComputersActions(":actions:", self)

class Computers(HostGroup):
    def __init__(self, name):
        super(Computers, self).__init__(name, lambda h: len(h.clusters) != 0, doc="All computers that are part of at least a group.")

class Cluster(HostGroup):
    def __init__(self, name):
        super(Cluster, self).__init__(name, lambda h: name in h.clusters)
        self.specials[":actions:"] = act.ComputersActions(":actions:", self)

    def _create(self, host, value):
        host.addCluster(self._name)

    def _delete(self, host):
        host.delCluster(self._name)

class Clusters(octofuss.Forest):
    def __init__(self, name, clusters = []):
        super(Clusters, self).__init__(name, doc="All existing groups of computers")
        for c in clusters:
            self.register(Cluster(c))
        hostPool.registerClusterAddListener(self)

    def notifyClusterAdded(self, name):
        if name not in self.branches:
            self.register(Cluster(name))

    def ldelete(self, path):
        del(self.branches[path[0]])
        hostPool.notifyClusterRemoved(path[0])

    def lcreate(self, path, value=None):
        if len(path) == 0: return super(Clusters, self).lcreate(path, value)
        if len(path) == 1:
            # Create head here
            hostPool.notifyClusterAdded(path[0])
            tree = self.branches.get(path[0], None)
            if tree == None: raise LookupError("path %s does not exist" % os.path.join(self._name, path[0]))
            return tree.lget([])
        else:
            tree = self.branches.get(path[0], None)
            if tree == None: raise LookupError("path %s does not exist" % os.path.join(self._name, path[0]))
            return tree.lcreate(path[1:], value)

def init(**kw):
    global hostPool, tree

    tree = kw.get("tree", None)
    conf = kw.get("conf", None)
    canMock = conf and conf.getboolean("General", "allowMockup")
    forceMock = conf and conf.getboolean("General", "forceMockup")

    if forceMock:
        hostPool = HostPool(mock=True)
    else:
        hostPool = HostPool(mock=False)
    clusters = hostPool.load()

    computers = Computers("computers")

    if os.path.isfile(AVAHI_BROWSE):
        avahi_computers = AvahiComputers("avahicomputers")
        yield dict(tree = avahi_computers, root = "/")

    @mydeferred
    def update_all_data():
        log.info("Scanning for hosts")
        hostPool.update_status();
        reactor.callLater(60, update_all_data)

    reactor.callLater(1, update_all_data)

    yield dict(tree = computers, root = "/")

    yield dict(tree = Clusters("cluster", clusters), root = "/")
