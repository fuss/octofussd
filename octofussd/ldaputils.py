# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import ldap3 as ldap
import six

class Object(object):
    def __init__(self, dn, attrs=None):
        object.__setattr__(self, "_dn", dn)

        if attrs == None:
            object.__setattr__(self, "_d", dict())
        else:
            object.__setattr__(self, "_d", attrs)

        object.__setattr__(self, "_changes", dict())

    def _clear_changes(self):
        object.__setattr__(self, "_changes", dict())

    # Proxy dict methods

    def __iter__(self):
        return self._d.__iter()

    def iterkeys(self):
        return iter(self._d.keys())

    def itervalues(self):
        return iter(self._d.values())

    def iteritems(self):
        return iter(self._d.items())

    def __len__(self):
        return len(self._d)

    def __getitem__(self, key):
        return self.__getattr__(key)

    def __setitem__(self, key, value):
        return self.__setattr__(key, value)

    def __delitem__(self, key):
        return self.__delattr__(key)

    def __contains__(self, item):
        return self._d.__contains__(item)

    # Also allow to access fields as normal attributes

    def __getattr__(self, name):
        val = self._d.get(name, None)
        if val == None:
            return []
        else:
            return val

    def __setattr__(self, name, value):
        # Set the value in the dictionary, and also keep track of the change in
        # case we do an ldapmodify

        # Convert to a list
        if value is None: raise ValueError("cannot set attribute {} to None".format(name))

        if isinstance(value, six.text_type):
            value = [value]
        elif isinstance(value, six.binary_type):
            value = [value]
        elif not hasattr(value, "__len__"):
            value = [str(value)]
        else:
            value = [str(x) for x in value]

        # Is it an add or a delete/replace?
        if name in self._d:
            if value is None:
                del self._d[name]
                self._changes[name] = (ldap.MODIFY_DELETE, None)
            else:
                self._d[name] = value
                self._changes[name] = (ldap.MODIFY_REPLACE, value)
        else:
            if value is None:
                pass
            else:
                self._d[name] = value
                self._changes[name] = (ldap.MODIFY_ADD, value)

    def __delattr__(self, name):
        if name in self.d:
            del self._d[name]
            self._changes[name] = (ldap.MODIFY_DELETE, None)
            
