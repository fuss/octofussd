#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name='octofussd',
    version='12.0.16',
    description='octofuss server daemon',
    author='FUSS Team',
    author_email='packages@fuss.bz.it',
    url='https://gitlab.fuss.bz.it/fuss/octofussd',
    install_requires=[
        "octofuss",
    ],
    packages=find_packages(exclude=("tests", "tests.*")),
    scripts=[
        'src/octofussd',
        'src/octofussdump',
        'src/ansible-octofuss',
    ],
)
