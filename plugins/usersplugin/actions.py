import octofuss
from urllib.parse import unquote_plus
from . import common

_ = lambda x:x

def _parseBool(value):
    if value == "True" or value == "true": return True
    if value == "False" or value == "false": return False
    return None

class Lister(octofuss.Tree):
    def __init__(self, name, generator):
        super(Lister, self).__init__(name)
        self.generator = generator

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) == 1: return path[0] in self.generator()
        return False

    def lget(self, path):
        if len(path) == 0: return sorted(list(self.generator()))
        if len(path) == 1: return sorted([x for x in self.generator() if x.startswith(path[0])])
        return None

    def llist(self, path):
        if len(path) == 0: return sorted(list(self.generator()))
        if len(path) == 1: return sorted([x for x in self.generator() if x.startswith(path[0])])
        return []


class DisableUserAction(octofuss.Action):
    def __init__(self, name, user, **info):
        self.user = user
        self.info = info
        self.info.update(
            type="nudge",
            label=_("Disable user"),
            reload="users",
            value=user.uid
        )
        super(DisableUserAction, self).__init__(name, self.info)

    def enabled(self):
        return self.user.is_enabled()

    def perform(self, value):
        self.user.disable()

class EnableUserAction(octofuss.Action):
    def __init__(self, name, user, **info):
        self.user = user
        self.info = info
        self.info.update(
            type="nudge",
            label=_("Enable user"),
            reload="users",
            value=user.uid
        )
        super(EnableUserAction, self).__init__(name, self.info)

    def enabled(self):
        if self.user.is_enabled():
            return False
        else:
            return True

    def perform(self, value):
        self.user.enable()

class AddGroupAction(octofuss.Action):
    def __init__(self, name, user, **info):
        self.user = user
        self.info = info
        self.info.update(
            type="choice",
            label=_("Add group"),
            options=[],
        )
        super(AddGroupAction, self).__init__(name, self.info)

    def perform(self, value):
        user, group = common.userDB.add_user_to_group(self.user.uid, value)
        if "docent" in group.cn:
            user.data.ou = "docenti"
            # refs #12213
            user.data.shadowMax = "90"
            user.commit()

    def lget(self, path):
        blacklist = set([x.cn for x in common.userDB.groups_of_user(self.user)])
        res = []
        for g in common.userDB.listGroups():
            if g.cn not in blacklist:
                res.append(g.cn)
        res.sort()
        self.info["options"] = res
        return super(AddGroupAction, self).lget(path)

class DelGroupAction(octofuss.Action):

    def __init__(self, name, group, **info):
        self.group = group
        self.info = info
        self.info.update(
            type="nudge",
            label=_("Delete group"),
            reload="groups",
            value=group.cn
        )
        super(DelGroupAction, self).__init__(name, self.info)

    def perform(self, value):
        value = unquote_plus(value)
        common.userDB.delete_group(value)

    def enabled(self):
        c = 0
        for x in self.group.members.users():
            c += 1
        if c > 0:
            return False
        else:
            return True

class GroupAction(octofuss.Action):
    def __init__(self, name, group, **info):
        super(GroupAction, self).__init__(name, info)
        self.peername = info.get("peername", name)
        self.group = group
    def iterusers(self):
        return self.group.members.users()
    def enabled(self):
        for h in self.iterusers():
            if h.lhas([":actions:", self.peername]):
                return True
        return False
    def perform(self, value):
        for h in list(self.iterusers()):
            if h.lhas([":actions:", self.peername]):
                h.lset([":actions:", self.peername], value)

class AddGroupToAllAction(octofuss.Action):
    def __init__(self, name, group, **info):
        self.group = group
        self.info = info
        self.info.update(
            type="choice",
            label=_("Add group to all"),
            options=[],
        )
        super(AddGroupToAllAction, self).__init__(name, self.info)

    def enabled(self):
        return len(self._options()) > 0

    def perform(self, value):
        for u in list(common.userDB.list(self.group)):
            user, group = common.userDB.add_user_to_group(u, value)
            if "docent" in group.cn:
                user.data.ou = "docenti"
                # refs #12213
                user.data.shadowMax = "90"
                user.commit()

    def lget(self, path):
        self.info["options"] = self._options()
        return super(AddGroupToAllAction, self).lget(path)

    def _options(self):
        # Compute the list of common groups for all users
        intersection = None
        for u in common.userDB.list(self.group):
            groups = set([x.cn for x in common.userDB.groups_of_user(u)])
            if intersection == None:
                intersection = groups
            else:
                intersection.intersection_update(groups)
        if intersection == None:
            intersection = set()

        # Get the list of all groups not in the intersection
        options = [g.cn for g in common.userDB.listGroups() if g.cn not in intersection]
        options.sort()
        return options

class DelGroupFromAllAction(octofuss.Action):
    def __init__(self, name, group, **info):
        self.group = group
        self.info = info
        self.info.update(
            type="choice",
            label=_("Remove group from all"),
            options=[],
        )
        super(DelGroupFromAllAction, self).__init__(name, self.info)

    def enabled(self):
        return len(self._options()) > 0

    def perform(self, value):
        for u in list(common.userDB.list(self.group)):
            common.userDB.delete_user_from_group(u, value)
            if "docent" in value:
                u.ou = "studenti"
                # refs #12213
                u.shadowMax = "90"
                u.commit()

    def lget(self, path):
        self.info["options"] = self._options()
        return super(DelGroupFromAllAction, self).lget(path)

    def _options(self):
        # Compute the union of the group lists of users
        union = set()
        for u in common.userDB.list(self.group):
            union.update([x.cn for x in common.userDB.groups_of_user(u)])
        return sorted(union)

class AddPermToAllAction(octofuss.Action):
    def __init__(self, name, group, **info):
        self.group = group
        self.info = info
        self.info.update(
            type="choice",
            label=_("Add permission to all"),
            options=[],
        )
        super(AddPermToAllAction, self).__init__(name, self.info)

    def enabled(self):
        return common.tree.lhas(["netperms"]) and len(self._options()) > 0

    def perform(self, value):
        for u in list(common.userDB.list(self.group)):
            common.tree.lcreate(["netperms", "byuser", u.uid, value], None)

    def lget(self, path):
        self.info["options"] = self._options()
        return super(AddPermToAllAction, self).lget(path)

    def _options(self):
        # Compute the list of common permissions for all users
        intersection = None
        for u in common.userDB.list(self.group):
            perms = set(common.tree.llist(["netperms", "byuser", u.uid]))
            if intersection == None:
                intersection = perms
            else:
                intersection.intersection_update(perms)
        if intersection == None:
            intersection = set()

        # Get the list of all permissions not in the intersection
        options = [p for p in common.tree.llist(["netperms", "bygroup"]) if p not in intersection]
        options.sort()
        return options

class DelPermFromAllAction(octofuss.Action):
    def __init__(self, name, group, **info):
        self.group = group
        self.info = info
        self.info.update(
            type="choice",
            label=_("Remove permission from all"),
            options=[],
        )
        super(DelPermFromAllAction, self).__init__(name, self.info)

    def enabled(self):
        return common.tree.lhas(["netperms"]) and len(self._options()) > 0

    def perform(self, value):
        for u in list(common.userDB.list(self.group)):
            common.tree.ldelete(["netperms", "byuser", u.uid, value])

    def lget(self, path):
        self.info["options"] = self._options()
        return super(DelPermFromAllAction, self).lget(path)

    def _options(self):
        # Compute the union of the permission lists of users
        union = set()
        for u in common.userDB.list(self.group):
            union.update(common.tree.llist(["netperms", "byuser", u.uid]))
        return sorted(union)

class SetQuotaToAllAction(octofuss.Action):
    def __init__(self, name, fs, group, property, **info):
        self.fs = fs
        self.group = group
        self.property = property
        self.info = info
        self.info.update(
            type="freeform",
            label=_("Set %s quota to all on %s") % (property, self.fs),
            text=_("Value"),
        )
        super(SetQuotaToAllAction, self).__init__(name, self.info)

    def enabled(self):
        return common.tree.lhas(["quota"])

    def perform(self, value):
        for u in list(common.userDB.list(self.group)):
            common.tree.lset(["quota", self.fs, "user", u.uid, self.property], int(value))

class UserActions(octofuss.ActionTree):
    def __init__(self, name, user):
        super(UserActions, self).__init__(name, doc="Actions for user " + user.uid)
        self.user = user
        self.register(AddGroupAction("addgroup", user))
        self.register(DisableUserAction("disable", user))
        self.register(EnableUserAction("enable", user))

class GroupActions(octofuss.ActionTree):
    def __init__(self, name, group):
        super(GroupActions, self).__init__(name, doc="Actions for group " + group.cn)
        self.group = group
        _order = [0]    # I hate this python scoping rule
        def order(_order=_order):
            _order[0] += 1
            return _order[0]
        # Add group to all users
        self.register(AddGroupToAllAction("addgrouptoall", group, order=order()))
        # Remove group from all users
        self.register(DelGroupFromAllAction("delgroupfromall", group, order=order()))
        # Add permission to all users
        self.register(AddPermToAllAction("addpermtoall", group, order=order()))
        # Remove permission from all users
        self.register(DelPermFromAllAction("delpermfromall", group, order=order()))
        # Set quota parameters
        if common.tree.lhas(["quota"]):
            for fs in common.tree.llist(["quota"]):
                self.register(SetQuotaToAllAction("setquotadisksoft"+fs, fs, group, "disk_soft", order=order()))
                self.register(SetQuotaToAllAction("setquotadiskhard"+fs, fs, group, "disk_hard", order=order()))
                self.register(SetQuotaToAllAction("setquotafilesoft"+fs, fs, group, "file_soft", order=order()))
                self.register(SetQuotaToAllAction("setquotafilehard"+fs, fs, group, "file_hard", order=order()))
        # Delete group
        self.register(DelGroupAction("delgroup", group, order=order()))
        # Delete all users from group
        # Delete all users in the group
        self.register(GroupAction("delusers", group,
            label = _("Delete all users in this group"),
            peername = "delete",
            type = "nudge",
            order = order()
        ))
