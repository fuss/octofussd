import octofuss
from octofussd.data.models import Users
import datetime

class UserLog(octofuss.PyTree):
    def __init__(self, name = None):
        super(UserLog, self).__init__(name, doc="Log of changes to the user database")

    def _get(self):
        return None

    def _lastinterval(self, interval):
        date_from = datetime.datetime.utcnow() - datetime.timedelta(seconds=interval)
        res = Users.objects.filter(timestamp__gt=date_from).order_by("-timestamp")
        return [(x.timestamp.isoformat(" "), x.username, x.action) for x in res]

    lastyear = property(lambda x:x._lastinterval(3600*24*365), doc="Last year of changes")
    lastmonth = property(lambda x:x._lastinterval(3600*24*31), doc="Last month of changes")
    lastweek = property(lambda x:x._lastinterval(3600*24*7), doc="Last week of changes")
    lastday = property(lambda x:x._lastinterval(3600*24), doc="Last day of changes")
    lasthour = property(lambda x:x._lastinterval(3600), doc="Last hour of changes")
    lastminute = property(lambda x:x._lastinterval(60), doc="Last minute of changes")

def init(**kw):
    yield dict(
            tree = UserLog("userlog"),
            root = "/"
            )
