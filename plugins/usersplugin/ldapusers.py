import octofuss
import os, os.path
import usersplugin.common as common
import octofussd.ldaputils as oldap
import octofuss.hooks
import octofuss.exceptions
import ldap3 as ldap
import urllib.request, urllib.parse, urllib.error
import subprocess
import time
from ldap3.core.exceptions import LDAPChangeError

_ = lambda x:x

TIMEOUT = 100
FIRSTUID = 10000
FIRSTGID = 10000
LASTUID = 64000
DEFAULT_GROUP = "Domain Users"
MIN_REMOVABLE_GROUP = 1500

class UserDB(common.UserDB):
    def _search(self, base, scope, filter='(objectClass=*)'):
        self._connect()
        try:
            result_data = self.ldap.search(base, filter, search_scope=scope,
                                           dereference_aliases=ldap.DEREF_ALWAYS,
                                           attributes=ldap.ALL_ATTRIBUTES)
        except:
            self._connect()
            result_data = self.ldap.search(base, filter,
                                           search_scope=scope,
                                           dereference_aliases=ldap.DEREF_ALWAYS,
                                           attributes=ldap.ALL_ATTRIBUTES)

        for d in self.ldap.response:
            nd = {}
            for key in d['raw_attributes'].keys():
                nval = [ i.decode() for i in d['raw_attributes'][key] ]
                nd[key] = nval
            yield oldap.Object(d['dn'], attrs=nd)

    def _connect(self):
        if not self.ldap:
            self.ldap = ldap.Connection(self.ldap_server, user=self.conf_binddn,
                                        password=self.conf_bindpw, auto_bind=True, receive_timeout=60)
        if self.ldap.closed:
            self.ldap = ldap.Connection(self.ldap_server, user=self.conf_binddn,
                                        password=self.conf_bindpw, auto_bind=True, receive_timeout=60)

    def __init__(self, tree, conf):
        super(UserDB, self).__init__()
        self.tree = tree
        self.conf_server = None
        self.conf_binddn = None
        self.conf_bindpw = None
        self.conf_basedn = None
        self.ldap = None

        if conf.has_section("LDAP"):
            if conf.has_option("LDAP", "host"):
                self.conf_server = conf.get("LDAP", "host")
            if conf.has_option("LDAP", "binddn"):
                self.conf_binddn = conf.get("LDAP", "binddn")
            if conf.has_option("LDAP", "bindpw"):
                # raw parameter is to avoid
                # configparser interpretation of special
                # characters like '%' used for in-section reference
                self.conf_bindpw = conf.get("LDAP", "bindpw", raw=True)
            if conf.has_option("LDAP", "basedn"):
                self.conf_basedn = conf.get("LDAP", "basedn")

        if not self.conf_server: self.conf_server = 'localhost'
        if not self.conf_binddn: self.conf_binddn = 'cn=admin,dc=octofuss'
        if not self.conf_bindpw: self.conf_bindpw = ''
        if not self.conf_basedn: self.conf_basedn = "dc=octofuss"

        self.ldap_server = ldap.Server(self.conf_server)
        self._connect()

        self.defaultGidNumber = None

        for g in self._search(base="cn={},ou=Groups,{}".format(DEFAULT_GROUP, self.conf_basedn), scope=ldap.BASE):
            self.defaultGidNumber = int(g["gidNumber"][0])
            break
        if self.defaultGidNumber == None:
            raise Error("Default group '%s' not found" % DEFAULT_GROUP)
        self.localSambaSid = None



    def commitModify(self, obj):
        self._connect()
        try:
            self.ldap.modify(obj._dn, obj._changes)
        except LDAPChangeError:
            pass

        obj._clear_changes()

    def list(self, group = False):
        """
        List all users.

        If a group is specified, list all users in that group
        """
        if group:
            # Instantiate the group
            group = self._want_group(group)
            # Iterate all the users that have gidNumber same as the one of the group
            for u in self._search(base="ou=Users,%s" % self.conf_basedn, scope=ldap.LEVEL, filter='(gidNumber=%d)' % int(group.data.gidNumber[0])):
                yield User(u)

            # Iterate all memberUid of the group
            for uid in group.data.memberUid:
                base = "uid={},ou=Users,{}".format(uid, self.conf_basedn)
                res = self._search(base="uid={},ou=Users,{}".format(uid, self.conf_basedn),
                                   scope=ldap.BASE)

                for u in res:
                    yield User(u)
        else:
            base = "ou=Users,%s" % self.conf_basedn
            for u in self._search(base=base, scope=ldap.LEVEL):
                yield User(u)

    def listGroups(self):
        for g in self._search(base="ou=Groups,%s" % self.conf_basedn, scope=ldap.LEVEL):
            yield Group(g)

    def groups_of_user(self, user):
        """
        Generate the Group objects to which the given User object belongs to
        """
        user = self._want_user(user)
        # Primary group first
        pg = self.group_by_gid(user.lget(["gidNumber"]))
        if pg is not None:
            yield pg
        for group in self._search(base="ou=Groups,%s" % self.conf_basedn, scope=ldap.LEVEL,
                                  filter="(memberUid=%s)" % user.uid):
            this = Group(group)
            if this._name != pg._name:
                yield Group(group)


    def user(self, name):
        try:
            users = [x for x in self._search(base="uid={},ou=Users,{}".format(name, self.conf_basedn),
                                             scope=ldap.BASE)]
        except ldap.NO_SUCH_OBJECT:
            return None

        if len(users) == 0:
            return None
        else:
            return User(users[0])

    def group(self, name):
        try:
            groups = [x for x in self._search(base="cn={},ou=Groups,{}".format(name, self.conf_basedn),
                                              scope=ldap.BASE)]
        except ldap.NO_SUCH_OBJECT:
            return None

        if len(groups) == 0:
            return None
        else:
            return Group(groups[0])

    def group_by_gid(self, gid):
        gid = int(gid)
        groups = [x for x in self._search(base="ou=Groups,%s" % self.conf_basedn, scope=ldap.LEVEL,
                     filter="(gidNumber=%d)" % gid)]
        if len(groups) == 0:
            return None
        else:
            return Group(groups[0])

    def user_in_group(self, username, groupname):
        u = self.user(username)
        if u == None: return None
        g = self.group(groupname)
        if g == None: return None
        if g.data.gidNumber == u.data.gidNumber: return u
        if u.uid in g.data.memberUid: return u
        return None

    def group_in_user(self, username, groupname):
        u = self.user(username)
        if u == None: return None
        g = self.group(groupname)
        if g == None: return None
        if g.data.gidNumber == u.data.gidNumber: return g
        if u.uid in g.data.memberUid: return g
        return None

    def add_user_to_group(self, user, group):
        user = self._want_user(user)
        group = self._want_group(group)
        group.data.memberUid = group.data.memberUid + [user.uid]
        common.userDB.commitModify(group.data)
        return user, group

    def delete_user_from_group(self, user, group):

        user = self._want_user(user)
        group = self._want_group(group)
        self._connect()
        if user.uid in group.data.memberUid:
            self.ldap.modify(group.data._dn,
                             {"memberUid": (ldap.MODIFY_DELETE, [user.uid])})

        if user.data.gidNumber == group.data.gidNumber:
            user.data.gidNumber = [self.defaultGidNumber]
            user.data.sambaPrimaryGroupSID = [self._getUserSID(self.defaultGidNumber)]
            user.commit()

    def delete(self, username):
        u = self.user(username)
        if u == None:
            raise KeyError("User %s does not exist" % username)
        homeDir = u.data.homeDirectory[0]
        octofuss.hooks.invoke("delete_user", username, homeDir)
        self._connect()
        self.ldap.delete("uid={},ou=Users,{}".format(username, self.conf_basedn))
        # Delete users from memberUid of the various groups
        mod = (ldap.MODIFY_DELETE, "memberUid", [username])
        for group in self._search(base="ou=Groups,%s" % self.conf_basedn, scope=ldap.LEVEL,
                                  filter="(memberUid={})".format(username)):
            self._connect()
            self.ldap.modify(group._dn, {"memberUid": (ldap.MODIFY_DELETE, [username])})
        if self.tree.has("/netperms/byuser/" + username):
            self.tree.delete("/netperms/byuser/"+username)
        # refs #12205 #539
        # Handle Kerberos
        try:
            # Go from something like "dc=scuola,dc=bzn" to "SCUOLA.BZN"
            realm = ".".join([x.replace("dc=", "").upper() for x in self.conf_basedn.split(",")])
            principal = "{}@{}".format(username, realm)
            # kadmin.local
            # delprinc -force username@realm<Enter>
            s = subprocess.Popen("kadmin.local", stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            s.communicate(bytes("delprinc -force {}\n".format(principal), "UTF-8"))
        except Exception as e:
            print("EXCEPTION WHILE DELETING {} KERBEROS PRINCIPAL:".format(principal), repr(e))
        common.UserLog(username=username, action="deleted")

    def delete_group(self, groupname):
        g = self.group(groupname)
        if g == None:
            raise KeyError("Group %s does not exist" % groupname)
        gid = str(g.get("gidNumber"))
        if int(g.get("gidNumber")) < MIN_REMOVABLE_GROUP:
            raise octofuss.exceptions.DeleteSystemGroupError("Group %s is mandatory, refusing delete" % groupname)
        self._connect()
        self.ldap.delete("cn={},ou=Groups,{}".format(groupname, self.conf_basedn))
        mod = {"gidNumber": (ldap.MODIFY_REPLACE, [self.defaultGidNumber]),
               "sambaPrimgaryGroupSID": (ldap.MODIFY_REPLACE,[self._getUserSID(int(self.defaultGidNumber))])}

        for user in self._search(base="ou=Users,{}".format(self.conf_basedn), scope=ldap.LEVEL,
                                 filter="(gidNumber={})".format(gid)):
            self._connect()
            self.ldap.modify(user._dn, mod)

    def _get_free_uidnumber(self):
        # Retrieve all existing uids
        uids = []
        for u in self._search(base="ou=Users,{}".format(self.conf_basedn), scope=ldap.LEVEL):
            uid = int(u["uidNumber"][0])
            if uid < FIRSTUID: continue
            if uid > LASTUID: continue
            uids.append(uid)
        # Sort them so that we can find gaps
        uids.sort()
        if len(uids) == 0:
            return FIRSTUID
        # Look for the first gap
        last = uids[0]
        for u in uids[1:]:
            if u > last + 1:
                return last + 1
            last = u
        # Else, return the maximum one plus 1
        return uids[-1] + 1

    def _get_free_gidnumber(self):
        # Retrieve all existing gids
        gids = []
        for g in self._search(base="ou=Groups,%s" % self.conf_basedn, scope=ldap.LEVEL):
            gid = int(g["gidNumber"][0])
            if gid < FIRSTGID: continue
            gids.append(gid)
        # Sort them so that we can find gaps
        gids.sort()
        if len(gids) == 0:
            return FIRSTGID
        # Look for the first gap
        last = gids[0]
        for u in gids[1:]:
            if u != last + 1:
                return last + 1
            last = u
        # Else, return the maximum one plus 1
        return gids[-1] + 1

    def _getUserSID(self, uidNumber):
        sidPrefix = self._searchLocalSid()
        sidPostfix = (int(uidNumber)*2) + 1000
        return sidPrefix+"-"+str(sidPostfix)

    def _searchLocalSid(self):
        """ Returns the samba sid of the server"""
        if self.localSambaSid:
            return self.localSambaSid
        for res in self._search(base="%s" % self.conf_basedn, scope=ldap.LEVEL, filter="(sambaDomainName=*)"):
            self.localSambaSid = res["sambaSID"][0]
            break
        return self.localSambaSid

    def new_user(self, value):
        self._connect()
        data = self._fillInNewUser(value)
        user = value["uid"]
        self._connect()
        # refs #13094
        if 'gecos' in data and len(data['gecos']):
            data['gecos'][0] = data['gecos'][0].encode("ascii", "xmlcharrefreplace")
        self.ldap.add(
            "uid={},ou=Users,{}".format(user, self.conf_basedn),
            attributes=data,
        )
        octofuss.hooks.invoke("new_user", user,
            int(data["uidNumber"][0]),
            int(data["gidNumber"][0]),
            data["homeDirectory"][0])

        res = self.user(user)
        if "controllerGroup" in value:
            res.set("controllerGroup", value["controllerGroup"])
        res.set("gidNumber", data['gidNumber'][0])
        common.UserLog(username=user, action="created")
        return res

    def new_group(self, value):
        self._connect()
        data = self._fillInNewGroup(value)
        group = value["cn"]
        self.ldap.add(
            "cn={},ou=Groups,{}".format(group, self.conf_basedn),
            attributes = data,
        )
        return self.group(group)

def _getfirstor(list, default):
    if len(list) > 0: return list[0]
    return default

class User(common.User):
    def commit(self):
        common.userDB.commitModify(self.data)

    def getControllerGroup(self):
        try:
            # Extract the information from the file system
            homedir = self.data.homeDirectory[0]
            info = os.stat(homedir)
            if (info.st_mode & 0o20) == 0:
                # If it is not group writable, then there is no controller group
                return None
            else:
                # Else, get the group name from the gid
                group = common.userDB.group_by_gid(info.st_gid)
                if group == None:
                    return str(info.st_gid)
                else:
                    return group.name
        except OSError:
            return None

    def setControllerGroup(self, value):
        # Call hooks with the numerical group ID so that they will still work if the
        # local Unix user database is not hooked into LDAP
        if value == "":
            octofuss.hooks.invoke("set_controller_group", self.uid, "", self.data.homeDirectory[0], common.userDB.defaultGidNumber)
        else:
            group = common.userDB.group(value)
            if group == None:
                raise KeyError("%s is not a valid group" % value)
            octofuss.hooks.invoke("set_controller_group", self.uid, group.data.gidNumber[0], self.data.homeDirectory[0], common.userDB.defaultGidNumber)

    controllerGroup = property(getControllerGroup, setControllerGroup, doc="FUSS controller group")

class Group(common.Group):
    def commit(self):
        common.userDB.commitModify(self.data)
