import octofuss
from octofussd.data.models import InstallQueue, UpgradeHost, ScriptRunHost

def ser_dt(dt):
    "Serialise a datetime to a string"
    if dt is None: return None
    return dt.strftime("%Y-%m-%d %H:%M:%S")

class Queue(octofuss.Tree):
    def __init__(self, name, doc):
        super(Queue, self).__init__(name, doc=doc)

    def lhas(self, path):
        if len(path) == 0: return True
        status = path[0]
        if status != "all" and status not in self.STATUSMAP: return False
        if len(path) == 1:
            hostname = "*"
        else:
            hostname = path[1]

        qs = self.table.objects.all()

        if status != "all":
            qs = qs.filter(**self.STATUSMAP[status])

        if hostname != "*":
            hq = {self.table_host_name: hostname}
            qs = qs.filter(**hq)

        return qs.count() > 0


    def llist(self, path):
        if len(path) == 0: return ["all"] + list(self.STATUSMAP.keys())
        status = path[0]

        if status != "all" and status not in self.STATUSMAP: return []

        if len(path) == 1:
            hostname = "*"
        else:
            hostname = path[1]

        qs = self.table.objects.all()

        if status != "all":
            qs = qs.filter(**self.STATUSMAP[status])

        if hostname != "*":
            hq = {self.table_host_name: hostname}
            qs = qs.filter(**hq)

        return list([getattr(x, self.table_host_name) for x in qs])

    def lget(self, path):
        if len(path) == 0: return [self._to_dict(x) for x in self.table.objects.all()]
        status = path[0]
        if status != "all" and status not in self.STATUSMAP: return None
        if len(path) == 1:
            hostname = "*"
        else:
            hostname = path[1]

        qs = self.table.objects.all().order_by(self.table_order_by)

        if status != "all":
            qs = qs.filter(**self.STATUSMAP[status])

        if hostname != "*":
            hq = {self.table_host_name: hostname}
            qs = qs.filter(**hq)


        return [self._to_dict(x) for x in qs[:1000]]

class InstallQueueTree(Queue):
    STATUSMAP = dict(
            done = dict(status__isnull=False),
            ok = dict(status=0),
            failed = dict(status__gt=0),
            todo = dict(status__isnull=True),
            )
    STATUSDOC = dict(
            done = "installation has been attempted",
            ok = "installation happened successfully",
            failed = "installation failed",
            todo = "installation has not yet been attempted")

    def __init__(self):
        super(InstallQueueTree, self).__init__("iqueue", "Log of install operations on hosts")
        self.table = InstallQueue
        self.table_name = "install_queue"
        self.table_host_name = "hostname"
        self.table_order_by = "-inserttime"

    def _to_dict(self, iq):
        return dict(
                t = "install",
                hostname = iq.hostname,
                result = iq.status,
                output = iq.lastlog,
                started = ser_dt(iq.inserttime),
                ended = ser_dt(iq.lastlog_time),
                package = iq.package,
        )

    def ldoc(self, path):
        if len(path) == 0: return "Package install queue logs"
        status = path[0]
        if status != "all" and status not in self.STATUSMAP: return None
        if len(path) == 1 or path[1] == "*":
            if status == "all": return "all entries in the install queue log"
            return "all entries in the install queue log where " + self.STATUSDOC[status]
        hostname = path[1]
        if status == "all": return "all entries in the install queue log for host " + hostname
        return "all entries in the install queue log for host " + hostname + " where " + self.STATUSDOC[status]

class UpgradeQueueTree(Queue):
    STATUSMAP = dict(
        done = dict(result__isnull=False),
        ok = dict(result=0),
        failed = dict(result__gt=0),
        todo = dict(result__isnull=True),
        )

    STATUSDOC = dict(
            done = "upgrade has been attempted",
            ok = "upgrade happened successfully",
            failed = "upgrade failed",
            todo = "upgrade has not yet been attempted")

    def __init__(self):
        super(UpgradeQueueTree, self).__init__("uqueue", "Log of upgrade operations on hosts")
        self.table = UpgradeHost
        self.table_name = "upgrade_host"
        self.table_host_name = "name"
        self.table_order_by = "-started"

    def _to_dict(self, iq):
        return dict(
                t = "upgrade",
                hostname = iq.name,
                result = iq.result,
                output = iq.output,
                started = ser_dt(iq.started),
                ended = ser_dt(iq.ended),
                upgrade_name = iq.upgrade.name,
                upgrade_type = iq.upgrade.type,
        )

    def ldoc(self, path):
        if len(path) == 0: return "system upgrade queue logs"
        status = path[0]
        if status != "all" and status not in self.STATUSMAP: return None
        if len(path) == 1 or path[1] == "*":
            if status == "all": return "all entries in the upgrade queue log"
            return "all entries in the upgrade queue log where " + self.STATUSDOC[status]
        hostname = path[1]
        if status == "all": return "all entries in the upgrade queue log for host " + hostname
        return "all entries in the upgrade queue log for host " + hostname + " where " + self.STATUSDOC[status]

class ScriptQueueTree(Queue):
    STATUSMAP = dict(
            done = dict(result__isnull=False),
            ok = dict(result=0),
            failed = dict(result__gt=0),
            todo = dict(result__isnull=True),
            )

    STATUSDOC = dict(
            done = "script run has been attempted",
            ok = "script run happened successfully",
            failed = "script run failed",
            todo = "script run has not yet been attempted")

    def __init__(self):
        super(ScriptQueueTree, self).__init__("uqueue", "Log of script run operations on hosts")
        self.table = ScriptRunHost
        self.table_name = "script_run_host"
        self.table_host_name = "name"
        self.table_order_by = "-started"

    def _to_dict(self, iq):
        return dict(
                t = "script",
                hostname = iq.name,
                result = iq.result,
                output = iq.output,
                started = ser_dt(iq.started),
                ended = ser_dt(iq.ended),
                script_run_name = iq.script_run.name,
                script_name = iq.script_run.script.name,
        )

    def ldoc(self, path):
        if len(path) == 0: return "script run queue logs"
        status = path[0]
        if status != "all" and status not in self.STATUSMAP: return None
        if len(path) == 1 or path[1] == "*":
            if status == "all": return "all entries in the script run queue log"
            return "all entries in the script run queue log where " + self.STATUSDOC[status]
        hostname = path[1]
        if status == "all": return "all entries in the script run queue log for host " + hostname
        return "all entries in the script run queue log for host " + hostname + " where " + self.STATUSDOC[status]


# Plugin implementation
class HostQueue(octofuss.Tree):
    def __init__(self, name = None):
        super(HostQueue, self).__init__(name, doc="Log of operations on hosts")

        self.logs = dict(
            install = InstallQueueTree(),
            upgrade = UpgradeQueueTree(),
            script = ScriptQueueTree(),
        )


    def lhas(self, path):
        if len(path) == 0: return True
        if path[0] == "all":
            for a in list(self.logs.values()):
                if a.lhas(path[1:]): return True
            return False
        action = self.logs.get(path[0], None)
        if action is None: return False
        return action.lhas(path[1:])

    def llist(self, path):
        if len(path) == 0: return ["all"] + list(self.logs.keys())
        if path[0] == "all":
            res = set()
            for a in list(self.logs.values()):
                res.update(a.llist(path[1:]))
            return sorted(res)
        action = self.logs.get(path[0], None)
        if action is None: return []
        return action.llist(path[1:])

    def lget(self, path):
        if len(path) == 0 or path[0] == "all":
            res = []
            for a in list(self.logs.values()):
                res.extend(a.lget(path[1:]))
            return res
        action = self.logs.get(path[0], None)
        if action is None: return None
        return action.lget(path[1:])

    def ldoc(self, path):
        if len(path) == 0 or path[0] == "all":
            res = "logs of host actions:\n\n"
            for a in list(self.logs.values()):
                res += a.ldoc(path[1:]) + "\n"
            return res

        action = self.logs.get(path[0], None)
        if action is None: return None
        return action.ldoc(path[1:])

def init(**kw):
    yield dict(
            tree = HostQueue("hostqueue"),
            root = "/"
            )
