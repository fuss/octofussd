import unittest
from octofuss.exceptions import *
from tests.test_base import OctofussdTestMixin, PREFIX
from urllib.parse import quote_plus

MAXUSER = 5
MAXGROUP = 3

def _username(num):
    if num >= MAXUSER:
        raise ValueError("Attempt to generate username #%d that will not be cleaned" % num)
    return PREFIX+"user"+str(num)

def _groupname(num):
    if num >= MAXGROUP:
        raise ValueError("Attempt to generate groupname #%d that will not be cleaned" % num)
    return PREFIX+"group"+str(num)

class TestUsers(OctofussdTestMixin, unittest.TestCase):

    def setUp(self):
        super().setUp()
        # Cleanup test users and groups
        for i in range(MAXUSER):
            if self.tree.has("/users/users/"+_username(i)):
                self.tree.delete("/users/users/"+_username(i))

        for i in range(MAXGROUP):
            if self.tree.has("/users/groups/"+_groupname(i)):
                self.tree.delete("/users/groups/"+_groupname(i))

    def testCreateUser(self):
        user = _username(0)
        userpath = "/users/users/" + user
        self.assertNotHas(userpath)
        self.tree.create(userpath, {})
        self.assertHas(userpath)
        members = self.tree.list(userpath + "/groups")
        self.assertEqual(len(members), 1)
        group = members[0]
        self.assertHas("/users/groups/" + group)
        self.assertInTree("/users/groups/" + group + "/members/" + user)
        self.assertInTree("/users/users/" + user + "/groups/" + group)
        # Try removing the primary group: it should not work
        self.tree.delete(userpath + "/groups/" + group)
        self.assertInTree("/users/groups/" + group + "/members/" + user)
        self.assertInTree("/users/users/" + user + "/groups/" + group)
        self.tree.delete("/users/groups/" + group + "/members/" + user)
        self.assertInTree("/users/groups/" + group + "/members/" + user)
        self.assertInTree("/users/users/" + user + "/groups/" + group)

    def testCreateGroup(self):
        testuser = "/users/groups/" + _groupname(0)
        self.assertNotHas(testuser)
        self.tree.create(testuser, {})
        self.assertHas(testuser)

    def testAssignUserGroup(self):
        # Create a user and a group
        user = _username(0)
        userpath = "/users/users/" + user
        group = _groupname(0)
        grouppath = "/users/groups/" + group
        self.tree.create(userpath, {})
        self.tree.create(grouppath, {})
        # Ensure that they are not assigned to each other
        self.assertNotInTree(userpath + "/groups/" + group)
        self.assertNotInTree(grouppath + "/members/" + user)
        # Create the group in the user groups/ branch
        self.tree.create(userpath + "/groups/" + group)
        self.assertInTree(userpath + "/groups/" + group)
        self.assertInTree(grouppath + "/members/" + user)
        # Remove the group from the user groups/ branch
        self.tree.delete(userpath + "/groups/" + group)
        self.assertNotInTree(userpath + "/groups/" + group)
        self.assertNotInTree(grouppath + "/members/" + user)
        # Create the user in the group members/ branch
        self.tree.create(grouppath + "/members/" + user)
        self.assertInTree(userpath + "/groups/" + group)
        self.assertInTree(grouppath + "/members/" + user)
        # Remove the user from the group users/ branch
        self.tree.delete(grouppath + "/members/" + user)
        self.assertNotInTree(userpath + "/groups/" + group)
        self.assertNotInTree(grouppath + "/members/" + user)

    def testBadChars(self):
        # Create a user and a group, with spaces in the name
        uid = _username(0) + "2"
        user = quote_plus(uid)
        userpath = "/users/users/" + user
        cn = _groupname(0) + "2"
        group = quote_plus(cn)
        grouppath = "/users/groups/" + group

        if self.tree.has(userpath): self.tree.delete(userpath)
        if self.tree.has(grouppath): self.tree.delete(grouppath)

        self.tree.create(userpath, {"uid": uid})
        self.tree.create(grouppath, {"cn": cn})

        # Check that the name is quoted in the tree but intact in the data
        self.assertInTree(userpath)
        self.assertInTree(grouppath)
        a = self.tree.get(userpath)
        self.assertEqual(a["uid"], uid)
        a = self.tree.get(grouppath)
        self.assertEqual(a["cn"], cn)

        # Create the group in the user groups/ branch
        self.tree.create(userpath + "/groups/" + group)
        self.assertInTree(userpath + "/groups/" + group)
        self.assertInTree(grouppath + "/members/" + user)
        # Remove the group from the user groups/ branch
        self.tree.delete(userpath + "/groups/" + group)
        self.assertNotInTree(userpath + "/groups/" + group)
        self.assertNotInTree(grouppath + "/members/" + user)
        # Create the user in the group members/ branch
        self.tree.create(grouppath + "/members/" + user)
        self.assertInTree(userpath + "/groups/" + group)
        self.assertInTree(grouppath + "/members/" + user)
        # Remove the user from the group users/ branch
        self.tree.delete(grouppath + "/members/" + user)
        self.assertNotInTree(userpath + "/groups/" + group)
        self.assertNotInTree(grouppath + "/members/" + user)

    def testCreateUserButHomeExists(self):
        user = _username(0)
        userpath = "/users/users/" + user
        self.assertNotHas(userpath)
        with self.assertRaises(HomeDirectoryExistsError):
            self.tree.create(userpath, {"homeDirectory": "/home/test-duplicate"})
        self.assertHas(userpath)
        members = self.tree.list(userpath + "/groups")
        self.assertEqual(len(members), 1)
        group = members[0]
        self.assertHas("/users/groups/" + group)
        self.assertInTree("/users/groups/" + group + "/members/" + user)
        self.assertInTree("/users/users/" + user + "/groups/" + group)
        # Try removing the primary group: it should not work
        self.tree.delete(userpath + "/groups/" + group)
        self.assertInTree("/users/groups/" + group + "/members/" + user)
        self.assertInTree("/users/users/" + user + "/groups/" + group)
        self.tree.delete("/users/groups/" + group + "/members/" + user)
        self.assertInTree("/users/groups/" + group + "/members/" + user)
        self.assertInTree("/users/users/" + user + "/groups/" + group)

