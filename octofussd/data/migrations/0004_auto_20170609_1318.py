# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0003_auto_20161123_2303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='installqueue',
            name='status',
            field=models.IntegerField(default=None, null=True),
        ),
    ]
