# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import unittest
import octofussd.ldaputils as oldap

class TestTree(unittest.TestCase):
    def setUp(self):
        self.tdict = dict(
            uid = ["antani"],
            uidNumber = ["12"],
            objectClass = ["top", "posixAccount"],
            cn = ["Antani Blinda"],
            job = ["Ispettore tombale"]
        )
        self.obj = oldap.Object("uid=antani,ou=Users,dc=octofuss", self.tdict)
            
    def testRead(self):
        """
        Test reading from the proxy class
        """
        self.assertEquals(self.obj.uid, ["antani"])
        self.assertEquals(self.obj["uid"], ["antani"])
        self.assertEquals(self.obj.uidNumber, ["12"])
        self.assertEquals(self.obj.objectClass, ["top", "posixAccount"])

    def testModify(self):
        """
        Test modifying attributes
        """
        self.assertEquals(self.obj.uid, ["antani"])
        self.assertEquals(self.obj._changes.get("uid", None), None)
        self.obj.uid = "blinda"
        self.assertEquals(self.obj.uid, ["blinda"])
