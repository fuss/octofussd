import octofuss
import os, os.path, re
from subprocess import Popen, PIPE


class Polygen(octofuss.Tree):
    def __init__(self, name = None):
        super(Polygen, self).__init__(name, doc="Random text generator")
        self.rootdir = '/usr/share/polygen'

    def lhas(self, path):
        pathname = os.path.join(self.rootdir, *path)
        return os.path.isdir(pathname) or os.path.exists(pathname + ".grm")

    def lget(self, path):
        pathname = os.path.join(self.rootdir, *path) + ".grm"
        if not os.path.exists(pathname):
            return False
        else:
            p = Popen(["/usr/games/polygen", pathname], stdout=PIPE).communicate()[0]
            p = p.decode('utf-8').strip()
            return p

    def llist(self, path):
        pathname = os.path.join(self.rootdir, *path)
        if not os.path.isdir(pathname):
            if os.path.isfile(pathname + ".grm"):
                return []
            else:
                raise LookupError("path %s does not exist" % os.path.join(self._name, *path))
        def clean(x):
            if x.endswith(".grm"):
                return x[:-4]
            else:
                return x
        return [clean(f) for f in os.listdir(pathname) if not f.endswith(".o")]

    def ldoc(self, path):
        if len(path) == 0: return super(Polygen, self).ldoc(path)
        pathname = os.path.join(self.rootdir, *path)
        if os.path.isdir(pathname):
            return "Polygen grammars in section "+pathname
        pathname += ".grm"
        if not os.path.exists(pathname):
            return None
        for i in Popen(["/usr/games/polygen", "-S", "I", pathname], stdout=PIPE).communicate()[0].split("\n"):
            mo = re.match(r"^title:\s*(.+?)\s*$", i, re.I)
            if mo:
                return mo.group(1)
        return None



def init(**kw):

    if os.path.isfile("/usr/games/polygen"):
        yield dict(
            tree = Polygen("polygen"),
            root = "/"
            )
