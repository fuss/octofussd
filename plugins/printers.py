import octofuss
from octofussd.data.models import NetworkPrinter
from subprocess import Popen, PIPE


class PrintersTree(octofuss.Tree):
    def lhas(self, path):
        if len(path) == 0: return True

    def _load_hosts(self, queue=None):
        qs = NetworkPrinter.objects.all()
        if queue:
            qs = qs.filter(queue_name=queue)
        return list(set([x.hostname for x in qs]))

    def get_current_queues(self):
        try:
            p = Popen(["/usr/bin/lpstat", "-v"], stdout=PIPE).communicate()[0]
            p = p.decode('utf-8').strip()
            r = []
            for line in p.split("\n"):
                r.append(line.split(" ")[2][:-1])
            return r
        except:
            return []

    def _load_queues(self, hostname):
        qs = NetworkPrinter.objects.filter(hostname=hostname)
        return list([x.queue_name for x in qs])


class PrintersByQueueTree(PrintersTree):

    def lcreate(self, path, value=None):
        obj, created = NetworkPrinter.objects.get_or_create(queue_name=path[1],
                                                            hostname=path[2])
        return obj.hostname

    def ldelete(self, path):
        qs = NetworkPrinter.objects.filter(queue_name=path[1],
                                           hostname=path[2])
        qs.delete()


    def lget(self, path):
        if len(path) == 0: return self.get_current_queues()
        if len(path) == 1:
            return self._load_hosts(path[0])
        return path[-1]

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) == 1:
            return path[0] in self.get_current_queues()
        if len(path) == 2:
            return path[1] in self._load_hosts(path[0])

    def ldoc(self, path):
        return "List configured printers by queue"

    def llist(self, path):
        queues = self.get_current_queues()
        if len(path) == 0:
            return queues

        if len(path) > 1:
            return []

        queue = path[0]
        return self._load_hosts(queue)

class PrinterByHostTree(PrintersTree):
    def lcreate(self, path, value=None):
        if path[2] not in self.get_current_queues():
            return None
        obj, created = NetworkPrinter.objects.get_or_create(hostname=path[1],
                                                            queue_name=path[2])
        return obj.hostname

    def ldelete(self, path):
        qs = NetworkPrinter.objects.filter(queue_name=path[2],
                                           hostname=path[1])
        qs.delete()

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) == 1:
            return path[0] in self._load_hosts()
        if len(path) == 2:
            return path[1] in self._load_queues(path[0])

    def lget(self, path):
        if len(path) == 0: return self._load_hosts()
        if len(path) == 1:
            return self._load_queues(path[0])
        return path[-1]

    def ldoc(self, path):
        return "List configured printers by hostname"

    def llist(self, path):
        hosts = self._load_hosts()
        if len(path) == 0:
            return hosts

        if len(path) > 1:
            return []

        host = path[0]
        return self._load_queues(host)

# Plugin implementation
class Printers(octofuss.Tree):
    def __init__(self, name = None):
        super().__init__(name, doc="Printers queues for LAN clients")

        self.managers = dict(
            byqueue = PrintersByQueueTree(),
            byhost = PrinterByHostTree(),
        )

    def lcreate(self, path, value=None):
        if len(path) <= 2: return None
        if len(path) == 3:
            m = self.managers.get(path[0], None)
            if m is None: return None
            return m.lcreate(path, value)
        return []

    def ldelete(self, path, value=None):
        if len(path) <= 2: return None
        if len(path) == 3:
            m = self.managers.get(path[0], None)
            if m is None: return None
            return m.ldelete(path)
        return []

    def lhas(self, path):
        if len(path) == 0: return True
        action = self.managers.get(path[0], None)
        if action is None: return False
        return action.lhas(path[1:])

    def llist(self, path):
        if len(path) == 0: return list(self.managers.keys())
        action = self.managers.get(path[0], None)
        if action is None: return []
        return action.llist(path[1:])

    def lget(self, path):
        if len(path) == 0:
            res = []
            for a in list(self.managers.values()):
                res.extend(a.lget(path[1:]))
            return res
        action = self.managers.get(path[0], None)
        if action is None: return None
        return action.lget(path[1:])

    def ldoc(self, path):
        if len(path) == 0:
            res = "network printing queues"
            return res

        action = self.managers.get(path[0], None)
        if action is None: return None
        return action.ldoc(path[1:])

def init(**kw):
    yield dict(
            tree = Printers("printers"),
            root = "/"
            )
