import sys
import os
import importlib


def init(conf):
    # Get the database filename from the configuration
    __DB = os.environ.get("OCTOFUSS_DB", None)
    if not __DB:
        __DB = conf.get("DB", "database")

    if not __DB:
        __DB = '/tmp/octofussd.db'

    db_filename = os.path.abspath(__DB)

    DATABASES = {
        'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': db_filename,
        }
        }

    INSTALLED_APPS = (
        'octofussd.data',
        )

    SECRET_KEY = 'REPLACE_ME'

    from django.conf import settings
    settings.configure(DATABASES=DATABASES, INSTALLED_APPS=INSTALLED_APPS,
                       SECRET_KEY=SECRET_KEY)

    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
    from django.core.management import ManagementUtility
    mg = ManagementUtility(['octofussd','migrate'])
    mg.execute()

if __name__ == "__main__":
    init({})
    try:
        from django.core.management import execute_from_command_line
        execute_from_command_line(sys.argv)
    except Exception as e:
        print(e)

        print("There was an error loading django modules. Do you have django installed?")
        sys.exit()


def fillrandom():
    import random
    import datetime
    from octofussd.data import models as m

    hosts = ["host{}".format(x) for x in range(300)]
    packages = ['cappuccino','sl','libreoffice','gimp',
                'polygen','firefox','kikcad']
    perms = ['scanner', 'lpadmin', 'plugdev', 'internet', 'cdrom','audio']

    # netperms

    from usersplugin.mockusers import UserDB
    db = UserDB()
    for u in db.list():
        for p in random.sample(perms, random.randint(0,len(perms))):
            m.UserGroups.objects.create(hostname="*",
                                        groupname=p,
                                        username=u.uid
                                        )


    # INSTALL QUEUE

    for x in range(random.randint(40,100)):
        status = [0,1,2]
        lastlog_time = None
        if status is not None:
            lastlog_time = datetime.datetime.now()
        m.InstallQueue.objects.create(hostname=random.choice(hosts),
                                      package=random.choice(packages),
                                      status=random.choice(status),
                                      lastlog="log data",
                                      lastlog_time=lastlog_time)

    # UPGRADE QUEUE
    for x in range(1,random.randint(3,6)):
        scheduled = random.choice([None, datetime.datetime(2016,12,x,0,0,0)])
        if scheduled:
            completed = random.choice([None, scheduled+datetime.timedelta(days=x)])
        else:
            completed = None


        upgrade = m.Upgrade.objects.create(name="Upgrade n. {}".format(x),
                                           type=random.choice(['upgrade','dist-upgrade']),
                                           notes="Upgrade n. {} as planned".format(x),
                                           scheduled=scheduled,
                                           completed=completed)

        for h in random.sample(hosts, random.randint(10,30)):
            started = random.choice([None, datetime.datetime.now()])
            if started:
                ended = random.choice([None, started+datetime.timedelta(seconds=60*random.randint(1,10))])
            else:
                ended = None
            if ended:
                output = "Upgrade results"
                result = random.choice([0,1])
            else:
                output = ""
                result = None

            m.UpgradeHost.objects.create(upgrade=upgrade,
                                         name=h,
                                         started=started,
                                         ended=ended,
                                         output=output,
                                         result=result)

    # SCRIPT QUEUE
    for s in range(random.randint(3,10)):
        script = m.Scripts.objects.create(name="Script n. {}".format(s),
                                          script="#!/bin/bash\necho {}\n".format(s))
        for r in range(random.randint(2,6)):
            run_scheduled = random.choice([None, datetime.datetime(2016,12,x,0,0,0)])
            if run_scheduled:
                run_completed = random.choice([None, run_scheduled+datetime.timedelta(days=x)])
            else:
                run_completed = None

            run = m.ScriptRun.objects.create(name="Script {} Run {}".format(s,r),
                                             script=script,
                                             notes="Some tests",
                                             scheduled=run_scheduled,
                                             completed=run_completed)

            for h in random.sample(hosts, random.randint(10,30)):
                if run_scheduled:
                    m_started = random.choice([None, run_scheduled+datetime.timedelta(seconds=random.randint(60,60*10))])
                    if m_started:
                        m_ended = random.choice([None, m_started+datetime.timedelta(seconds=random.randint(20,600))])
                        if m_ended:
                            m_output = "Script output"
                            m_result = random.choice([0,1])
                        else:
                            m_output = ""
                            m_result = None
                    else:
                        m_ended = None
                        m_result = None
                        m_output = None

                else:
                    m_started = None
                    m_ended = None
                    m_result = None
                    m_output = None

                m.ScriptRunHost.objects.create(script_run=run,
                                               name=h,
                                               started=m_started,
                                               ended=m_ended,
                                               result=m_result,
                                               output=m_output)
