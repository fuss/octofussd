# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0002_upgradehost_result'),
    ]

    operations = [
        migrations.CreateModel(
            name='NetworkPrinter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('queue_name', models.CharField(max_length=255)),
                ('hostname', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='networkprinter',
            unique_together=set([('queue_name', 'hostname')]),
        ),
    ]
