import os, os.path
from subprocess import Popen, PIPE
import usersplugin.common as common
from octofuss.exceptions import *

_ = lambda x:x

class mockldap(object):
    def __init__(self, defaults, values):
        defaults.update(values)
        for k, v in defaults.items():
            setattr(self, k, v)

    def __getitem__(self, k):
        return getattr(self, k)

class udict(mockldap):
    def __init__(self, values):
        super(udict, self).__init__(dict(
            uid = None,
            objectClass = None,
            sn = None,
            cn = None,
            displayName = None,
            givenName = None,
            uidNumber = None,
            gidNumber = None,
            homeDirectory = None,
            loginShell = None,
            gecos = None,
            ou = None,
            sambaSID = None,
            sambaPwdLastSet = None,
            sambaLogonTime = None,
            sambaLogoffTime = None,
            sambaKickoffTime = None,
            sambaPwdCanChange = None,
            sambaPwdMustChange = None,
            sambaAcctFlags = None,
            sambaLMPassword = None,
            sambaPrimaryGroupSID = None,
            sambaNTPassword = None,
            sambaLogonScript = None,
            sambaHomeDrive = None,
            controllerGroup = None,
            shadowMax = None,
            members = []
        ), values)

class gdict(mockldap):
    def __init__(self, values):
        super(gdict, self).__init__(dict(
            objectClass = None,
            sambaGroupType = None,
            displayName = None,
            cn = None,
            memberUid = None,
            sambaSID = None,
            gidNumber = None,
            description = None,
            members = []
        ), values)


class UserDB(common.UserDB):
    def __init__(self):
        super(UserDB, self).__init__()

        self.defaultGidNumber = 500
        #self.defaultGroupSID = "SIDPREFIX-%d"%self.defaultGidNumber

        import random
        # Make things repeatable (maybe)
        random.seed(1)
        self.users = dict()
        self.groups = dict()
        namesources = [ ("eng/nipponame", 100), ("eng/chiname", 100), ("eng/fernanda", 100), ("ita/nomiemiliani", 1000) ]
        shells = [ "/bin/bash", "/bin/tcsh", "/bin/ksh", "/bin/false", "/bin/halt" ]
        names = set()
        # We need to invoke polygen here for performance, to use -X instead of running it a thousand of times
        for g, q in namesources:
            pathname = "/usr/share/polygen/" + g + ".grm"
            p = Popen(["/usr/games/polygen", "-seed", "1", "-X", str(q), pathname], stdout=PIPE)
            for line in p.stdout:
                names.add(line.decode('utf-8').strip())
        names = list(names)
        names.sort()
        # Generate mock users
        for i, fullname in enumerate(random.sample(names, min(len(names), 1000))):
            uname = fullname.lower().split()
            if len(uname) < 2: continue
            uname = "".join((str(uname[0]), str(uname[1][0])))
            if uname in self.users:
                uname = uname+str(i)
            self.users[uname] = udict(self._fillInNewUser(dict(
                uid = uname,
                gecos = fullname,
                ou = random.choice(["docenti", "studenti"]),
                loginShell = random.choice(shells),
                homeDirectory = "/home/" + uname,
            )))
        # Add a user with spaces in the name
        #self.users["Main Adm"] = udict(self._fillInNewUser(dict(
        #    uid = "Main Adm",
        #    gecos = "Main Administrator",
        #    loginShell = random.choice(shells),
        #    homeDirectory = "/home/mainadm",
        #)))
        allusernames = list(self.users.keys())
        allusernames.sort()
        # Generate mock groups
        groupnames = set()
        for line in Popen(["/usr/games/polygen", "-seed", "1", "-X", "20", "/usr/share/polygen/eng/designpatterns.grm"], stdout=PIPE).stdout:
            groupnames.update(line.decode('utf-8').strip().lower().split())
        groupnames = list(groupnames)
        groupnames.sort()
        for idx, name in enumerate(random.sample(groupnames, min(len(groupnames), random.randint(10,30)))):
            self.groups[name] = gdict(self._fillInNewGroup(dict(
                cn = name,
            )))
            for i in range(random.randint(1,20)):
                self.groups[name].memberUid.append(random.choice(allusernames))
        # Add a group with spaces in the name
        self.groups["Big Deal"] = udict(self._fillInNewGroup(dict(
            cn = "Big Deal",
        )))
        # Assign controller groups
        allgroupnames = [str(x) for x in self.groups.keys()]
        allgroupnames.sort()
        for u in self.users.values():
            if random.randint(0, 2) == 0:
                u.controllerGroup = None
            else:
                u.controllerGroup = random.choice(allgroupnames)

    def list(self, group = False):
        if group:
            g = self._want_group(group)
            gid = int(g.data.gidNumber[0])
            seen = set()
            for u in self.users.values():
                if int(u.gidNumber[0]) == gid:
                    seen.add(u.uid[0])
                    yield User(u)
            for name in g.data.memberUid:
                if name not in seen:
                    yield User(self.users[name])
        else:
            for u in self.users.values():
                yield User(u)

    def listGroups(self):
        for g in self.groups.values():
            yield Group(g)

    def groups_of_user(self, user):
        """
        Generate the Group objects to which the given User object belongs to
        """
        user = self._want_user(user)
        # Primary group first
        pg = self.group_by_gid(user.lget(["gidNumber"]))
        if pg != None: yield pg
        for group in self.groups.values():
            if user.uid in group.memberUid:
                yield Group(group)

    def user(self, name):
        u = self.users.get(name, None)
        if u == None: return None
        return User(u)

    def group(self, name):
        g = self.groups.get(name, None)
        if g == None: return None
        return Group(g)

    def group_by_gid(self, gid):
        gid = int(gid)
        for g in self.groups.values():
            if int(g.gidNumber[0]) == gid:
                return Group(g)
        return None

    def user_in_group(self, username, groupname):
        u = self.users.get(username, None)
        if u == None: return None
        g = self.groups.get(groupname, None)
        if g == None: return None
        if int(u.gidNumber[0]) == int(g.gidNumber[0]): return User(u)
        if username in g.memberUid: return User(u)
        return None

    def group_in_user(self, username, groupname):
        u = self.users.get(username, None)
        if u == None: return None
        g = self.groups.get(groupname, None)
        if g == None: return None
        if int(u.gidNumber[0]) == int(g.gidNumber[0]): return Group(g)
        if username in g.memberUid: return Group(g)
        return None

    def add_user_to_group(self, user, group):
        user = self._want_user(user)
        group = self._want_group(group)
        user_check = self.user_in_group(user.uid, group.cn)
        if not getattr(user_check, "uid", None):
            group.data.memberUid.append(user.uid)
        return user, group

    def delete_user_from_group(self, user, group):
        user = self._want_user(user)
        group = self._want_group(group)

        try:
            group.data.memberUid.remove(user.uid)
        except ValueError:
            # Ignore ValueError in case one tries to remove the
            # primary group
            pass
        if user.data.gidNumber == group.data.gidNumber:
            user.data.gidNumber = [self.defaultGidNumber]
            user.data.sambaPrimaryGroupSID = [self._getUserSID(self.defaultGidNumber)]

    def delete(self, username):
        del self.users[username]
        for g in self.groups.values():
            try:
                g.memberUid.remove(username)
            except ValueError:
                # If the user is not in the list, ignore the
                # error
                pass
        common.UserLog(username=username, action="deleted")

    def delete_group(self, groupname):
        g = self.groups[groupname]
        del self.groups[groupname]
        for u in self.users.values():
            if u.gidNumber == g.gidNumber:
                u.gidNumber = [self.defaultGidNumber]
                u.sambaPrimaryGroupSID = [self._getUserSID(self.defaultGidNumber)]

    def _get_free_uidnumber(self):
        if len(self.users) == 0:
            return 1000
        return int(max([x.uidNumber[0] for x in self.users.values()])) + 1

    def _get_free_gidnumber(self):
        if len(self.groups) == 0:
            return 500
        return int(max([x.gidNumber[0] for x in self.groups.values()])) + 1

    def _getUserSID(self, uidNumber):
        sidPrefix = "SIDPREFIX"
        sidPostfix = (int(uidNumber)*2) + 1000
        return sidPrefix+"-"+str(sidPostfix)

    def new_user(self, value):
        data = self._fillInNewUser(value)
        user = value["uid"]
        self.users[user] = udata = udict(data)

        res = User(udata)
        if "controllerGroup" in value:
            res.set("controllerGroup", value["controllerGroup"])
        common.UserLog(username=user, action="created")

        if data["homeDirectory"][0] == "/home/test-duplicate":
            raise HomeDirectoryExistsError("Directory /home/test-duplicate already exists")

        return res

    def new_group(self, value):
        data = self._fillInNewGroup(value)
        group = value["cn"]
        self.groups[group] = gdata = gdict(data)
        return Group(gdata)

class User(common.User):
    def commit(self):
        pass

    def getControllerGroup(self):
        return self.data.controllerGroup

    def setHomeDirectory(self, value):
        value = os.path.abspath(value)
        validate_home = re.compile('/home/\w+')
        if validate_home.match(value) is None:
            raise ValueError("The home directory must be somewhere inside /home")
        self.data.homeDirectory = [value]
        self.commit()

    def setControllerGroup(self, value):
        if value == "":
            self.data.controllerGroup = value

        if value not in common.userDB.groups:
            raise KeyError("%s is not a valid group" % value)

        self.data.controllerGroup = value
        return value

    controllerGroup = property(getControllerGroup, setControllerGroup, doc="FUSS controller group")

class Group(common.Group):
    def commit(self):
        pass
