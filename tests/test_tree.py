import unittest
from tests.test_base import OctofussdTestMixin
import fnmatch, sys, random

class TestTree(OctofussdTestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        # List of paths that lead to infinite recursion: we do not enter those
        self.stoplist = [
            "users/users/*/groups/*/*/",
            "users/users/*/:actions:/*/user/",
            "users/users/*/:actions:/*/group/",
            "users/groups/*/:actions:/*/group/",
            "users/groups/*/:actions:/*/user/",
            "users/groups/*/members/*/",
            "*/:actions:/addgroup/user/",
            "*/:actions:/addgrouptoall/group/",
            "*/:actions:/delete/user/",
            "*/:actions:/addpermtoall/group/",
            "*/:actions:/addgroup/user/",
            "*/:actions:/delgroup/group/",
            "cluster/*/:actions:/*/:actions:/",
            "avahicomputers/:actions:/*/computers/",
            "avahicomputers/*/:actions:/addcluster/candidates/*/",

            ]
		# List of patterns that every path we enter must match (used only to
		# restrict navigation to problematic items, during debugging
        self.restrictlist = [
            	"users/*",
                "upgrades/*",
                "avahicomputers/*",
            ]
		# If not None, sample this amount of random children if a list gives
		# more than this
		#self.maxchildren = None
        self.maxchildren = 20
        if self.maxchildren != None:
            # If we do random samplings, still we want predictable results
            random.seed(1)

    def _canEnter(self, path):
        path = "/".join(path) + "/"
        for pattern in self.restrictlist:
            if not fnmatch.fnmatch(path, pattern):
                return False
        for pattern in self.stoplist:
            if fnmatch.fnmatch(path, pattern):
                return False
        return True

    def testListHas(self, root=[]):
        """
        Ensure that for each path that can be listed, has returns true
        """
        if not self.tree.lhas(root):
            self.fail("Tree does not have path %s, but it can be listed" % "/".join(root))

        subnodes = self.tree.llist(root)
        if self.maxchildren != None and len(subnodes) > self.maxchildren:
            subnodes = random.sample(subnodes, self.maxchildren)
        for sub in sorted(subnodes):
            if self._canEnter(root + [sub]):
                self.testListHas(root + [sub])

    def testListGet(self, root=[]):
        """
        Ensure that for each path that can be listed, get does not raise an
        exception
        """
        try:
            self.tree.lget(root)
        except Exception as e:
            self.fail("getting %s raised exception %s, but it can be listed" % ("/".join(root), str(e)))

        subnodes = self.tree.llist(root)
        if self.maxchildren != None and len(subnodes) > self.maxchildren:
            subnodes = random.sample(subnodes, self.maxchildren)
        for sub in sorted(subnodes):
            if self._canEnter(root + [sub]):
                self.testListGet(root + [sub])
