import octofuss
from octofuss.treeextra import ListTree
import octofussd.data.models as model
import datetime as dt

def ser_dt(dt):
    "Serialise a datetime to a string"
    if dt is None: return None
    return dt.strftime("%Y-%m-%d %H:%M:%S")

class Scripts(ListTree):
    def __init__(self, name = None):
        super(Scripts, self).__init__(Script, name, doc="""Scripts stored for scheduling

Every child node is a script that is stored in the system. Stored scripts can
be scheduled to be run on several hosts.""")

    def child_data_by_name(self, name):
        try:
            return model.Scripts.objects.get(name=name)
        except model.Scripts.DoesNotExist:
            return None

    def list_child_data(self):
        return model.Scripts.objects.all()

    def create_child_data(self, name):
        obj, created = model.Scripts.objects.get_or_create(name=name)
        return obj

    def delete_child_data(self, data):
        data.delete()

class Script(octofuss.PyTree):
    def __init__(self, data):
        self.data = data
        self.name = self.data.name
        super(Script, self).__init__(self.name, doc="Stored script " + self.name)
        self.runs = ScriptRuns(self.data)

    def _get(self, **kw):
        return {
                "name": self.name,
                "script": self.data.script,
                "runs": self.runs.lget([], **kw)
        }

    def _set_script(self, val):
        self.data.script = val
        self.data.save()

    script = property(
                    lambda self: self.data.script,
                    _set_script,
                    doc="The text of the script itself")

class ScriptRuns(ListTree):
    def __init__(self, data):
        self.data = data
        self.name = self.data.name
        super(ScriptRuns, self).__init__(ScriptRun, self.name, doc="Scheduled runs of the script " + self.name)
        self.data = data

    def child_data_by_name(self, name):
        res = model.ScriptRun.objects.filter(script=self.data, name=name)
        if res.count() == 0:
            return None
        else:
            return res[0]

    def list_child_data(self):
        return model.ScriptRun.objects.filter(script=self.data)

    def create_child_data(self, name):
        obj, created = model.ScriptRun.objects.get_or_create(script=self.data, name=name)
        return obj

    def delete_child_data(self, data):
        data.delete()

class ScriptRun(octofuss.PyTree):
    def __init__(self, data):
        name = data.name
        super(ScriptRun, self).__init__(name, doc="Script run " + name)
        self.data = data
        self.hosts = ScriptRunHosts(data)

    def _get(self, **kw):
        return {
                "name": self.data.name,
                "script": self.data.script.name,
                "notes": self.data.notes,
                "scheduled": ser_dt(self.data.scheduled),
                "completed": ser_dt(self.data.completed),
                "hosts": self.hosts.lget([], **kw)
        }

    def _set_scheduled(self, val):
        if val:
            self.data.scheduled = dt.datetime.utcnow()
        else:
            self.data.scheduled = None
        self.data.save()
        
    scheduled = property(
            lambda self: ser_dt(self.data.scheduled),
            _set_scheduled,
            doc="Scheduling status.\n\nIf the run has been scheduled, it is the timestamp of when it has been scheduled; if the run is still in preparation, it is the empty string. Set to any value to schedule using the current time, set to no value to clear.""")

    completed = property(
            lambda self: ser_dt(self.data.completed),
            lambda self, val: val,
            doc="""Completion time.\n\nIf the run has completed on all hosts, it is the timestamp of the completion time of the last host; if the run has not completed yet, it is the empty string""")

class ScriptRunHosts(ListTree):
    def __init__(self, data):
        super(ScriptRunHosts, self).__init__(ScriptRunHost, data.name, doc="List of hosts in script run " + data.name)
        self.data = data

    def child_data_by_name(self, name):
        res = model.ScriptRunHost.objects.filter(script_run=self.data,
                                                 name=name)
        if res.count() == 0:
            return None
        else:
            return res[0]

    def list_child_data(self):
        return model.ScriptRunHost.objects.filter(script_run=self.data)

    def create_child_data(self, name):
        obj, created =  model.ScriptRunHost.objects.get_or_create(script_run=self.data,
                                                                  name=name)
        return obj

    def delete_child_data(self, data):
        data.delete()

class ScriptRunHost(octofuss.PyTree):
    def __init__(self, data):
        name = data.name
        super(ScriptRunHost, self).__init__(name, doc="Host " + data.name + " in script run " + data.script_run.name)
        self.data = data

    def _get(self, **kw):
        if kw.get("short", False):
            return {
                "name": self.data.name,
                "started": ser_dt(self.data.started),
                "ended": ser_dt(self.data.ended),
                "result": self.data.result,
                "output": self.data.output
                }
        else:
            return {
                "name": self.data.name,
                "started": ser_dt(self.data.started),
                "ended": ser_dt(self.data.ended),
                "result": self.data.result,
                "output": self.data.output
                }

    started = property(
                    lambda self: self.data.started,
                    doc="Time the script run was started")
    ended = property(
                    lambda self: self.data.ended,
                    doc="Time the script run was ended")
    result = property(
                    lambda self: self.data.result,
                    doc="Last return code of the script run process")
    output = property(
                    lambda self: self.data.output,
                    doc="Annotated output of all the processes run during the script run")

def init(**kw):
    yield dict(
            tree = Scripts("scripts"),
            root = "/"
            )
