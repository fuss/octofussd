To build a new package:

* Bump the version *both* in debian/changelog *and* setup.py
* Build the package as documented on
  https://fuss-dev-guide.readthedocs.io/it/latest/pacchetti-e-repository.html
  using debian/rules debsrc to create the source tarball.

To run a local copy for developement:

by default octofussd runs in demo mode (as long as the file
conf/octofuss.conf.d/demo.conf is present), on mock data.
To create a database with random data use:

    eatmydata ./run_server --random-data

followed by ./run_server to actually start the server.

