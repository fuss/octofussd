import datetime
import socket
import os.path
from octofussd.data import models
from twisted.web import xmlrpc

def gather_ssh_key(hostaddr):
    try:
        client_info = socket.gethostbyaddr(hostaddr)[0]
        full_hostname = client_info
        hostname = client_info.split(".")[0]

        if not os.path.isdir("/root/.ssh/"):
            os.mkdir("/root/.ssh")
        os.system("ssh -o StrictHostKeyChecking=accept-new -o HashKnownHosts=no -o ConnectTimeout=1 -o BatchMode=yes %s /bin/true" % hostname)
        os.system("ssh -o StrictHostKeyChecking=accept-new -o HashKnownHosts=no -o ConnectTimeout=1 -o BatchMode=yes %s /bin/true" % full_hostname)
    except:
        # FIXME: how to report this error?
        pass

def octofussd_get_upgrade_todo(fqdn, do_start=False):
    res = []
    for u in models.UpgradeHost.objects.filter(name = fqdn, ended = None):
        if not u.upgrade.scheduled: continue
        if do_start and u.started is None:
            u.started = datetime.datetime.utcnow()
            u.save()
        res.append(dict(
            name = u.upgrade.name,
            type = u.upgrade.type))
    return res

def octofussd_get_script_todo(fqdn, do_start=False):
    res = []
    for u in models.ScriptRunHost.objects.filter(name = fqdn, ended = None):
        if not u.script_run.scheduled: continue
        if do_start and u.started is None:
            u.started = datetime.datetime.utcnow()
            u.save()
        res.append(dict(
            scriptname = u.script_run.script.name,
            name = u.script_run.name,
            script = u.script_run.script.script))
    return res


class FussClientInteraction(xmlrpc.XMLRPC):

    def __init__(self, service, tree):
        xmlrpc.XMLRPC.__init__(self, allowNone=True)
        self.service = service
        self.tree = tree

    def render(self,request):
        self.client = request.getClientIP()
        return xmlrpc.XMLRPC.render(self,request)

    def xmlrpc_client_printers(self, fqdn):
        res = [p.queue_name for p in models.NetworkPrinter.objects.filter(hostname=fqdn)]
        return res

    def xmlrpc_privileges(self):
        # return a dict like
        # { "groupname": ["user1", "user2"]}
        res = {}
        for x in models.UserGroups.objects.all():
            if x.groupname not in res:
                res[x.groupname] = []
            res[x.groupname].append(x.username)

        return res

    def xmlrpc_list_clusters(self):
        return self.tree.llist(["cluster"])

    def xmlrpc_user_request(self, username, hostname, message):
        try:
            models.Request.objects.create(username=username,
                                          hostname=hostname,
                                          message=message)
            return True
        except:
            return False

    def xmlrpc_client_join(self, fqdn, group):
        if group is None or group == "":
            group = "unknown"
        # Create the group if it is missing
        if not self.tree.lhas(["cluster", group]):
            self.tree.lcreate(["cluster", group])
        self.tree.lset(["cluster", group, ":actions:", "create"], fqdn)
        return "ack"

    def xmlrpc_client_ping_fqdn(self,fqdn):
        models.ClientActivity.objects.create(hostname=fqdn)
        gather_ssh_key(fqdn)
        return "ack"

    def xmlrpc_client_ping(self):
        models.ClientActivity.objects.create(hostname=self.client)
        gather_ssh_key(self.client)
        return "ack"

    def xmlrpc_client_components_fqdn(self, fqdn, data):
        for component in list(data.keys()):
            models.ClientComponent.objects.create(hostname=fqdn,
                                                  component=component,
                                                  value = data[component])
            
    def xmlrpc_client_components(self,data):
        for component in list(data.keys()):
            models.ClientComponent.objects.create(hostname=self.client,
                                                  component=component,
                                                  value = data[component])
        return "ack"

    def xmlrpc_install_queue(self, fqdn):
        return [x.package for x in models.InstallQueue.objects.filter(hostname=fqdn, status=None)]


    def xmlrpc_install_log(self,fqdn,package,result,log):
        for s in models.InstallQueue.objects.filter(hostname=fqdn, package=package):
            s.status = int(result)
            s.lastlog_time = datetime.datetime.utcnow()
            s.lastlog = log
            s.save()
        return "ack"

    def xmlrpc_get_upgrade_todo(self, fqdn):
        return octofussd_get_upgrade_todo(fqdn, do_start=True)

    def xmlrpc_post_upgrade_results(self, fqdn, res):
        """
        Report the results of one upgrade.

        res is a dict with the result
        """
        name = res.get("name", None)
        ended = res.get("ended", None)
        result = res.get("result", None)
        output = res.get("output", None)

        if name is None: return "name does not exists"
        if ended is None: return "missing end date"
        ended = datetime.datetime.utcfromtimestamp(float(ended))
        if result is None: return "missing upgrade result"
        result = int(result)
        if output is None: return "missing upgrade output"

        try:
            upg = models.Upgrade.objects.get(name=name)
        except sqlobject.SQLObjectNotFound:
            return "upgrade does not exist"
        u = list(models.UpgradeHost.objects.filter(upgrade = upg, name = fqdn))
        if len(u) == 0: return "upgrade for this host does not exist"
        u = u[0]

        if u.ended is None:
            u.ended = ended
            u.result = result
            u.output = output.data
            u.save()
            if upg.completed is None and models.UpgradeHost.objects.filter(upgrade = upg, ended = None).count() == 0:
                upg.completed = datetime.datetime.utcnow()

        return "ack"

    def xmlrpc_get_script_todo(self, fqdn):
        return octofussd_get_script_todo(fqdn, do_start=True)

    def xmlrpc_post_script_results(self, fqdn, res):
        """
        Report the results of one script run.

        res is a dict with the result
        """

        script = res.get("script", None)
        name = res.get("name", None)
        ended = res.get("ended", None)
        result = res.get("result", None)
        output = res.get("output", None)

        if script is None: return "script name does not exists"
        if name is None: return "run name does not exists"
        if ended is None: return "missing end date"
        ended = datetime.datetime.utcfromtimestamp(float(ended))
        if result is None: return "missing script result"
        result = int(result)
        if output is None: return "missing script output"

        try:
            s = models.Scripts.objects.get(name=script)
        except sqlobject.SQLObjectNotFound:
            return "script does not exist"

        scriptrun = list(models.ScriptRun.objects.filter(script = s, name = name))
        if len(scriptrun) == 0: return "script run does not exist"
        scriptrun = scriptrun[0]

        host = list(models.ScriptRunHost.objects.filter(script_run = scriptrun, name = fqdn))
        if len(host) == 0: return "script run for this host does not exist"
        host = host[0]

        if host.ended is None:
            host.ended = ended
            host.result = result
            host.output = output.data
            host.save()

            if scriptrun.completed is None and models.ScriptRunHost.objects.filter(script_run = scriptrun, ended = None).count() == 0:
                scriptrun.completed = datetime.datetime.utcnow()
                scriptrun.save()
        return "ack"
