import octofuss
import time
import datetime
import os, os.path
import re, netaddr
import dns.resolver, dns.tsigkeyring, dns.update, dns.query
from isc_dhcp_leases import Lease, IscDhcpLeases
from twisted.internet.threads import deferToThread
from twisted.logger import Logger

log=Logger()

mydeferred = deferToThread.__get__

LEASE_FILES = ['/var/lib/dhcp/dhcpd.leases', '/tmp/dhcpd.leases']

STATIC_LEASE_FILE = '/etc/dhcp/dhcp-reservations'

DDNS_CONF_FILE = '/etc/bind/named.conf.local'

class DhcpLeases(octofuss.Tree):
    def __init__(self, name = None):
        super(DhcpLeases, self).__init__(name, doc="DHCP Leases")
        self.leases = {}
        self.load_conf()

    def load_conf(self):
        filename = None
        for x in LEASE_FILES:
            if os.path.isfile(x):
                filename = x
                break

        leases = IscDhcpLeases(filename)
        for i in leases.get_current().keys():
            rdesc = {}
            rdesc['ip'] = leases.get_current()[i].ip
            rdesc['mac'] = leases.get_current()[i].ethernet
            if leases.get_current()[i].hostname:
                rdesc['hostname'] = leases.get_current()[i].hostname
            rdesc['start'] = str(leases.get_current()[i].start)
            rdesc['end'] = str(leases.get_current()[i].end)
            rdesc['ended'] = leases.get_current()[i].valid

            self.leases[rdesc['ip']] = rdesc

    def llist(self, path):
        self.load_conf()
        if not path:
            k = list(self.leases.keys())
            k.sort()
            return k
        else:
            if len(path) == 1:
                if path[0] in self.leases:
                    return list(self.leases[path[0]].keys())
            if len(path) == 2:
                return []

    def lhas(self, path):
        self.load_conf()
        if len(path) == 1:
            if path[0] in self.leases:
                return True
        if len(path) == 2:
            if path[0] in self.leases:
                if path[1] in self.leases[path[0]]:
                    return True

    def lget(self, path):
        self.load_conf()
        if len(path) == 0:
            return self.leases
        if len(path) == 1:
            if path[0] in self.leases:
                return self.leases[path[0]]
        if len(path) == 2:
            if path[0] in self.leases:
                if path[1] in self.leases[path[0]]:
                    return self.leases[path[0]][path[1]]

class MockDhcpLeases(DhcpLeases):
    def load_conf(self):
        import random
        for x in range(random.randint(5,20)):
            ip = "10.0.0.{}".format(x)

            lease = dict()
            mac = ":".join([str(hex(random.randint(0,255)))[2:] for i in range(6)])
            lease['mac'] = mac
            lease['hostname'] = 'host{}'.format(x)
            start_lease = datetime.datetime.now() - datetime.timedelta(seconds=random.randint(400,18000))
            lease['start'] = str(start_lease)
            lease['end'] = str(start_lease + datetime.timedelta(seconds=random.randint(800,40000)))
            lease['ended'] = lease['end']

            self.leases[ip] = lease


class DhcpStaticReservation(octofuss.Tree):
    def __init__(self, name=None):
        super(DhcpStaticReservation, self).__init__(name, doc="DHCP Static Reservation")
        self.items = {}
        self.load_conf()
        # parse bind file for DDNS data
        data=open(DDNS_CONF_FILE).read()
        regexp=r'^include "(.*key)";'
        reg=re.compile(regexp,re.M|re.S)
        keyfile=open(reg.findall(data)[0]).read()
        # get key data from key file
        regexp=r'^key "([^"]+)"\s+{.+secret\s+([^ ;]+);'
        reg=re.compile(regexp,re.M|re.S)
        keydata=reg.findall(keyfile)
        key = { keydata[0][0]: keydata[0][1] }
        self.keyring=dns.tsigkeyring.from_text(key)
        # direct zone
        regexp=r'^zone "([a-z0-9.]+)"'
        reg=re.compile(regexp,re.M)
        self.zone=reg.findall(data)[0]
        # reverse zone
        regexp=r'^zone "([0-9.]+in-addr.arpa)"'
        reg=re.compile(regexp,re.M)
        self.reverse=reg.findall(data)[0]

    def parse_item(self, item_row):
        r = dict()
        hostname, content = item_row.split("{")
        lines = content.split(";")
        r['hostname'] = hostname.strip()
        for line in lines:
            if "hardware ethernet" in line:
                r['mac'] = line.split(" ")[-1].strip()
            if "fixed-address" in line:
                r['address'] = line.split(" ")[-1].strip()
        return r

    def load_conf(self):
        # parse static lease file
        raw_data = open(STATIC_LEASE_FILE).readlines()
        data = "".join([x for x in raw_data if not x.startswith("#")])
        self.items = {}
        for d in data.split("host"):
            d = d.strip()
            if d:
                item = self.parse_item(d)
                self.items[item['hostname']] = item


    def build_item(self, item):
        return "host {} {{\n\thardware ethernet {};\n\tfixed-address {};\n}}\n".format(
            item.get("hostname"),
            item.get("mac"),
            item.get("address")
            )

    def get_new_content(self):
        r = ""
        for k,v in self.items.items():
            item = self.build_item(v)
            r += item
        return r

    def save_conf(self):
        r = open(STATIC_LEASE_FILE, "w")
        r.write(self.get_new_content())
        r.close()
        dns.query.tcp(self.update,'127.0.0.1')
        dns.query.tcp(self.updaterev,'127.0.0.1')

    def llist(self, path):
        self.load_conf()
        if not path:
            k = list(self.items.keys())
            k.sort()
            return k
        else:
            if len(path) == 1:
                if path[0] in self.items:
                    return list(self.items[path[0]].keys())
            if len(path) == 2:
                return []

    def lhas(self, path):
        self.load_conf()
        if len(path) == 1:
            if path[0] in self.items:
                return True
        if len(path) == 2:
            if path[0] in self.items:
                if path[1] in self.items[path[0]]:
                    return True

    def lget(self, path):
        self.load_conf()
        if len(path) == 0:
            return self.items
        if len(path) == 1:
            if path[0] in self.items:
                return self.items[path[0]]
        if len(path) == 2:
            if path[0] in self.items:
                if path[1] in self.items[path[0]]:
                    return self.items[path[0]][path[1]]

    def lset(self, path, value):
        self.load_conf()
        if len(path) != 2: return super(DhcpStaticReservation, self).lset(path, value)
        self.items[path[0]][path[1]] = value
        self.save_conf()

    def lcreate(self, path, value):
        self.load_conf()
        if len(path) != 1: return super(DhcpStaticReservation, self).lset(path, value)
        if value and isinstance(value, dict):
            new_val = dict(hostname=path[0])
            new_val['mac'] = value.get("mac", "")
            new_val['address'] = value.get("address", "")
            self.items[path[0]] = new_val

        else:
            self.items[path[0]] = dict(hostname=path[0],
                                      mac="",
                                      address="")
        # adding update dnspython
        self.update=dns.update.Update(self.zone,keyring=self.keyring)
        self.updaterev=dns.update.Update(self.reverse,keyring=self.keyring)
        host=self.items[path[0]]['hostname']
        address=self.items[path[0]]['address']
        self.update.add(host, 604800, 'A', address)
        self.updaterev.add(netaddr.IPAddress(address).reverse_dns, 604800, 'PTR', host+"."+self.zone+".")
        self.save_conf()
        return self.items[path[0]]

    # refs #244
    def ldelete(self, path):
        self.load_conf()
        if isinstance(path, list) and len(path) == 1 and path[0] in self.items:
            # adding update dnspython
            self.update=dns.update.Update(self.zone,keyring=self.keyring)
            self.updaterev=dns.update.Update(self.reverse,keyring=self.keyring)
            host=self.items[path[0]]['hostname']
            address=self.items[path[0]]['address']
            self.update.delete(host, 'A')
            self.updaterev.delete(netaddr.IPAddress(address).reverse_dns, 'PTR')
            del self.items[path[0]]
            self.save_conf()


class MockDhcpStaticReservation(DhcpStaticReservation):
    def gen_mac(self):
        import random
        values = []
        for x in range(6):
            values.append(hex(random.randint(0,255))[2:])
        return ":".join(values)

    def save_conf(self):
        pass

    def load_conf(self):
        if not self.items:
            for x in range(5):
                hostname = "client{}".format(x)
                self.items[hostname] = dict(
                    hostname=hostname,
                    mac=self.gen_mac(),
                    address="{}.domain".format(hostname)
                    )

def init(**kw):
    conf = kw.get("conf", None)
    forceMock = conf and conf.getboolean("General", "forceMockup")

    # lease plugin
    leases = None
    if forceMock:
        leases = MockDhcpLeases("leases")

    else:
        filename = None
        for x in LEASE_FILES:
            if os.path.isfile(x):
                filename = x
                break

        if filename is not None:
            leases = DhcpLeases("leases")

    if leases:
        yield dict(
            tree = leases,
            root = '/dhcp'
            )

    # static reservation plugin
    statics = None
    if forceMock:
        statics = MockDhcpStaticReservation("static")
    else:
        if os.path.isfile(STATIC_LEASE_FILE):
            statics = DhcpStaticReservation("static")

    if statics:
        yield dict(
            tree = statics,
            root = '/dhcp'
            )
