import octofuss
from octofussd.data.models import UserGroups

class Netperms(octofuss.Tree):
    def __init__(self, conf, name = "bygroup"):
        if name == "byuser":
            doc = "FUSS permissions displayed by user name"
        else:
            doc = "FUSS permissions displayed by permission name"

        super(Netperms, self).__init__(name, doc=doc)
        self.groups = set([v for k, v in conf.items("GROUPS")])

    def _hasGroup(self, name):
        return name in self.groups or UserGroups.objects.filter(groupname=name).count() > 0

    def impl_has(self, user=None, group=None):
        try:
            if group != None:
                if user != None:
                    x = UserGroups.objects.filter(username=user, groupname=group)[0]
                    return True
                else:
                    return self._hasGroup(group)
            else:
                if user != None:
                    return True
                else:
                    return True
        except:
            return False

    def impl_get(self, user=None, group=None):
        try:
            if group != None:
                if user != None:
                    return dict(hostname=[x.hostname for x in UserGroups.objects.filter(username=user, groupname=group)])
                else:
                    res = dict()
                    for x in UserGroups.objects.filter(groupname=group):
                        if x.username not in res:
                            res[x.username] = dict(hostname=[x.hostname])
                        else:
                            res[x.username]["hostname"].append(x.hostname)
                    return list(res.items())
            else:
                if user != None:
                    res = dict()
                    for x in UserGroups.objects.filter(username=user):
                        if x.groupname not in res:
                            res[x.groupname] = dict(hostname=[x.hostname])
                        else:
                            res[x.groupname]["hostname"].append(x.hostname)
                    return list(res.items())
                else:
                    return None
        except:
            return None

    def impl_list(self, user=None, group=None, topGroups=True):
        """
        Implement the tree list function.

        topGroups defines what to list if neither user nor group is
        specified.  If True, list the groups, else list the users
        """
        if group != None:
            if user != None:
                return []
            else:
                qs = UserGroups.objects.filter(groupname=group).values("username").order_by("username")
                return [x['username'] for x in qs]
        else:
            if user != None:
                qs = UserGroups.objects.filter(username=user).values("groupname").order_by("groupname")
                return [x['groupname'] for x in qs]
            else:
                if topGroups:
                    return list(self.groups)
                else:
                    qs = UserGroups.objects.all().values("username").order_by("username").distinct()
                    return [x['username'] for x in qs]


    def impl_set(self, user, group, value):
        if not value:
            hostnames = set(["*"])
        else:
            hostnames = set(value.get("hostname", ["*"]))
        for u in UserGroups.objects.filter(username=user, groupname=group):
            if u.hostname in hostnames:
                hostnames.discard(u.hostname)
            else:
                # Delete removed entries
                u.destroySelf()
        # Add new entries
        for h in hostnames:
            UserGroups.objects.create(hostname=h, username=user, groupname=group)

    def impl_delete(self, user=None, group=None):
        if group != None:
            if user != None:
                UserGroups.objects.filter(groupname=group, username=user).delete()
            else:
                # refs #12212 #404 - Add forgotten .delete()
                UserGroups.objects.filter(groupname=group).delete()
        else:
            if user != None:
                # refs #12212 #404 - Add forgotten .delete()
                UserGroups.objects.filter(username=user).delete()
            else:
                pass

    def impl_doc(self, user=None, group=None):
        if group != None:
            if user != None:
                return "Value of permission %s for user %s.\n\nThis is normally '*'" % (group, user)
            else:
                return "Users with permission %s" % group
        else:
            if user != None:
                return "Permissions for user %s" % user
            else:
                return self.documentation


class ByGroup(Netperms):
    def lhas(self, path):
        if len(path) == 0:
            return self.impl_has()
        elif len(path) == 1:
            return self.impl_has(group=path[0])
        elif len(path) == 2:
            return self.impl_has(group=path[0], user=path[1])
        else:
            return False

    def lget(self, path):
        if len(path) == 0:
            return self.impl_get()
        elif len(path) == 1:
            return self.impl_get(group=path[0])
        elif len(path) == 2:
            return self.impl_get(group=path[0], user=path[1])
        else:
            return None

    def lset(self, path, value):
        if len(path) != 2 or not self._hasGroup(path[0]):
            return super(ByGroup, self).lset(path, value)
        return self.impl_set(group=path[0], user=path[1], value=value)

    def lcreate(self, path, value=None):
        if len(path) != 2 or not self._hasGroup(path[0]):
            return super(ByGroup, self).lset(path, value)
        return self.impl_set(group=path[0], user=path[1], value=value)

    def ldelete(self, path):
        if len(path) == 0:
            return self.impl_delete()
        elif len(path) == 1:
            return self.impl_delete(group=path[0])
        elif len(path) == 2:
            return self.impl_delete(group=path[0], user=path[1])
        else:
            return None

    def llist(self, path):
        if len(path) == 0:
            return self.impl_list(topGroups=True)
        elif len(path) == 1:
            return self.impl_list(group=path[0])
        elif len(path) == 2:
            return self.impl_list(group=path[0], user=path[1])
        else:
            return []

    def ldoc(self, path):
        if len(path) == 0:
            return self.impl_doc()
        elif len(path) == 1:
            return self.impl_doc(group=path[0])
        elif len(path) == 2:
            return self.impl_doc(group=path[0], user=path[1])
        else:
            return None


class ByUser(Netperms):
    def lhas(self, path):
        if len(path) == 0:
            return self.impl_has()
        elif len(path) == 1:
            return self.impl_has(user=path[0])
        elif len(path) == 2:
            return self.impl_has(user=path[0], group=path[1])
        else:
            return False

    def lget(self, path):
        if len(path) == 0:
            return self.impl_get()
        elif len(path) == 1:
            return self.impl_get(user=path[0])
        elif len(path) == 2:
            return self.impl_get(user=path[0], group=path[1])
        else:
            return None

    def lset(self, path, value):
        if len(path) != 2 or not self._hasGroup(path[1]):
            return super(ByUser, self).lset(path, value)
        return self.impl_set(user=path[0], group=path[1], value=value)

    def lcreate(self, path, value=None):
        if len(path) != 2 or not self._hasGroup(path[1]):
            return super(ByUser, self).lset(path, value)
        return self.impl_set(user=path[0], group=path[1], value=value)

    def ldelete(self, path):
        if len(path) == 0:
            return self.impl_delete()
        elif len(path) == 1:
            return self.impl_delete(user=path[0])
        elif len(path) == 2:
            return self.impl_delete(user=path[0], group=path[1])
        else:
            return None

    def llist(self, path):
        if len(path) == 0:
            return self.impl_list(topGroups=False)
        elif len(path) == 1:
            return self.impl_list(user=path[0])
        elif len(path) == 2:
            return self.impl_list(user=path[0], group=path[1])
        else:
            return []

    def ldoc(self, path):
        if len(path) == 0:
            return self.impl_doc()
        elif len(path) == 1:
            return self.impl_doc(user=path[0])
        elif len(path) == 2:
            return self.impl_doc(user=path[0], group=path[1])
        else:
            return None


def init(**kw):
    conf = kw.get("conf", None)

    top = octofuss.Forest("netperms", doc="FUSS network permissions")

    top.register(ByUser(conf, "byuser"))
    top.register(ByGroup(conf, "bygroup"))

    yield dict(tree = top, root = "/")
