import os
import subprocess
import time
from collections import deque
from twisted.internet import reactor
from twisted.python import threadable; threadable.init(1)
from twisted.logger import Logger
from twisted.internet.threads import deferToThread
import octofuss
from django.db.models import Max
import octofussd.data.models as om

defer = deferToThread.__get__
log = Logger()

class ServerInfo(octofuss.Tree):
    keyword_doc = {
        "organization": "Organization configured in octofussd",
        "kernel_version": "Current kernel version",
        "ram_mbyte": "Total ram size in megabytes",
        "last_seen_clients": "Last client connected",
        "filesystems": "filesystem usage",
        "load_avg": "Load average"
    }

    def __init__(self, name = None, **kw):
        super(ServerInfo, self).__init__(name, doc="Server information")
        self.conf = kw.get("conf", None)
        self.data = {}
        self.load_deque = deque([], maxlen=20)
        if self.conf:
            if self.conf.has_option("General", "organization"):
                self.data['organization'] = self.conf.get("General", "organization")
            else:
                self.data['organization'] = "NOT CONFIGURED"

        self.data['kernel_version'] = open("/proc/version").readline().split()[2].strip()
        self.data['ram_mbyte'] = int(int(open("/proc/meminfo").readline().split()[1].strip())/1024)

        ls_data = om.ClientActivity.objects.values("hostname").annotate(last=Max("timestamp"))[:10]
        self.data['last_seen_clients'] = [(x.get("hostname", "Unknown"), x.get("last").strftime("%Y-%m-%d %H:%M")) for x in ls_data]
        self.data['filesystems'] = {}
        self.data['load_avg'] = []

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) > 1: return False
        return path[0] in self.data

    def lget(self, path):
        if len(path) == 0: return self.data
        if len(path) > 1: return super(ServerInfo, self).lget(path)
        return self.data[path[0]]

    def llist(self, path):
        if len(path) == 0: return list(self.data.keys())
        return []

    def ldoc(self, path):
        if len(path) == 0: return super(ServerInfo, self).ldoc(path)
        if len(path) > 1: return None
        return self.keyword_doc.get(path[0], None)

    def update_filesystems(self):
        out = subprocess.getoutput("df -h | grep -e '^/dev' | awk '{print $6,$5;}'")
        for line in out.split("\n"):
            fs, space = line.split()
            space = space.replace("%","")
            self.data['filesystems'][fs] = space

    def update_loadavg(self):
        load = open("/proc/loadavg").readline().split()[0]
        self.load_deque.append((time.time()*1000, float(load)))
        self.data['load_avg'] = list(self.load_deque)

    def update_stats(self):
        self.update_loadavg()
        self.update_filesystems()
        log.info("Collecting data")

def init(**kw):
    conf = kw.get("conf", None)

    tree = ServerInfo("serverinfo", conf=conf)

    @defer
    def update_all_data():
        log.info("Updating server stats")
        tree.update_stats()
        reactor.callLater(120, update_all_data)

    reactor.callLater(1, update_all_data)

    yield dict(
        tree = tree,
        root = "/"
        )
