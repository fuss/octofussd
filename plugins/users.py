# -*- coding: utf-8 -*-

import octofuss

class Shells(octofuss.Tree):
    def __init__(self, name, mocking):
        super(Shells, self).__init__(name, doc="Read-only contents of file /etc/shells")
        self.mocking = mocking
        self._shells = []
        self._load()

    def _load(self):
        if self.mocking:
            self._shells = ['/bin/bash', '/bin/false']
        else:
            self._shells = sorted([x.strip() for x in open("/etc/shells", "r") if not x.startswith('#')])

    def llist(self, path):
        self._load()
        if not path:
            return self._shells
        else:
            return []

    def lget(self, path):
        self._load()
        return self._shells


def init(**kw):
    tree = kw.get("tree", None)
    conf = kw.get("conf", None)
    forceMock = conf and conf.getboolean("General", "forceMockup")

    import usersplugin.common as common
    from usersplugin.common import UserList, GroupList

    if forceMock:
        import usersplugin.mockusers as mockusers
        common.userDB = mockusers.UserDB()
    else:
        try:
            import usersplugin.ldapusers as ldapusers
            common.userDB = ldapusers.UserDB(tree, conf)
        except:
            import traceback
            traceback.print_exc()
            return

    common.tree = tree

    top = octofuss.Forest("users", doc="User and group database")
    top.register(UserList("users"))
    top.register(GroupList("groups"))
    top.register(Shells("shells", forceMock))
    yield dict(tree = top, root = "/")
