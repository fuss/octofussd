import octofuss
import xmlrpc.client

SERVERURL = "http://localhost:13400/conf/"
USERNAME = "root"
PASSWORD = "root"
PREFIX = "octofussd-test-"

class OctofussdTestMixin(object):
    def setUp(self):
        self.loginserver = xmlrpc.client.ServerProxy(SERVERURL)
        self.apikey = self.loginserver.login(USERNAME, PASSWORD)
        self.tree = octofuss.xmlrpc.Client(SERVERURL, self.apikey)

    def assertHas(self, path):
        if not self.tree.has(path):
            self.fail("Tree does not have path " + path)

    def assertNotHas(self, path):

        if self.tree.has(path):
            self.fail("Tree does have path " + path)

    def assertInList(self, name, path):
        for i in self.tree.list(path):
            if i == name:
                return
        self.fail("'%s' does not appear when listing path %s" % (name, path))

    def assertNotInList(self, name, path):
        for i in self.tree.list(path):
            if i == name:
                self.fail("'%s' does appear when listing path %s" % (name, path))

    def assertInTree(self, path):
        self.assertHas(path)
        parent, leaf = path.rsplit("/", 1)
        self.assertInList(leaf, parent)

    def assertNotInTree(self, path):
        self.assertNotHas(path)
        parent, leaf = path.rsplit("/", 1)
        self.assertNotInList(leaf, parent)
