import octofuss
from urllib.parse import quote_plus, unquote_plus
import os.path

WOL = os.path.isfile("/usr/bin/wakeonlan") or False

_ = lambda x:x

def _parseBool(value):
    if value == "True" or value == "true": return True
    if value == "False" or value == "false": return False
    return None

class Lister(octofuss.Tree):
    def __init__(self, name, generator):
        super(Lister, self).__init__(name)
        self.generator = generator

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) == 1: return path[0] in self.generator()
        return False

    def lget(self, path):
        if len(path) == 0: return sorted(list(self.generator()))
        if len(path) == 1: return sorted([x for x in self.generator() if x.startswith(path[0])])
        return None

    def llist(self, path):
        if len(path) == 0: return sorted(list(self.generator()))
        if len(path) == 1: return sorted([x for x in self.generator() if x.startswith(path[0])])
        return []

class ShutdownAction(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(
                label=_("Shutdown now"),
                type="nudge")
        super(ShutdownAction, self).__init__(name, self.info)
    def perform(self, value):
        self.host.shutdown()

class LockDesktopAction(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(type="nudge")
        super(LockDesktopAction, self).__init__(name, self.info)
        self.updateInfo()

    def updateInfo(self):
        if self.host["screen_locked"]:
            self.info["label"] = _("Unlock desktop")
        else:
            self.info["label"] = _("Lock desktop")
        self.info["value"] = not self.host["screen_locked"]

    def perform(self, value):
        value = _parseBool(value)
        if value != None:
            self.host.setScreenLock(bool(value))
            self.updateInfo()

class SendMessageAction(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(
                type="freeform",
                label=_("Send a message"),
                text=_("Message to send"))
        super(SendMessageAction, self).__init__(name, self.info)
    def enabled(self):
        return self.host["logged_user"] != None
    def perform(self, value):
        self.host.sendMessage(value)

class WakeUpAction(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(
                type="nudge",
                label=_("Power up"))
        super(WakeUpAction, self).__init__(name, self.info)

    def enabled(self):
        if not WOL:
            return False

        return self.host["isup"] != True

    def perform(self, value):
        self.host.wakeup()


class ToggleNetAction(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(type="nudge")
        super(ToggleNetAction, self).__init__(name, self.info)
        self.updateInfo()

    def updateInfo(self):
        if self.host["net_disabled"]:
            self.info["label"] = _("Enable internet")
        else:
            self.info["label"] = _("Disable internet")
        self.info["value"] = not self.host["net_disabled"]

    def perform(self, value):
        value = _parseBool(value)
        if value != None:
            self.host.setNetworkLock(value)
            self.updateInfo()

class AddScriptRun(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(
            type="completion",
            path="candidates",
            label=_("Add to script run"),
            text=_("Add to script run"),
            reload="cluster",
        )
        self.candidates = Lister("candidates", self._get_candidates)
        super(AddScriptRun, self).__init__(name, self.info)

    def _get_candidates(self):
        return self.host.available_scripts()

    def perform(self, value):
        value = quote_plus(unquote_plus(value))
        self.host.addScriptRun(value)


class AddUpgradeCampaign(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(
            type="completion",
            path="candidates",
            label=_("Add to upgrade"),
            text=_("Add to upgrade"),
            reload="cluster",
        )
        self.candidates = Lister("candidates", self._get_candidates)
        super(AddUpgradeCampaign, self).__init__(name, self.info)

    def _get_candidates(self):
        return self.host.available_upgrades()

    def perform(self, value):
        value = quote_plus(unquote_plus(value))
        self.host.addUpgradeCampaign(value)


class AddClusterAction(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(
            type="completion",
            path="candidates",
            label=_("Add to cluster"),
            text=_("Add to cluster"),
            reload="cluster",
        )
        self.candidates = Lister("candidates", self._get_candidates)
        super(AddClusterAction, self).__init__(name, self.info)

    def _get_candidates(self):
        return self.host.pool().clusters().difference(self.host.clusters)

    def perform(self, value):
        value = quote_plus(unquote_plus(value))
        self.host.addCluster(value)

class DelClusterAction(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(
            type="choice",
            label=_("Remove from cluster"),
            reload="cluster",
            options=[]
        )
        super(DelClusterAction, self).__init__(name, self.info)

    def enabled(self):
        return len(self.host.clusters) > 0

    def perform(self, value):
        value = quote_plus(unquote_plus(value))
        self.host.delCluster(value)

    def lget(self, path):
        self.info["options"] = list(self.host.clusters)
        return super(DelClusterAction, self).lget(path)

class InstallPkgAction(octofuss.Action):
    def __init__(self, name, host):
        self.host = host
        self.info = dict(
            type="freeform",
            label=_("Install software"),
            text=_("Package to install")
        )
        super(InstallPkgAction, self).__init__(name, self.info)

    def perform(self, value):
        self.host.installPackage(value)

class BaseComputerAction(octofuss.Action):
    def __init__(self, name, info, computers):
        super(BaseComputerAction, self).__init__(name, info)
        self.computers = computers

    def iterhosts(self):
        return self.computers.hosts()

    def enabled(self):
        for h in self.iterhosts():
            if h.has(":actions:/"+self._name):
                return True
        return False

    def perform(self, value):
        for h in self.iterhosts():
            if h.has(":actions:/"+self._name):
                h.set(":actions:/"+self._name, value)

class DelEntireClusterAction(BaseComputerAction):
    def __init__(self, name, computers, info=dict()):
        info.update(
            label=_("Delete Cluster"),
            type="nudge"
        )
        super(DelEntireClusterAction, self).__init__(name, info, computers)

    def enabled(self):
        counter = 0
        for h in self.iterhosts():
            counter += 1
        if counter > 0:
            return False
        else:
            return True

    def perform(self, value):
        # try to delete computer, if any
        try:
            del(self.computers)
        except:
            pass
        del(self)

class ComputersWakeUp(BaseComputerAction):
    def __init__(self, name, computers, info=dict()):
        info.update(
            label=_("Power up"),
            type="nudge"
        )
        super(ComputersWakeUp, self).__init__(name, info, computers)

    def perform(self, value):
        for h in self.iterhosts():
            if not h["is_up"]:
                h.wakeup()


class ComputersEnableNet(BaseComputerAction):
    def __init__(self, name, computers, info=dict()):
        info.update(
            label=_("Enable internet"),
            type="nudge"
        )
        super(ComputersEnableNet, self).__init__(name, info, computers)
    def enabled(self):
        for h in self.iterhosts():
            if h["net_disabled"]:
                return True
        return False
    def perform(self, value):
        for h in self.iterhosts():
            if h["net_disabled"]:
                h.setNetworkLock(False);

class ComputersDisableNet(BaseComputerAction):
    def __init__(self, name, computers, info=dict()):
        info.update(
            label=_("Disable internet"),
            type="nudge"
        )
        super(ComputersDisableNet, self).__init__(name, info, computers)
    def enabled(self):
        for h in self.iterhosts():
            if not h["net_disabled"]:
                return True
        return False
    def perform(self, value):
        for h in self.iterhosts():
            if not h["net_disabled"]:
                h.setNetworkLock(True);

class ComputersLockScreen(BaseComputerAction):
    def __init__(self, name, computers, info=dict()):
        info.update(
            label=_("Lock screen"),
            type="nudge"
        )
        super(ComputersLockScreen, self).__init__(name, info, computers)

    def enabled(self):
        for h in self.iterhosts():
            if not h["screen_locked"]:
                return True
        return False

    def perform(self, value):
        for h in self.iterhosts():
            if not h["screen_locked"]:
                h.setScreenLock(True);

class ComputersUnlockScreen(BaseComputerAction):
    def __init__(self, name, computers, info=dict()):
        info.update(
            label=_("Unlock screen"),
            type="nudge"
        )
        super(ComputersUnlockScreen, self).__init__(name, info, computers)

    def enabled(self):
        for h in self.iterhosts():
            if h["screen_locked"]:
                return True
        return False

    def perform(self, value):
        for h in self.iterhosts():
            if h["screen_locked"]:
                h.setScreenLock(False);

class ComputersCreate(octofuss.Action):
    def __init__(self, name, computers, info=dict()):
        self.computers = computers
        info.update(
            type="freeform",
            label=_("Create new computer"),
            text=_("Name of the new computer")
        )
        super(ComputersCreate, self).__init__(name, info)

    def perform(self, value):
        self.computers._create_new(value)

class HostActions(octofuss.ActionTree):
    def __init__(self, name, host):
        super(HostActions, self).__init__(name, doc="Actions on host '"+host._name+"'")
        self.host = host
        self.register(ShutdownAction("shutdown", host))
        self.register(SendMessageAction("message", host))
        self.register(LockDesktopAction("lockdesktop", host))
        self.register(ToggleNetAction("togglenet", host))
        self.register(InstallPkgAction("installpkg", host))
        self.register(AddScriptRun("addscript", host))
        self.register(AddUpgradeCampaign("addupgrade", host))
        self.register(AddClusterAction("addcluster", host))
        self.register(DelClusterAction("delcluster", host))
        self.register(WakeUpAction("wakeup", host))

class ComputersActions(octofuss.ActionTree):
    def __init__(self, name, computers):
        super(ComputersActions, self).__init__(name, doc="Actions for all computers")
        self.register(BaseComputerAction("shutdown", dict(
                        label=_("Shutdown now"),
                        type="nudge",
                        order=1,
                      ), computers))
        self.register(BaseComputerAction("message", dict(
                        label=_("Send a message"),
                        type="freeform",
                        text=_("Message to send"),
                        order=2,
                      ), computers))
        self.register(BaseComputerAction("installpkg", dict(
                        label=_("Install software"),
                        type="freeform",
                        text=_("Package to install"),
                        order=3,
                      ), computers))
        self.register(ComputersEnableNet("enablenet", computers, dict(order=4)))
        self.register(ComputersDisableNet("disablenet", computers, dict(order=5)))
        self.register(ComputersLockScreen("lockdesktop", computers, dict(order=6)))
        self.register(ComputersUnlockScreen("unlockdesktop", computers, dict(order=7)))
        self.register(ComputersWakeUp("wakeup", computers, dict(order=8)))
        self.register(ComputersCreate("create", computers, dict(order=9)))
        self.register(DelEntireClusterAction("delentirecluster", computers, dict(order=10)))
