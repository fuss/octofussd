#  File: model.py
#
#  Copyright (C) 2008-2016 The Fuss Project <info@fuss.bz.it>
#  Copyright (C) 2008-2016 Christopher R. Gabriel <cgabriel@truelite.it>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#

import hashlib
import random


Hasher = hashlib.md5
SALTCHARS="".join(map(chr, range(33,126)))


from django.db import models

class Users(models.Model):
    username = models.TextField(null=False, blank=False)
    action = models.TextField(null=False, blank=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "users"

class Request(models.Model):
    username = models.TextField(null=False, blank=False)
    hostname = models.TextField(null=False, blank=False)
    message = models.TextField(null=False, blank=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "request"

class UserGroups(models.Model):
    hostname = models.TextField(null=False)
    username = models.TextField(null=False)
    groupname = models.TextField(null=False)
    class Meta:
        db_table = "user_groups"

class ClientComponent(models.Model):
    hostname = models.TextField(null=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    component = models.TextField(null=False)
    value = models.TextField(null=False)
    class Meta:
        db_table = "client_component"

class ClientActivity(models.Model):
    hostname = models.TextField(null=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "client_activity"

class InstallQueue(models.Model):
    hostname = models.TextField(null=False)
    package = models.TextField(null=False)
    status = models.IntegerField(null=True, default=None)
    lastlog = models.TextField(null=True)
    inserttime = models.DateTimeField(auto_now_add=True)
    lastlog_time = models.DateTimeField(null=True, blank=True)
    class Meta:
        db_table = "install_queue"

class Upgrade(models.Model):
    name = models.TextField(null=False, unique=True)
    type = models.TextField(null=False, default="upgrade")
    notes = models.TextField(null=False, default="")
    scheduled = models.DateTimeField(null=True, blank=True, default=None)
    completed = models.DateTimeField(null=True, blank=True, default=None)
    class Meta:
        db_table = "upgrade"

class UpgradeHost(models.Model):
    upgrade = models.ForeignKey(
        "Upgrade",
        related_name = "hosts",
        on_delete=models.CASCADE
    )
    name = models.TextField(null=False)
    started = models.DateTimeField(null=True, default=None)
    ended = models.DateTimeField(null=True, default=None)
    output = models.TextField(null=True, default=None)
    result = models.IntegerField(null=True, default=None)

    class Meta:
        db_table = "upgrade_host"
        unique_together = ['upgrade', 'name']

class Scripts(models.Model):
    name = models.TextField(null=False, unique=True)
    script = models.TextField(null=False, default="")
    class Meta:
        db_table = "scripts"

class ScriptRun(models.Model):
    name = models.TextField(null=False, unique=True)
    script = models.ForeignKey(
        Scripts,
        related_name = "runs",
        on_delete=models.CASCADE
    )
    notes = models.TextField(null=False, default="")
    scheduled = models.DateTimeField(null=True, default=None)
    completed = models.DateTimeField(null=True, default=None)
    class Meta:
        db_table = "script_run"

class ScriptRunHost(models.Model):
    script_run = models.ForeignKey(
        ScriptRun,
        related_name = "hosts",
        on_delete=models.CASCADE
    )
    name = models.TextField(null=False)
    started = models.DateTimeField(null=True, default=None)
    ended = models.DateTimeField(null=True, default=None)
    result = models.IntegerField(null=True, default=None)
    output = models.TextField(null=True, default=None)

    class Meta:
        db_table = "script_run_host"
        unique_together = ['script_run', 'name']

class NetworkPrinter(models.Model):
    queue_name = models.CharField(max_length=255)
    hostname = models.CharField(max_length=255)
    class Meta:
        unique_together = ['queue_name','hostname']

class AuthPath(models.Model):
    user_name = models.CharField(max_length=16)
    path = models.CharField(max_length=300)
    class Meta:
        db_table = "auth_path"

class Auth(models.Model):
    user_name = models.CharField(max_length=16, unique=True)
    email_address = models.CharField(max_length=255)
    display_name = models.CharField(max_length=255)
    password = models.CharField(max_length=40)

    class Meta:
        db_table = "auth"

    def __str__(self):
        return self.user_name

    def __repr__(self):
        return self.user_name

    def verify_password(self, val):
        return verify_password(val, self.password)

    def set_password(self, password):
        self.password = crypt_password(password)

    @classmethod
    def reset_root(cls, password):
        try:
            obj = cls.objects.get(user_name="root")
            obj.set_password(password)
            obj.save()
        except:
            obj = cls.objects.create(user_name = "root",
                                     email_address = "root@localhost",
                                     display_name = "System Administrator")
            obj.set_password(password)
            obj.save()


def crypt_password(pwd, salt=None):
    """Crypt a password.

    salt is randomly generated if it is not given"""
    if salt is None:
        salt = random.choice(SALTCHARS) + random.choice(SALTCHARS)
    h = Hasher()
    h.update((salt + pwd).encode('utf-8'))
    return salt + h.hexdigest()

def verify_password(pwd, hash_pwd):
    """Verify a password against a hashed password"""
    salt = hash_pwd[:2]
    h = Hasher()
    h.update((salt + pwd).encode('utf-8'))
    return salt + h.hexdigest() == hash_pwd
