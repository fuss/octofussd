import unittest
from tests.test_base import OctofussdTestMixin, PREFIX
MAXSHARES = 3

def _share(num):
	if num >= MAXSHARES:
		raise ValueError("Attempt to generate share name #%d that will not be cleaned" % num)
	return PREFIX+"share"+str(num)

class TestSamba(OctofussdTestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        # Cleanup test shares
        for i in range(MAXSHARES):
            if self.tree.has("/samba/"+_share(i)):
                self.tree.delete("/samba/"+_share(i))

    def testCreateShare(self):
        share = _share(0)
        sharepath = "/samba/" + share
        self.assertNotHas(sharepath)
        self.tree.create(sharepath, {})
        self.assertInTree(sharepath)

    def testDeleteShare(self):
        share = _share(0)
        sharepath = "/samba/" + share
        self.assertNotHas(sharepath)
        self.tree.create(sharepath, {})
        self.assertInTree(sharepath)
        self.tree.delete(sharepath)
        self.assertNotInTree(sharepath)
