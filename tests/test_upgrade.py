from unittest import TestCase
from tests.test_base import OctofussdTestMixin, PREFIX

class TestUpgrade(OctofussdTestMixin, TestCase):
    def setUp(self):
        super().setUp()
        if self.tree.has("/upgrades/test"): self.tree.delete("/upgrades/test")

    def test_type(self):
        self.tree.create("/upgrades/test")
        self.assertEquals(self.tree.get("/upgrades/test/type"), "upgrade")
        self.tree.set("/upgrades/test/type", "dist-upgrade")
        self.assertEquals(self.tree.get("/upgrades/test/type"), "dist-upgrade")

    def test_scheduled(self):
        self.tree.create("/upgrades/test")
        self.tree.create("/upgrades/test/hosts/foo")
        self.assertEquals(self.tree.get("/upgrades/test/scheduled"), None)
        self.tree.set("/upgrades/test/scheduled", True)
        self.assertNotEquals(self.tree.get("/upgrades/test/scheduled"), None)
