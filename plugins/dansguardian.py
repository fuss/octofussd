import octofuss
import os, os.path
from octofussd import system

naughtyvalues = [
    ("< 10", 50),
    ("10 - 14", 100),
    ("14 - 18", 160),
    ("> 18", 200)
    ]


class DansguardianRestart(octofuss.Tree):
    def __init__(self, name = None):
        super(DansguardianRestart, self).__init__(name, doc="""Restart e2guardian.

Set this node to any value to restart e2guardian.
""")

    def _perform(self):
        return system.restart_service("e2guardian")

    def lget(self, path):
        if len(path) == 0:
            return self._perform()
        else:
            return super(DansguardianRestart, self).lget(path)

    def lset(self, path, value):
        """ duplicates the get method """
        return self.lget(path)

    def llist(self, path):
        return []

class MockDansguardianRestart(DansguardianRestart):
    def _perform(self):
        import random
        if random.randrange(2) > 0:
            return False
        else:
            return True

DANSGUARDIAN_HELP="""Sites always allowed

This node contains a list of websites that are always accessible. Each entry
has the web address as the name, and an explanation as the value."""

class DansguardianConfig(octofuss.Tree):
    def __init__(self, conf_file, name = "config", doc=DANSGUARDIAN_HELP):
        super(DansguardianConfig, self).__init__(name)
        self.conf_file = conf_file
        self.config_items = {}
        self._load()

    def _load(self):
        self.config_items = {}
        f = open(self.conf_file)
        for line in f:
            line = line.strip()
            if line.startswith("#"):
                continue
            s = line.split("=")
            key = ''
            value = 'Missing value'
            if len(s) > 1:
                key = s[0].strip()
                value = s[1].strip()
                if key and value:
                    self.config_items[key] = value

        f.close()

    def _commit(self):
        f = open(self.conf_file, "w")
        for k in list(self.config_items.keys()):
            f.write("%s = %s\n" % (k, self.config_items[k]))
        f.close()
        self._load()

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) > 1: return super(DansguardianConfig, self).lhas(path)
        return path[0] in self.config_items

    def lget(self, path):
        if len(path) == 0: return self.config_items
        if len(path) > 1: return super(DansguardianConfig, self).lget(path)
        return self.config_items.get(path[0], None)

    def lset(self, path, value):
        if len(path) != 1: return super(DansguardianConfig, self).lset(path, value)
        self.config_items[path[0]] = value
        self._commit()

    def lcreate(self, path, value=None):
        if not path[0].startswith('.'):
            path[0] = "."+path[0]
        self.lset(path, value)
        return self.lget(path)

    def ldelete(self, path):
        if len(path) != 1: return super(DansguardianConfig, self).ldelete(path)
        del self.config_items[path[0]]
        self._commit()

    def llist(self, path):
        if len(path) == 0:
            k = list(self.config_items.keys())
            k.sort()
            return k
        else:
            return []

    def ldoc(self, path):
        if len(path) == 0: return super(DansguardianConfig, self).ldoc(path)
        if len(path) > 1: return None
        return self.config_items.get(path[0], None)


class MockDansguardianConfig(octofuss.DictTree):
    def __init__(self, name = "config"):
        super(MockDansguardianConfig, self).__init__(name, doc=DANSGUARDIAN_HELP)
        from urllib.parse import quote_plus
        self[quote_plus("naughtynesslimit")] = "50"

    def ldoc(self, path):
        if len(path) == 0: return super(MockDansguardianConfig, self).ldoc(path)
        if len(path) > 1: return None
        return self.lget(path)




class DansguardianBannedExtensions(octofuss.Tree):
    def __init__(self, conf_file, name = "banned_extensions", doc=DANSGUARDIAN_HELP):
        super(DansguardianBannedExtensions, self).__init__(name)
        self.conf_file = conf_file
        self.banned_extensions = {}
        self._load()

    def _load(self):
        self.banned_extensions = {}
        f = open(self.conf_file)
        for line in f:
            line = line.strip()
            if line.startswith("#"):
                continue
            s = line.split("#")
            ext = ''
            description = 'Missing description'
            if len(s) > 1:
                ext = s[0].strip()
                description = s[1].strip()
            else:
                ext = line.strip()

            if ext:
                self.banned_extensions[ext] = description

        f.close()

    def _commit(self):
        f = open(self.conf_file, "w")
        for k in list(self.banned_extensions.keys()):
            f.write("%s # %s\n" % (k, self.banned_extensions[k]))
        f.close()
        self._load()

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) > 1: return super(DansguardianBannedExtensions, self).lhas(path)
        return path[0] in self.banned_extensions

    def lget(self, path):
        if len(path) == 0: return self.banned_extensions
        if len(path) > 1: return super(DansguardianBannedExtensions, self).lget(path)
        return self.banned_extensions.get(path[0], None)

    def lset(self, path, value):
        if len(path) != 1: return super(DansguardianBannedExtensions, self).lset(path, value)
        self.banned_extensions[path[0]] = value
        self._commit()

    def lcreate(self, path, value=None):
        if not path[0].startswith('.'):
            path[0] = "."+path[0]
        self.lset(path, value)
        return self.lget(path)

    def ldelete(self, path):
        if len(path) != 1: return super(DansguardianBannedExtensions, self).ldelete(path)
        del self.banned_extensions[path[0]]
        self._commit()

    def llist(self, path):
        if len(path) == 0:
            k = list(self.banned_extensions.keys())
            k.sort()
            return k
        else:
            return []

    def ldoc(self, path):
        if len(path) == 0: return super(DansguardianBannedExtensions, self).ldoc(path)
        if len(path) > 1: return None
        return self.banned_extensions.get(path[0], None)


class MockDansguardianBannedExtensions(octofuss.DictTree):
    def __init__(self, name = "banned_extensions"):
        super(MockDansguardianBannedExtensions, self).__init__(name, doc=DANSGUARDIAN_HELP)
        from urllib.parse import quote_plus
        self[quote_plus(".exe")] = "MS Window Executable (example)"

    def ldoc(self, path):
        if len(path) == 0: return super(MockDansguardianBannedExtensions, self).ldoc(path)
        if len(path) > 1: return None
        return self.lget(path)


class DansguardianAllowedSites(octofuss.Tree):
    def __init__(self, conf_file, name = "allowed_sites", doc=DANSGUARDIAN_HELP):
        super(DansguardianAllowedSites, self).__init__(name)
        self.conf_file = conf_file
        self.allowed_sites = {}
        self._load()

    def _load(self):
        self.allowed_sites = {}
        f = open(self.conf_file)
        for line in f:
            if line.startswith("#"):
                continue
            s = line.split("#")
            if len(s) > 1:
                if len(s[0].strip()) > 0:
                    self.allowed_sites[s[0].strip()] = s[1].strip()
            else:
                if len(s[0].strip()) > 0:
                    self.allowed_sites[s[0].strip()] = ""
        f.close()

    def _commit(self):
        f = open(self.conf_file, "w")
        for k in list(self.allowed_sites.keys()):
            f.write("%s # %s\n" % (k, self.allowed_sites[k]))
        f.close()
        self._load()

    def lhas(self, path):
        self._load()
        if len(path) == 0: return True
        if len(path) > 1: return super(DansguardianAllowedSites, self).lhas(path)
        return path[0] in self.allowed_sites

    def lget(self, path):
        self._load()
        if len(path) == 0: return self.allowed_sites
        if len(path) > 1: return super(DansguardianAllowedSites, self).lget(path)
        return self.allowed_sites.get(path[0], None)

    def lset(self, path, value):
        self._load()
        if len(path) != 1: return super(DansguardianAllowedSites, self).lset(path, value)
        self.allowed_sites[path[0]] = value
        self._commit()

    def lcreate(self, path, value=None):
        self.lset(path, value)
        return self.lget(path)

    def ldelete(self, path):
        self._load()
        if len(path) != 1: return super(DansguardianAllowedSites, self).ldelete(path)
        del self.allowed_sites[path[0]]
        self._commit()

    def llist(self, path):
        self._load()
        if len(path) == 0:
            k = list(self.allowed_sites.keys())
            k.sort()
            return k
        else:
            return []

    def ldoc(self, path):
        self._load()
        if len(path) == 0: return super(DansguardianAllowedSites, self).ldoc(path)
        if len(path) > 1: return None
        return self.allowed_sites.get(path[0], None)


class MockDansguardianAllowedSites(octofuss.DictTree):
    def __init__(self, name = "allowed_sites"):
        super(MockDansguardianAllowedSites, self).__init__(name, doc=DANSGUARDIAN_HELP)
        from urllib.parse import quote_plus
        self[quote_plus("http://www.example.org")] = "This is an example"

    def ldoc(self, path):
        if len(path) == 0: return super(MockDansguardianAllowedSites, self).ldoc(path)
        if len(path) > 1: return None
        return self.lget(path)


class DansguardianBannedSites(octofuss.Tree):
    def __init__(self, conf_file, name = "banned_sites", doc=DANSGUARDIAN_HELP):
        super(DansguardianBannedSites, self).__init__(name)
        self.conf_file = conf_file
        self.banned_sites = {}
        self._load()

    def _load(self):
        self.banned_sites = {}
        f = open(self.conf_file)
        for line in f:
            if line.startswith("#"):
                continue
            if "Include" in line:
                continue
            s = line.split("#")
            if len(s) > 1:
                if len(s[0].strip()) > 0:
                    self.banned_sites[s[0].strip()] = s[1].strip()
            else:
                if len(s[0].strip()) > 0:
                    self.banned_sites[s[0].strip()] = ""
        f.close()

    def _commit(self):
        f = open(self.conf_file, "w")
        for k in list(self.banned_sites.keys()):
            f.write("%s # %s\n" % (k, self.banned_sites[k]))
        f.close()
        self._load()

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) > 1: return super(DansguardianBannedSites, self).lhas(path)
        return path[0] in self.banned_sites

    def lget(self, path):
        if len(path) == 0: return self.banned_sites
        if len(path) > 1: return super(DansguardianBannedSites, self).lget(path)
        return self.banned_sites.get(path[0], None)

    def lset(self, path, value):
        if len(path) != 1: return super(DansguardianBannedSites, self).lset(path, value)
        self.banned_sites[path[0]] = value
        self._commit()

    def lcreate(self, path, value=None):
        self.lset(path, value)
        return self.lget(path)

    def ldelete(self, path):
        if len(path) != 1: return super(DansguardianBannedSites, self).ldelete(path)
        del self.banned_sites[path[0]]
        self._commit()

    def llist(self, path):
        if len(path) == 0:
            k = list(self.banned_sites.keys())
            k.sort()
            return k
        else:
            return []

    def ldoc(self, path):
        if len(path) == 0: return super(DansguardianBannedSites, self).ldoc(path)
        if len(path) > 1: return None
        return self.banned_sites.get(path[0], None)


class MockDansguardianBannedSites(octofuss.DictTree):
    def __init__(self, name = "banned_sites"):
        super(MockDansguardianBannedSites, self).__init__(name, doc=DANSGUARDIAN_HELP)
        from urllib.parse import quote_plus
        self[quote_plus("http://www.example.org")] = "This is an example"

    def ldoc(self, path):
        if len(path) == 0: return super(MockDansguardianBannedSites, self).ldoc(path)
        if len(path) > 1: return None
        return self.lget(path)


def init(**kw):
    conf = kw.get("conf", None)
    canMock = conf and conf.getboolean("General", "allowMockup")
    forceMock = conf and conf.getboolean("General", "forceMockup")

    start = False
    top = octofuss.Forest("contentfilter", doc="Configuration of e2guardian content filter")

    if not forceMock and os.path.exists("/etc/fuss-server/content-filter-allowed-sites"):
        top.register(DansguardianAllowedSites("/etc/fuss-server/content-filter-allowed-sites"))
        start = True
    elif forceMock or canMock:
        top.register(MockDansguardianAllowedSites())
        start = True

    if not forceMock and os.path.exists("/etc/e2guardian/lists/bannedsitelist"):
        top.register(DansguardianBannedSites("/etc/e2guardian/lists/bannedsitelist"))
        start = True
    elif forceMock or canMock:
        top.register(MockDansguardianBannedSites())
        start = True

    datafile = "/etc/e2guardian/lists/bannedextensionlist"
    if not forceMock and os.path.exists(datafile):
        top.register(DansguardianBannedExtensions(datafile))
    elif forceMock or canMock:
        top.register(MockDansguardianBannedExtensions())

    datafile = "/etc/e2guardian/e2guardianf1.conf"
    if not forceMock and os.path.exists(datafile):
        top.register(DansguardianConfig(datafile))
    elif forceMock or canMock:
        top.register(MockDansguardianConfig())

    if not forceMock and os.path.exists("/etc/init.d/e2guardian"):
        top.register(DansguardianRestart("restart"))
        start = True
    elif forceMock or canMock:
        top.register(MockDansguardianRestart("restart"))
        start = True

    if start:
        yield dict(tree = top, root = "/")
