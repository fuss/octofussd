import octofuss
from octofussd.data.models import AuthPath, Auth

def get_perms_for_user(user):
    perms = AuthPath.objects.filter(user_name=user)
    return [x.path for x in perms]


class OctofussAuth(octofuss.Tree):
    def __init__(self, name = None):
        super(OctofussAuth, self).__init__(name, doc="Octofuss Authorization")

    def lhas(self, path):
        if len(path) < 2: return True

        u = AuthPath.objects.filter(user_name=path[0], path=path[1])
        if u.count() > 0:
            return True
        else:
            return False

    def llist(self, path):
        if len(path) == 0:
            s = set([x.user_name for x in AuthPath.objects.all()])
            return list(s)

        perms = get_perms_for_user(path[0])
        if len(path) == 1:
            return perms

    def lget(self, path):
        return self.llist(path)

    def lcreate(self, path, value):
        if len(value) > 0:
            AuthPath.objects.create(user_name=path[0], path=value)

    def ldelete(self, path):
        if len(path) == 1:
            # deleting the whole user
            r = AuthPath.objects.filter(user_name=path[0])
        if len(path) == 2:
            r = AuthPath.objects.filter(user_name=path[0],
                                        path = path[1])
        r.delete()

class OctofussUsers(octofuss.Tree):
    def __init__(self, name = None):
        super(OctofussUsers, self).__init__(name, doc="Octofuss Users")

    def lhas(self, path):
        if len(path) == 0: return True
        u = Auth.objects.filter(user_name=path[0])
        if u.count() > 0:
            return True
        else:
            return False

    def llist(self, path):
        if len(path) == 0:
            s = set([x.user_name for x in Auth.objects.all() if x.user_name != 'root'])
            return list(s)

        if len(path) == 1:
            try:
                u = Auth.objects.get(user_name=path[0])
                return ['password']
            except:
                pass

        if len(path) == 2:
            if path[1] == 'password':
                return "Password can't be read, only set"
        return None

    def lset(self, path, value):
        if len(path) == 2:
            if path[1] == 'password':
                # change the user password
                try:
                    u = Auth.by_user_name(path[0])
                    u.password = value
                    u._connection.commit()
                except:
                    pass

    def lget(self, path):
        return self.llist(path)

    def lcreate(self, path, value):
        if len(path) == 1:
            obj, created = Auth.objects.get_or_create(user_name=path[0],
                                                      defaults=dict(password=value,
                                                                   email_address="{}@localhost".format(path[0]),
                                                                   display_name=path[0])
                                                      )
            return created

    def ldelete(self, path):
        if path[0] == 'root':
            return "Don't remove root user!"
        if len(path) == 1:
            Auth.objects.filter(user_name = path[0]).delete()
            AuthPath.objects.filter(user_name = path[0]).delete()



def init(**kw):
    yield dict(
            tree = OctofussAuth("access"),
            root = "/auth"
            )

    yield dict(
            tree = OctofussUsers("users"),
            root = "/auth"
            )
