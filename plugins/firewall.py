import octofuss
import os, os.path

class AvailableServices(octofuss.Tree):
    def __init__(self, name = None):
        super(AvailableServices, self).__init__(name, doc="Read-only contents of file /etc/services")
        self._services = {}
        try:
            f = open("/etc/services")
            lines = f.readlines()
        except:
            lines = []
        for line in lines:
            s = line.split()
            if len(s) > 0 and s[0] and not s[0].startswith("#"):
                self._services[s[0]] = s[1]

    def llist(self, path):
        if not path:
            k = list(self._services.keys())
            k.sort()
            return k
        else:
            return []

    def lget(self, path):
        if len(path) == 0: return self._services
        if len(path) > 1: return super(AvailableServices, self).lget(self, path)
        return self._services[path[0]]


class FirewallRestart(octofuss.Tree):
    def __init__(self, name = None):
        super(FirewallRestart, self).__init__(name, doc="""Firewall restart control

Set this node to any value to restart the firewall""")

    def _perform(self):
        r = os.system("/etc/init.d/firewall restart")
        if r > 0:
            return False
        else:
            return True

    def lget(self, path):
        return self._perform()

    def lhas(self, path):
        if len(path) == 0: return True
        return False

    def llist(self, path):
        return []

    def lset(self, path, value):
        return None

class MockFirewallRestart(FirewallRestart):
    def __init__(self, name = None):
        super(MockFirewallRestart, self).__init__(name)

    def _perform(self):
        import random
        if random.randrange(2) > 0:
            return False
        else:
            return True

# See http://devel.fuss.bz.it/wiki/FussServer/2.0/Conf
files_doc = {
    "denied_lan_hosts": "List of hosts that cannot reach any service on the Internet",
    "allowed_lan_hosts": "List of hosts that can access services on the Internet",
    "allowed_wan_hosts": "List of hosts on the internet that can always, fully be reached",
    "allowed_wan_host_services": """Services on the internet that are reachable from certain hosts on the internal network.

Services are specified as "IP address:port".""",
    "allowed_wan_services": """List of services on the internet that can always, fully be reached.

Services are specified by internet port number""",
    "external_services": """Services offered from the server to the Internet.

Services are specfied by internet port number.""",
}


class FirewallConfFile(octofuss.Tree):
    def __init__(self, conf_file, name = None):
        super(FirewallConfFile, self).__init__(name, doc=files_doc.get(name, None))
        # we use a generic file
        self.conf_file = conf_file
        self._load()

    def _load(self):
        self.configuration = {}
        self.header_comments = []
        f = open(self.conf_file)
        for line in f:
            if line.startswith("#"):
                self.header_comments.append(line)
                continue
            r = line.strip().split(":")
            if len(r) == 1:
                host = r[0]
                desc = ""
            else:
                host = r[0]
                desc = r[1]

            self.configuration[host] = desc

        f.close()

    def _commit(self):
        f = open(self.conf_file, "w")
        for line in self.header_comments:
            f.write(line)
        for k in list(self.configuration.keys()):
            f.write("%s:%s\n" % (k, self.configuration[k]))
        f.close()
        self._load()

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) > 1: return False
        return path[0] in self.configuration

    def lget(self, path):
        if len(path) == 0: return self.configuration
        if len(path) > 1: return super(FirewallConfFile, self).lget(path)
        return self.configuration[path[0]] and self.configuration[path[0]] or None

    def lset(self, path, value):
        if len(path) != 1: return super(FirewallConfFile, self).lset(path, value)
        self.configuration[path[0]] = value
        self._commit()

    def ldelete(self, path):
        if len(path) != 1: return super(FirewallConfFile, self).ldelete(path)
        del(self.configuration[path[0]])
        self._commit()

    def lcreate(self, path, value=None):
        self.lset(path, value)
        return self.lget(path)

    def llist(self, path):
        if not path:
            k = list(self.configuration.keys())
            k.sort()
            return k
        else:
            return []

class FirewallServiceConfFile(octofuss.Tree):
    def __init__(self, conf_file, name = None):
        super(FirewallServiceConfFile, self).__init__(name, doc=files_doc.get(name, None))
        # we use a generic file
        self.conf_file = conf_file
        self._load()

    def _load(self):
        self.configuration = {}
        self.header_comments = []
        f = open(self.conf_file)
        for line in f:
            if line.startswith("#"):
                self.header_comments.append(line)
                continue
            r = line.strip().split(":")
            host = r[0]
            if "/" in host:
                host = host.replace("/","-")
            if len(r) == 1:
                desc = ""
            else:
                desc = r[1]

            self.configuration[host] = desc
        f.close()

    def _commit(self):
        f = open(self.conf_file, "w")
        for line in self.header_comments:
            f.write(line)
        for k in list(self.configuration.keys()):
            key = k
            if "-" in key:
                key = key.replace("-","/")
            f.write("%s:%s\n" % (key, self.configuration[k]))
        f.close()
        self._load()

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) > 1: return False
        return path[0] in self.configuration

    def lget(self, path):
        if len(path) == 0: return self.configuration
        if len(path) > 1: return super(FirewallServiceConfFile, self).lget(path)
        # when editing firewall-allowed-wan-host-services the original
        # return fails because _load doesn't read back the right value,
        # but everything otherwise works.
        return self.configuration.get(path[0], None)
        return self.configuration[path[0]] and self.configuration[path[0]] or None

    def lset(self, path, value):
        if len(path) != 1: return super(FirewallServiceConfFile, self).lset(path, value)
        self.configuration[path[0]] = value
        self._commit()

    def ldelete(self, path):
        if len(path) != 1: return super(FirewallServiceConfFile, self).ldelete(path)
        del(self.configuration[path[0]])
        self._commit()

    def lcreate(self, path, value=None):
        print(path)
        new_path = path[0]
        if "-" not in new_path:
            new_path = new_path + "-tcp"
        new_path = [new_path]
        self.lset(new_path, value)
        return self.lget(new_path)

    def llist(self, path):
        if not path:
            k = list(self.configuration.keys())
            k.sort()
            return k
        else:
            return []

class MockFirewallConfFile(octofuss.DictTree):
    def __init__(self, conf_file, name = None, ):
        super(MockFirewallConfFile, self).__init__(name, doc=files_doc.get(name, None))
        self.conf_file = conf_file
        self["www.example.org"] = "This is an example"

firewall_files = {
    "denied_lan_hosts": "/etc/fuss-server/firewall-denied-lan-hosts",
    "allowed_lan_hosts": "/etc/fuss-server/firewall-allowed-lan-hosts",
    "allowed_wan_hosts": "/etc/fuss-server/firewall-allowed-wan-hosts",
}
firewall_services_files = {
    "allowed_wan_host_services": "/etc/fuss-server/firewall-allowed-wan-host-services",
    "allowed_wan_services": "/etc/fuss-server/firewall-allowed-wan-services",
    "external_services": "/etc/fuss-server/firewall-external-services"
}


def init(**kw):
    # Access configuration
    conf = kw.get("conf", None)
    canMock = conf and conf.getboolean("General", "allowMockup")
    forceMock = conf and conf.getboolean("General", "forceMockup")

    top = octofuss.Forest("firewall", doc="Firewall controls")
    topconf = octofuss.Forest("conf", doc="Firewall configuration")
    top.register(topconf)

    for conf_file in list(firewall_files.keys()):
        if not forceMock and os.path.exists(firewall_files[conf_file]):
            topconf.register(FirewallConfFile(firewall_files[conf_file], conf_file))
        elif canMock or forceMock:
            topconf.register(MockFirewallConfFile(firewall_files[conf_file], conf_file))

    for conf_file in list(firewall_services_files.keys()):
        if not forceMock and os.path.exists(firewall_services_files[conf_file]):
            topconf.register(FirewallServiceConfFile(firewall_services_files[conf_file], conf_file))
        elif canMock or forceMock:
            #topconf.register(MockFirewallServiceConfFile(firewall_files[conf_file], conf_file))
            pass

    if not forceMock:
        top.register(FirewallRestart("restart"))
    else:
        top.register(MockFirewallRestart("restart"))

    top.register(AvailableServices("available_services"))

    yield dict(tree = top,root = "/")
