import octofuss
import configparser
from urllib.parse import quote_plus, unquote_plus

class Samba(octofuss.Tree):
    keyword_doc = {
        "path": "path to be shared",
        "readonly": "if true, this share will not allow writing",
        "guestok": "if true, this share can be used by anyone",
        "readlist": "groups that are allowed to read from this share",
        "writelist": "groups that are allowed to write to this share",
    }

    def __init__(self, name, filename=None):
        """
        name: tree name
        filename: the configuration file name. If None, a mock, in memory
                  configuration is used
        """
        super(Samba, self).__init__(name, doc="Configuration of the Samba server")
        self.filename = filename
        self.conf = None

    def _checkConfig(self):
        self.conf = configparser.ConfigParser()
        if self.filename != None:
            self.conf.read(self.filename)
        else:
            self.conf.read_string("""
            [cdrom]
            path=/cdrom
            read only=yes
            guest ok=yes
            write list=@wizards

            [public]
            path=/public
            read only=no
            guest ok=no

            [news area]
            path=/news
            read only=yes
            write list=@editors

            [cabal]
            path=/cabal
            read only=yes
            read list=@cabal
            write list=@cabal
            """)


    def _commit(self):
        if self.filename != None:
            self.conf.write(open(self.filename, "w"))

    def _service(self, name):
        """
        Return a dict of key=value for a smb.conf section, with
        lowercased section name and keys
        """
        res = self.conf.items(name)
        service = dict()
        for x in res:
            service[unquote_plus(x[0])] = x[1]
        return service

    def lhas(self, path):
        self._checkConfig()
        if len(path) == 0: return True
        name = unquote_plus(path[0])
        if not self.conf.has_section(name): return False
        if len(path) == 1: return True
        if len(path) > 2: return False

    def lget(self, path):
        self._checkConfig()
        if len(path) == 0:
            # Convert service names and keys to lower case
            res = dict()
            for sname in self.conf.sections():
                res[sname.lower()] = self._service(sname)
            return res
        name = unquote_plus(path[0])
        if name not in self.conf.sections(): return False
        if len(path) > 2: return None
        sdict = self._service(name)
        if len(path) == 1: return sdict
        return sdict.get(path[1], None)

    def llist(self, path):
        self._checkConfig()
        if len(path) == 0: return sorted([quote_plus(x.lower()) for x in self.conf.sections()])
        name = unquote_plus(path[0])
        if name not in self.conf.sections(): return []
        if len(path) > 1: return []
        return sorted([unquote_plus(x.lower()) for x in self._service(name).keys()])

    def lset(self, path, value):
        # print("lset()", "path=", path, type(path), "value=", value, type(value))
        self._checkConfig()
        if len(path) == 0 or len(path) > 2:
            return super(Samba, self).lset(path, value)
        name = unquote_plus(path[0])
        if name not in self.conf.sections():
            return super(Samba, self).lset(path, value)
        if len(path) == 1:
            # Follow the form field default in Octonet for the default here
            guestok = value.get('guestok', "no")
            # Follow the form field default in Octonet for the default here
            readonly = value.get('readonly', "yes")
            readlist = value.get('readlist', "")
            writelist = value.get('writelist', "")
            # print("BEFORE", name, readlist, writelist)
            new_list = []
            for group in readlist.split(","):
                new_list.append(group.replace("@", "").replace("+", " ").strip())
            readlist = ",".join(new_list)
            new_list = []
            for group in writelist.split(","):
                new_list.append(group.replace("@", "").replace("+", " ").strip())
            writelist = ",".join(new_list)
            # print("AFTER", name, readlist, writelist)
            for k, v in list(value.items()):
                self.conf.set(name, k, v)
                # refs #14440
                # Set the permissions on the share directory
                octofuss.hooks.invoke(
                    "set_share_permissions",
                    name,
                    guestok,
                    readonly,
                    readlist,
                    writelist,
                )
            self._commit()
        else:
            self.conf.set(name, path[1], value)
            self._commit()

    def lcreate(self, path, value):
        # print("lcreate()", "path=", path, type(path), "value=", value, type(value))
        self._checkConfig()
        if len(path) == 0 or len(path) > 2:
            return super(Samba, self).lcreate(path, value)
        name = unquote_plus(path[0])
        if name not in self.conf.sections():
            self.conf.add_section(name)
        if len(path) == 1:
            if value == None or value == "": value = {}
            for k, v in list(value.items()):
                self.conf.set(name, k, v)
            self._commit()
            # refs #14440
            # Create the directory on the server
            # Clean and pass all the needed data to the script
            # Follow the form field default in Octonet for the default here
            guestok = value.get('guestok', "no")
            # Follow the form field default in Octonet for the default here
            readonly = value.get('readonly', "yes")
            readlist = value.get('readlist', "")
            writelist = value.get('writelist', "")
            # print("BEFORE", share, readlist, writelist)
            new_list = []
            for group in readlist.split(","):
                new_list.append(group.replace("@", "").replace("+", " ").strip())
            readlist = ",".join(new_list)
            new_list = []
            for group in writelist.split(","):
                new_list.append(group.replace("@", "").replace("+", " ").strip())
            writelist = ",".join(new_list)
            # print("AFTER", share, readlist, writelist)
            if name:
                # print(share, '"%s"' % share)
                octofuss.hooks.invoke(
                    "create_share_directory",
                    name,
                    guestok,
                    readonly,
                    readlist,
                    writelist,
                )
        else:
            self.conf.set(name, path[1], value)
            self._commit()

    def ldelete(self, path):
        self._checkConfig()
        if len(path) == 0 or len(path) > 2:
            return super(Samba, self).ldelete(path)
        name = unquote_plus(path[0])
        if name not in self.conf.sections():
            return super(Samba, self).ldelete(path)
        if len(path) == 1:
            self.conf.remove_section(name)
            self._commit()
        else:
            pass

    def ldoc(self, path):
        self._checkConfig()
        if len(path) == 0: return "Samba share configuration"
        name = unquote_plus(path[0])
        if name not in self.conf.sections(): return None
        if len(path) > 2: return None
        if len(path) == 1:
            return "Configuration for share %s" % unquote_plus(name)
        return self.keyword_doc.get(path[1], None)


def init(**kw):
    conf = kw.get("conf", None)
    canMock = conf and conf.getboolean("General", "allowMockup")
    forceMock = conf and conf.getboolean("General", "forceMockup")
    fname = None
    if not forceMock:
        fname = "/etc/samba/shares.conf"
    elif forceMock or canMock:
        fname = None
    yield dict(tree = Samba("samba", fname), root="/")
