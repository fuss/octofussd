# coding: utf-8
import locale
import os
import random
import subprocess
import octofuss


REPQUOTA="/usr/sbin/repquota"
QUOTATOOL="/usr/sbin/quotatool"
FSTAB="/etc/fstab"


class QuotaTree(octofuss.Tree):
    keyword_doc = {
        "disk_used": "number of disk blocks in use",
        "disk_soft": "soft limit for disk blocks used",
        "disk_hard": "hard limit for disk blocks used",
        "file_used": "number of files in use",
        "file_soft": "soft limit for files used",
        "file_hard": "hard limit for files used",
    }
    def __init__(self, name, type=None, doc=None):
        type = type or name
        doc = doc or "%s quota" % type
        super(QuotaTree, self).__init__(name, doc=doc)
        self.type = type
        if self.type not in ["user", "group"]:
            raise ValueError("Unsupported quota type: %s" % self.type)

    def ldoc(self, path):
        if len(path) == 0: return "%s quota information" % self.type
        if not self.tree.lhas([path[0]]): return False
        if len(path) > 2: return None
        if len(path) == 1: return "Quota information for %s %s" % (self.type, path[0])
        return self.keyword_doc.get(path[1], None)


class MockQuota(QuotaTree):
    keywords = ["disk_used", "disk_soft", "disk_hard", "file_used", "file_soft", "file_hard"]
    set_keywords = ["disk_soft", "disk_hard", "file_soft", "file_hard"]

    def __init__(self, name, tree,
                    def_disk_soft = 20000, def_disk_hard = 30000,
                    def_file_soft = 100000, def_file_hard = 200000):
        """
        name: tree name
        tree: subtree that provides the item list (via a simple list(""))
        def_disk_soft: default block soft quota limit for all items
        def_disk_hard: default block hard quota limit for all items
        def_file_soft: default inode soft quota limit for all items
        def_file_hard: default inode hard quota limit for all items
        """
        super(MockQuota, self).__init__(name)
        self.tree = tree
        self.quotas = dict()
        self.def_disk_soft = def_disk_soft
        self.def_disk_hard = def_disk_hard
        self.def_file_soft = def_file_soft
        self.def_file_hard = def_file_hard

    def _quota_for_item(self, name):
        if name not in self.quotas:
            self.quotas[name] = dict(
                name = name,
                disk_used = random.randint(0, self.def_disk_hard),
                disk_soft = self.def_disk_soft,
                disk_hard = self.def_disk_hard,
                file_used = random.randint(0, self.def_file_hard),
                file_soft = self.def_file_soft,
                file_hard = self.def_file_hard,
            )
        return self.quotas[name]

    def _set_quota_for_item(self, name, value):
        cur = self._quota_for_item(name)
        for k in self.set_keywords:
            if k in value:
                cur[k] = int(value[k])
        self.quotas[name] = cur
        return cur

    def lhas(self, path):
        if len(path) == 0: return True
        if not self.tree.lhas([path[0]]): return False
        if len(path) == 1: return True
        if len(path) > 2: return False
        return path[1] in self.keywords

    def lget(self, path):
        if len(path) == 0: return [self._quota_for_item(x) for x in self.tree.llist([])]
        if not self.tree.lhas([path[0]]): return False
        if len(path) > 2: return None
        q = self._quota_for_item(path[0])
        if len(path) == 1: return q
        if path[1] in q: return q[path[1]]
        return None

    def llist(self, path):
        if len(path) == 0: return self.tree.llist([])
        if not self.tree.lhas([path[0]]): return []
        if len(path) > 1: return []
        return self.keywords

    def lset(self, path, value):
        if len(path) == 0 or len(path) > 2 or not self.tree.lhas([path[0]]):
            return super(MockQuota, self).lset(path, value)
        if len(path) == 1:
            return self._set_quota_for_item(path[0], value)
        if path[1] not in self.set_keywords:
            return super(MockQuota, self).lset(path, value)
        return self._set_quota_for_item(path[0], {path[1]:value})


class Quota(QuotaTree):
    keywords = ["disk_used", "disk_soft", "disk_hard", "file_used", "file_soft", "file_hard"]
    set_keywords = ["disk_soft", "disk_hard", "file_soft", "file_hard"]

    def __init__(self, name, fs, type = None):
        """
        name: tree name
        fs: file system to query
        """
        super(Quota, self).__init__(name, type)
        self.fs = fs
        if self.type == "user":
            self.repquota = [REPQUOTA, "-u", "-p"]
            self.quotatoolsel = "-u"
        elif self.type == "group":
            self.repquota = [REPQUOTA, "-g", "-p"]
            self.quotatoolsel = "-g"

    def _quota_for_item(self, name):
        #            |------- BLOCKS --------| |-------- FILES --------|
        # uid/gid mountpoint current quota limit grace current quota limit grace
        proc = subprocess.Popen([QUOTATOOL, "-d", self.quotatoolsel, name, self.fs], stdout=subprocess.PIPE)
        out = proc.communicate()[0]
        res = proc.wait()
        if res != 0: return None
        vals = list(map(int, out.split()[2:]))
        return dict(
            name=name,
            disk_used = vals[0],
            disk_soft = vals[1],
            disk_hard = vals[2],
            file_used = vals[4],
            file_soft = vals[5],
            file_hard = vals[6],
        )

    def _quota_for_items(self):
        # *** Report for user quotas on device /dev/sda1
        # Block grace time: 7days; Inode grace time: 7days
        #                         Block limits                File limits
        # User            used    soft    hard  grace    used  soft  hard  grace
        # ----------------------------------------------------------------------
        proc = subprocess.Popen(self.repquota + [self.fs], stdout=subprocess.PIPE)
        out = proc.communicate()[0]
        res = proc.wait()
        if res != 0: return None
        # refs #227
        # Make the mock and real octofussd coherent: list and not dict
        res = []

        # refs #227
        # https://work.fuss.bz.it/issues/227
        # http://stackoverflow.com/a/15817457
        encoding = locale.getdefaultlocale()[1]
        for line in out.decode(encoding).split("\n")[5:]:
            line = line.strip()
            if len(line) == 0: continue
            # Skip users not in passwd, since quotatool does not work with them
            if line[0] == "#": continue
            if line[0] == "-": continue
            tries = [line.split("--"),
                     line.split("-+"),
                     line.split("++"),
                     line.split("+-")]
            name = ''
            vline = ''
            for x in tries:
                if len(x) > 1:
                    name = x[0].strip()
                    vline = x[1].strip()
                    break

            vals = vline.split()
            vals = list(map(int, vals[:7]))
            # refs #227
            res.append(dict(
                name = name,
                disk_used = vals[0],
                disk_soft = vals[1],
                disk_hard = vals[2],
                file_used = vals[4],
                file_soft = vals[5],
                file_hard = vals[6],
            ))
        return res

    def _set_quota_for_item(self, name, value):
        cmd = [QUOTATOOL, self.quotatoolsel, name]
        if "disk_soft" in value or "disk_hard" in value:
            cmd += ["-b"]
            if "disk_soft" in value:
                cmd += ["-q", str(value["disk_soft"])]
            if "disk_hard" in value:
                cmd += ["-l", str(value["disk_hard"])]
        if "file_soft" in value or "file_hard" in value:
            cmd += ["-i"]
            if "file_soft" in value:
                cmd += ["-q", str(value["file_soft"])]
            if "file_hard" in value:
                cmd += ["-l", str(value["file_hard"])]
        cmd += [self.fs]
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        res = proc.wait()
        if res != 0:
            err = stderr and stderr.split("\n") or ""
            msg = "Cannot set quota for " + name + " on " + self.fs
            if len(err) >= 0:
                msg += ": " + err[0].strip()
            raise RuntimeError(msg)
        return None

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) > 2: return False
        info = self._quota_for_item(path[0])
        if not info: return False
        if len(path) == 1: return True
        return path[1] in info

    def lget(self, path):
        if len(path) == 0: return self._quota_for_items()
        if len(path) > 2: return None
        info = self._quota_for_item(path[0])
        if not info: return None
        if len(path) == 1: return info
        return info.get(path[1], None)

    def llist(self, path):
        if len(path) == 0: return sorted(self._quota_for_items().keys())
        if len(path) > 1: return []
        info = self._quota_for_item(path[0])
        if not info: return []
        return sorted(info.keys())

    def lset(self, path, value):
        if len(path) == 0 or len(path) > 2:
            return super(MockQuota, self).lset(path, value)
        if len(path) == 1:
            return self._set_quota_for_item(path[0], value)
        if path[1] not in self.set_keywords:
            return super(MockQuota, self).lset(path, value)
        return self._set_quota_for_item(path[0], {path[1]:value})


def find_quota_fs():
    fs = []
    if os.path.isfile(FSTAB):
        for line in open(FSTAB):
            line = line.strip()
            if len(line) > 0:
                if line.startswith("#"):
                    continue
                if 'usrquota' in line and 'grpquota' in line:
                    fs.append(line.split()[1])
                elif 'usrjquota' in line and 'grpjquota' in line:
                    fs.append(line.split()[1])
    return fs


def mangle_fs_names(mountpoints):
    seen = set()
    res = []
    for m in mountpoints:
        # Turn it into a name that fits in the octofuss tree
        name = m.strip("/").replace("/", "-")
        if name == "": name = "root"
        # Prevent collisions after mangling
        cand = name
        idx = 0
        while cand in seen:
            idx += 1
            cand = name + str(idx)
        res.append(cand)
        seen.add(cand)
    return res


def init(**kw):
    tree = kw.get("tree", None)
    conf = kw.get("conf", None)
    canMock = conf and conf.getboolean("General", "allowMockup")
    forceMock = conf and conf.getboolean("General", "forceMockup")

    if forceMock:
        filesystems = []
    else:
        filesystems = find_quota_fs()

    top = octofuss.Forest("quota", doc="Quota configuration")

    if len(filesystems) == 0:
        if not(forceMock or canMock):
            return
        filesystems = [ "/", "/root", "/home" ]
        # Mock quota implementation
        def attach(fs, name, branch):
            branch.register(MockQuota("user", octofuss.Subtree(tree, "/users/users")))
            branch.register(MockQuota("group", octofuss.Subtree(tree, "/users/groups")))
    else:
        # Real quota implementation
        def attach(fs, name, branch):
            branch.register(Quota("user", fs))
            branch.register(Quota("group", fs))

    names = mangle_fs_names(filesystems)
    for fs, name in zip(filesystems, names):
        branch = octofuss.Forest(name, doc="Quota configuration for file system '%s'" % name)
        attach(fs, name, branch)
        top.register(branch)
        yield dict(tree = top, root="/")
