#!/usr/bin/env python3

from passlib import hash
alg = {
    'ssha':'Seeded SHA-1',
    'sha':'Secure Hash Algorithm',
    'smd5':'Seeded MD5',
    'md5':'MD5',
    'ssha512':'Seeded SHA-512',
    'crypt':'Standard unix crypt',
    'ldapcrypt':'LDAP unix crypt'
    }

def mkpasswd(pwd,hash_type='ldapcrypt'):
    if hash_type not in list(alg.keys()):
        return None
    else:
        h = None
        if hash_type == "ssha":
            h = hash.ldap_salted_sha1.encrypt(pwd, salt_size=16)
        elif hash_type == "sha":
            h = hash.ldap_sha1.encrypt(pwd)
        elif hash_type == "md5":
            h = hash.ldap_md5.encrypt(pwd)
        elif hash_type == "smd5":
            h = hash.ldap_salted_md5.encrypt(pwd, salt_size=16)
        elif hash_type == "crypt":
            h = hash.ldap_bcrypt.encrypt(pwd)
        elif hash_type == "ldapcrypt":
            h = hash.ldap_sha512_crypt.encrypt(pwd,rounds=5000)
        elif hash_type == "ssha512":
            h = hash.ldap_salted_sha512.encrypt(pwd)

        return h


if __name__ == "__main__":
    read_pwd = input("Password? ")
    for x in list(alg.keys()):
        print(read_pwd, "hashed in", x, "is", mkpasswd(read_pwd, x))
