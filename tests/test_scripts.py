import os.path
import unittest
from tests.test_base import OctofussdTestMixin, PREFIX
MAX_SCRIPTS = 3

def _script(num):
    if num >= MAX_SCRIPTS:
        raise ValueError("Attempt to generate script name #%d that will not be cleaned" % num)
    return PREFIX+"script"+str(num)

class TestScripts(OctofussdTestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        for i in range(MAX_SCRIPTS):
            if self.tree.has("/scripts/"+_script(i)):
                self.tree.delete("/scripts/"+_script(i))
                
    def testCreateScript(self):
        script = _script(0)
        scriptpath = "/scripts/" + script
        self.assertNotHas(scriptpath)
        self.tree.create(scriptpath)
        self.assertInTree(scriptpath)
        script_content_path = os.path.join(scriptpath, "script")
        self.tree.set(script_content_path, "foo")
        script_val = self.tree.get(script_content_path)
        self.assertEqual(script_val, "foo")

    def testDeleteScript(self):
        script = _script(0)
        scriptpath = "/scripts/" + script
        self.assertNotHas(scriptpath)
        self.tree.create(scriptpath)
        self.assertInTree(scriptpath)
        self.tree.delete(scriptpath)
        self.assertNotInTree(scriptpath)
