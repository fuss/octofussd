import octofuss
import os, os.path
from configparser import ConfigParser


SIP_CONF_FILE = "/etc/asterisk/sip.conf"
MOCK_SIP_CONF_FILE = "/tmp/sip.conf"


# Plugin implementation
class AsteriskSIP(octofuss.Tree):
    conf_file = SIP_CONF_FILE

    def __init__(self, name = None):
        super(AsteriskSIP, self).__init__(name, doc="Asterisk SIP Extension configurator")
        self.extensions = None
        self._load_data()

    def _load_data(self):
        self.extensions = ConfigParser()
        self.extensions.read(self.conf_file)

    def _commit(self):
        self.extensions.write(open(self.conf_file, 'w'))

    def lhas(self, path):
        if len(path) == 1:
            return self.extensions.has_section(path[0])
        elif len(path) == 2:
            return self.extensions.has_option(path[0], path[1])
        else:
            return False

    def lget(self, path):
        if len(path) == 1:
            i = self.extensions.items(path[0])
            r = {}
            for x in i:
                r[x[0]] = x[1]
            return r

        elif len(path) == 2:
            return self.extensions.get(path[0], path[1])

        else:
            return False

    def lset(self, path, value):
        if len(path) != 2: return super(AsteriskSIP, self).lset(path, value)
        self.extensions.set(path[0], path[1], value)
        self._commit()

    def lcreate(self, path, value=None):
        if len(path) != 1: return super(AsteriskSIP, self).lset(path, value)
        if self.extensions.has_section(path[0]):
            raise Exception("Name already exists.")

        try:
            self.extensions.add_section(path[0])
            self.extensions.set(path[0], "username", path[0])
            self.extensions.set(path[0], "call-limit", "10")
            self.extensions.set(path[0], "host", "dynamic")
            self.extensions.set(path[0], "pickupgroup", "1")
            self.extensions.set(path[0], "mailbox", "01")
            self.extensions.set(path[0], "secret", path[0])
            self.extensions.set(path[0], "context", "lanscuola")
            self.extensions.set(path[0], "callgroup", "1")
            self.extensions.set(path[0], "type", "friend")
            self._commit()
        except:
            # what kind of exception here?
            return None
        return self.lget(path)

    def ldelete(self, path):
        if len(path) == 1:
            self.extensions.remove_section(path[0])
            self._commit()

    def llist(self, path):
        if len(path) == 0:
            r = self.extensions.sections()
            r.sort()
            return r
        elif len(path) == 1:
            if self.extensions.has_section(path[0]):
                return self.extensions.options(path[0])
        return []

class MockAsteriskSIP(AsteriskSIP):
    conf_file = MOCK_SIP_CONF_FILE
    def __init__(self, name = None):
        super(MockAsteriskSIP, self).__init__(name)

def init(**kw):
    conf = kw.get("conf", None)
    canMock = conf and conf.getboolean("General", "allowMockup")
    forceMock = conf and conf.getboolean("General", "forceMockup")

    if os.path.isfile(SIP_CONF_FILE):
        yield dict(
            tree = AsteriskSIP("sip"),
            root = "/asterisk"
            )
    elif canMock or forceMock:
        yield dict(
            tree = MockAsteriskSIP("sip"),
            root = "/asterisk"
            )
