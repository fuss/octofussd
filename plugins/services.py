import octofuss
import os, os.path
from gettext import gettext as _
from octofussd import system


possible_services = {
    'chilli': (_("Captive portal access controller"), "restart"),
    'cron': (_("Time-based job scheduler"), "restart"),
    'cups': (_("Common Unix Printing System"), "restart"),
    'e2guardian' : (_("Content filter"), "reload"),
    'firewall' : (_("Firewall"), "restart"),
    'isc-dhcp-server': (_("DHCP server"), "restart"),
    'krb5-kdc': (_("Kerberos KDC server"), "restart"),
    # refs #14653
    #'nfs-common' : (_("Network File System - Common stack"), "restart"),
    'nfs-kernel-server' : (_("Network File System - Server"),"restart"),
    'nscd' : (_("Name service cache"), "restart"),
    'slapd': (_("OpenLDAP server"), "restart"),
    'squid3' : (_("Web Proxy/Cache"), "reload"),
    'tftpd-hpa': (_("TFTP server (network boot)"), "restart"),
    # refs #13826
    'propagate-net-perm': (_("Propagate network access permission"), "restart"),
    }

# Plugin implementation
class Services(octofuss.Tree):
    def __init__(self, name = None, **kw):
        super(Services, self).__init__(name, doc="""Service restart facility

This node lists the services in the system that can be restarted.

Set a service to any value to restart it.""")
        self.conf = kw.get("conf", {})
        self.script_dir = '/etc/init.d'
        self.mock = self.conf and self.conf.getboolean("General", "forceMockup")

    def _enabled(self, name):
        return name in possible_services and os.path.isfile(os.path.join(self.script_dir, name))

    def lhas(self, path):
        if len(path) == 0: return True
        if len(path) > 1: return False
        return self._enabled(path[0])

    def lget(self, path):
        if len(path) == 0:
            return dict([i for i in possible_services.items() if self._enabled(i[0])])
        if len(path) > 1: return None
        if not self._enabled(path[0]): return None
        return possible_services[path[0]]

    def llist(self, path):
        if len(path) != 0: return []
        return [x for x in list(possible_services.keys()) if self._enabled(x)]

    def lset(self, path, value):
        if len(path) != 1: return super(Services, self).lset(path, value)
        if not self._enabled(path[0]): return None
        if not self.mock:
            return system.restart_service(path[0])


    def ldoc(self, path):
        if len(path) == 0: return super(Services, self).ldoc(path)
        if len(path) > 1: return None
        return possible_services.get(path[0], None)


def init(**kw):
    conf = kw.get("conf", None)
    yield dict(
            tree = Services("services", conf=conf),
            root = "/"
            )
