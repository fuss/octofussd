from twisted.web import resource, server, static, xmlrpc
from twisted.python import reflect
import os.path, os

class FussClientDeployer(xmlrpc.XMLRPC):

    def __init__(self, service):
        xmlrpc.XMLRPC.__init__(self)
        self.service = service

    def render(self,request):
        self.client = request.getClientIP()
        return xmlrpc.XMLRPC.render(self,request)

    def xmlrpc_get_conf(self):
        """Give a client machine several configuration
        possibilities, such as user authentication, network
        share and such. It's just the client that choose which
        data are relevant and how to use them.
        """

        r = {}
        functions = reflect.prefixedMethodNames(self.__class__, 'deploy_')
        for f in functions:
            try:
                val = getattr(self, "deploy_%s" %f , None)()
                if val:
                    r[f] = val
            except Exception as e:
                print("ERROR GETTING", f, ":", e)
                # can't get data, what to do?
                pass
        return r

    def _get_domain(self):
        lines = open("/etc/fuss-server/fuss-server.yaml").readlines()
        domain = None
        for line in lines:
            if "domain:" in line:
                domain = line.split(":")[1].strip()
                break
        return domain

    def deploy_domain_name(self):
        return self._get_domain()
    
    def deploy_ldap_base(self):
        p = os.popen('ldapsearch -x -s base -b "" +', "r")
        data = p.readlines()
        p.close()
        base = None
        for line in data:
            if "namingContexts" in line:
                base = line.split(":")[1].strip()
                return base
        if not base:
            raise Exception("Can't find any LDAP base")

    def deploy_ldap_server(self):
        # try to see if we've a fuss server here
        server_uri = None
        domain = self._get_domain()
        if not domain:
            raise Exception("Can't find LAN domain")

        host = os.popen("hostname","r").read().strip()
        if host:
            server_uri = host + "." + domain

        if not server_uri:
            raise Exception("Can't find any LDAP server")

        return server_uri

    def deploy_root_ssh_key(self):
        f = open("/root/.ssh/id_rsa.pub")
        return f.read()

    def deploy_ssl_cacert(self):
        f = open("/etc/ssl/certs/{}-cacert.pem".format(self._get_domain()))
        data = f.read()
        f.close()
        return data
