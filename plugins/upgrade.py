import octofuss
from octofuss.treeextra import ListTree
import octofussd.data.models as model
import datetime


def ser_dt(dt):
    "Serialise a datetime to a string"
    if dt is None: return None
    return dt.strftime("%Y-%m-%d %H:%M:%S")

class Upgrades(ListTree):
    def __init__(self, name = None):
        super(Upgrades, self).__init__(Upgrade, name, doc="List of upgrade operations")

    def child_data_by_name(self, name):
        return model.Upgrade.objects.get(name=name)

    def list_child_data(self):
        return model.Upgrade.objects.all()

    def create_child_data(self, name):
        obj, created = model.Upgrade.objects.get_or_create(name=name)
        if not created:
            raise ValueError("upgrade " + name + " already exists")
        return obj

    def delete_child_data(self, data):
        data.delete()

class UpgradeHosts(ListTree):
    def __init__(self, upg):
        super(UpgradeHosts, self).__init__(UpgradeHost, upg.name, doc="Hosts to upgrade.\n\nList of hosts in which the upgrade " + upg.name + " has been scheduled")
        self.upg = upg

    def child_data_by_name(self, name):
        res = model.UpgradeHost.objects.filter(upgrade=self.upg,
                                               name=name)
        if res.count() == 0:
            return None
        else:
            return res[0]

    def list_child_data(self):
        return model.UpgradeHost.objects.filter(upgrade=self.upg)

    def create_child_data(self, name):
        obj, created = model.UpgradeHost.objects.get_or_create(upgrade=self.upg,
                                                               name=name)
        if not created:
            raise ValueError("upgrade host " + name + " already exists")
        return obj

    def delete_child_data(self, data):
        data.delete()

class Upgrade(octofuss.PyTree):
    def __init__(self, upg):
        name = upg.name
        super(Upgrade, self).__init__(name, doc="Ugprade operation " + name)
        self.upg = upg
        self.hosts = UpgradeHosts(upg)

    def _get(self, **kw):
        return {
                "name": self.upg.name,
                "type": self.upg.type,
                "scheduled": ser_dt(self.upg.scheduled),
                "completed": ser_dt(self.upg.completed),
                "hosts": self.hosts.lget([], **kw)
        }

    def _set_scheduled(self, val):
        if val:
            self.upg.scheduled = datetime.datetime.utcnow()
        else:
            self.upg.scheduled = None
        self.upg.save()

    def _set_type(self, val):
        self.upg.type = val
        self.upg.save()

    type = property(
            lambda self: self.upg.type,
            _set_type,
            doc="Upgrade type.\n\nWhat kind of upgrade to perform: 'upgrade' or 'dist-upgrade'")
    scheduled = property(
            lambda self: ser_dt(self.upg.scheduled),
            _set_scheduled,
            doc="Scheduling status.\n\nIf the update has been scheduled, it is the timestamp of when it has been scheduled; if the upgrade is still in preparation, it is the empty string. Set to any value to schedule using the current time, set to no value to clear.""")
    completed = property(
            lambda self: ser_dt(self.upg.completed),
            lambda self, val: val,
            doc="""Completion time.\n\nIf the update has completed on all hosts, it is the timestamp of the completion time of the last host; if the upgrade has not completed yet, it is the empty string""")

class UpgradeHost(octofuss.PyTree):
    def __init__(self, uh):
        name = uh.name
        super(UpgradeHost, self).__init__(name, doc="Host " + uh.name + " in upgrade operation " + uh.upgrade.name)
        self.uh = uh

    def _get(self, **kw):
        if kw.get("short", False):
            return {
                "name": self.uh.name,
                "started": ser_dt(self.uh.started),
                "ended": ser_dt(self.uh.ended),
                }
        else:
            return {
                "name": self.uh.name,
                "started": ser_dt(self.uh.started),
                "ended": ser_dt(self.uh.ended),
                "output": self.uh.output
                }

    started = property(
                    lambda self: ser_dt(self.uh.started),
                    doc="Time the upgrade was started")
    ended = property(
                    lambda self: ser_dt(self.uh.ended),
                    doc="Time the upgrade was ended")
    output = property(
                    lambda self: self.uh.output,
                    doc="Annotated output of all the processes run during the upgrade")

def init(**kw):
    yield dict(
            tree = Upgrades("upgrades"),
            root = "/"
            )
