# -*- coding: utf-8 -*-
import os, os.path
import time
import passlib.hash
import octofuss
import subprocess
from octofussd.data.models import Users as UserLog
from urllib.parse import quote_plus, unquote_plus
import re
from . import hashpwd

userDB = None
tree = None

_ = lambda x:x

class UserList(octofuss.DynamicView):
    def __init__(self, name, doc="List of users in the system"):
        super(UserList, self).__init__(name, doc)
        #self.specials[":search:"] = UserSearch()

    def users(self):
        """
        Generate a sequence of hosts
        """
        return userDB.list()

    def elements(self):
        return self.users()

    def element(self, name):
        return userDB.user(name)

    def new_element(self, name, value):
        # Ensure that name is the quoted version of the uid
        if value == "" or value == None: value = {}
        if "uid" not in value:
            value["uid"] = unquote_plus(name)
        elif quote_plus(value["uid"]) != name:
            raise ValueError("The user name in the tree (%s) should match the escaped uid (%s)" % (name, quote_plus(value["uid"])))
        u = userDB.new_user(value)
        return u.lget([])

    def delete_element(self, name):
        userDB.delete(unquote_plus(name))


class MemberList(octofuss.DynamicView):
    def __init__(self, name, groupName):
        super(MemberList, self).__init__(name, doc="Members of group {}".format(groupName))
        self.groupName = groupName
        #self.specials[":search:"] = UserSearch(baseSearch=["group:"+groupName])

    def users(self):
        """
        Generate a sequence of groups
        """
        return userDB.list(self.groupName)

    def elements(self):
        return self.users()

    def element(self, name):
        """
        Return the user "name" but only if it is a member of this group
        """
        return userDB.user_in_group(unquote_plus(name), self.groupName)

    def new_element(self, name, value):
        user, group = userDB.add_user_to_group(unquote_plus(name), self.groupName)
        if "docent" in self.groupName:
            user.data.ou = "docenti"
            # refs #12213
            user.data.shadowMax = "90"
            user.commit()
        return user.lget([])

    def delete_element(self, name):
        if "docent" in self.groupName:
            user = userDB.user(name)
            user.data.ou = "studenti"
            user.data.shadowMax = "99999"
            user.commit()

        userDB.delete_user_from_group(unquote_plus(name), self.groupName)

class UserGroupList(octofuss.DynamicView):
    def __init__(self, name, userName):
        super(UserGroupList, self).__init__(name, doc="Groups of user " + userName)
        self.userName = userName

    def groups(self):
        """
        Generate a sequence of groups
        """
        return userDB.groups_of_user(self.userName)

    def elements(self):
        return self.groups()

    def element(self, name):
        """
        Return the group "name" but only if this user is a member of it
        """
        return userDB.group_in_user(self.userName, unquote_plus(name))

    def new_element(self, name, value):
        user, group = userDB.add_user_to_group(self.userName, unquote_plus(name))
        if "docent" in name:
            user.data.ou = "docenti"
            # refs #12213
            user.data.shadowMax = "90"
            user.commit()

        return group.lget([])

    def delete_element(self, name):
        if "docent" in name:
            user = userDB.user(self.userName)
            user.data.ou = "studenti"
            user.data.shadowMax = "99999"
            user.commit()
        userDB.delete_user_from_group(self.userName, unquote_plus(name))

class GroupList(octofuss.DynamicView):
    def __init__(self, name, doc="List of groups in the system"):
        super(GroupList, self).__init__(name, doc)

    def groups(self):
        """
        Generate a sequence of groups
        """
        return userDB.listGroups()

    def elements(self):
        return self.groups()

    def element(self, name):
        return userDB.group(unquote_plus(name))

    def new_element(self, name, value):
        # Ensure that name is the quoted version of the cn
        if value == "" or value == None: value = {}
        if "cn" not in value:
            value["cn"] = unquote_plus(name)
        elif quote_plus(value["cn"]) != name:
            raise ValueError("The group name in the tree (%s) should match the escaped cn (%s)" % (name, quote_plus(value["cn"])))

        group = userDB.new_group(value)
        return group.lget([])

    def delete_element(self, name):
        userDB.delete_group(unquote_plus(name))

class UserDB(object):
    def __init__(self):
        pass

    def _want_user(self, user):
        """
        If `user` is a string, convert it to a User object
        """
        if isinstance(user, str) or isinstance(user, str):
            name = user
            user = self.user(user)
            if user == None:
                raise KeyError("User %s does not exist" % name)
        return user

    def _want_group(self, group):
        """
        If `group` is a string, convert it to a Group object
        """
        if isinstance(group, str) or isinstance(group, str):
            name = group
            group = self.group(group)
            if group == None:
                raise KeyError("Group %s does not exist" % name)
        return group

    def _fillInNewUser(self, value):
        """
        Fill in 'value' with missing data, to make it a good record for
        creating a new user
        """
        # Check that it doesn't already exist
        user = value["uid"]
        if self.user(user) != None:
            # refs #192
            raise octofuss.exceptions.UserAlreadyExistsError("User %s already exists" % user)

        if "gidNumber" in value:
            primaryGroup = self.group_by_gid(int(value["gidNumber"]))
            if primaryGroup == None:
                raise ValueError("Group %s does not exist" % value["gidNumber"])
            else:
                gidNumber = int(primaryGroup.get("gidNumber"))
        elif "primaryGroup" in value:
            primaryGroup = self.group(value["primaryGroup"])
            if primaryGroup == None:
                raise ValueError("Group %s does not exist" % value["primaryGroup"])
            else:
                gidNumber = int(primaryGroup.get("gidNumber"))
        else:
            primaryGroup = None
            gidNumber = self.defaultGidNumber

        uidNumber = self._get_free_uidnumber()

        # https://work.fuss.bz.it/issues/225
        # Do not override with the default home directory, but respect what comes from the caller
        if 'homeDirectory' in value and value['homeDirectory']:
            homeDirectory = value['homeDirectory']
        else:
            homeDirectory = '/home/%s' % user

        data = {
            "uid": [user],
            "objectClass": ["top", "person", "organizationalPerson", "inetOrgPerson", "posixAccount", "shadowAccount", "sambaSamAccount" ],
            "sn": [user],
            "cn": [user],
            "displayName": [user],
            "givenName": [user],
            "uidNumber": [str(uidNumber)],
            "gidNumber": [str(gidNumber)],
            "homeDirectory": [homeDirectory],
            "loginShell": ["/bin/bash"],
            "gecos": [user],
            "ou": ["studenti"],
            "sambaSID": [self._getUserSID(uidNumber)],
            "sambaPwdLastSet": ["{}".format(int(time.time()))],
            "sambaLogonTime": ["0"],
            "sambaLogoffTime": ["2147483647"],
            "sambaKickoffTime": ["2147483647"],
            "sambaPwdCanChange": ["0"],
            "sambaPwdMustChange": ["2147483647"],
            "sambaAcctFlags": ["[UX]"],
            "sambaLMPassword": ["XXX"],
            "sambaPrimaryGroupSID": [self._getUserSID(gidNumber)],
            "sambaNTPassword": ["XXX"],
            "sambaHomeDrive": ["H:"],
            "shadowMax": ["99999"],
            "shadowLastChange": ["{}".format(int(time.time()/86400))],
        }
        # Handle the various user-settable items in "value"
        for attr in ["gecos", "loginShell", "password", "ou", "shadowMax"]:
            if attr in value:
                data[attr] = [value[attr]]

        # Fix by supporting also /home/something/$USER and respecting user input
        if not homeDirectory.startswith("/home/"):
            # refs #192
            raise octofuss.exceptions.HomeDirectoryOutsideHomeError(
                "The home directory must be somewhere inside /home"
            )
        if os.path.exists(homeDirectory):
            # refs #192
            raise octofuss.exceptions.HomeDirectoryExistsError(
                "The chosen home directory already exists"
            )

        return data

    def _fillInNewGroup(self, value):
        """
        Fill in 'value' with missing data, to make it a good record for
        creating a new user
        """
        # Check that it doesn't already exist
        name = value["cn"]
        if self.group(name) != None:
            raise ValueError("Group %s already exists" % name)

        gidNumber = self._get_free_gidnumber()

        # Start with a default template, that later we fill in.
        # We do not edit 'value' in order to avoid bringing in unwanted fields

        data = {
            "objectClass": ["top", "posixGroup", "sambaGroupMapping"],
            "sambaGroupType": ["2"],    # 2 is "domain group" (no idea about other numbers)
            "displayName": [name],
            "cn": [name],
            "sambaSID": [self._getUserSID(gidNumber)],
            "gidNumber": [str(gidNumber)],
        }
        # https://work.fuss.bz.it/issues/217
        # https://bugs.contribs.org/show_bug.cgi?id=5920#c1
        # Not passing memberUid crashes the mock octofussd when listing users, passing memberUid=[]
        # crashes the LDAP octofussd when creating a new empty group. So read the configuration and
        # try not to make any of the octofussd crash.
        # NB: The code to read the configuration is taken from octofussd's source
        conf = octofuss.readConfig(defaults = """
[General]
allowMockup = False
forceMockup = False
        """)
        canMock = conf and conf.getboolean("General", "allowMockup")
        forceMock = conf and conf.getboolean("General", "forceMockup")
        if canMock and forceMock:
            data['memberUid'] = []

        # Handle the various user-settable items in "value"
        for attr in ["description"]:
            if attr in value:
                data[attr] = [value[attr]]

        return data

def _getfirstor(list, default):
    if len(list) > 0: return list[0]
    return default

class User(octofuss.PyTree):
    def __init__(self, data):
        doc = "User " + data.uid[0]
        if data.gecos: doc = "{} ({})".format(doc, data.gecos[0])
        super(User, self).__init__(quote_plus(data.uid[0]), doc=doc)
        self.data = data
        self.groups = UserGroupList("groups", data.uid[0])
        from . import actions as act
        self.__dict__[":actions:"] = act.UserActions(":actions:", self)

    def _get(self):
        return dict(
            uid = self.uid,
            uidNumber = self.uidNumber,
            gidNumber = self.gidNumber,
            gecos = self.gecos,
            loginShell = self.loginShell,
            homeDirectory = self.homeDirectory,
            controllerGroup = self.controllerGroup,
            sambaSID = self.sambaSID,
            sambaPrimaryGroupSID = self.sambaPrimaryGroupSID,
            ou = self.ou,
            enabled = self.is_enabled(),
            # refs #9972
            shadowLastChange = self.shadowLastChange,
            shadowMax = self.shadowMax,
        )

    def is_enabled(self):
        if "false" not in self.data.loginShell[0]:
            return True
        else:
            return False

    def enable(self):
        self.data.sambaAcctFlags = "[UX]"
        self.data.loginShell = ["/bin/bash"]
        self.commit()

    def disable(self):
        self.data.sambaAcctFlags = "[UDX]"
        self.data.loginShell  = ["/bin/false"]
        self.commit()

    def setEnabled(self, value):
        if "true" in value.lower():
            self.enable()
        elif "false" in value.lower():
            self.disable()
        elif int(value) == 1:
            self.enable()
        elif int(value) == 0:
            self.disable()

    def setGecos(self, value):
        self.data.gecos = [value]
        self.commit()

    def setShadowMax(self, value):
        self.data.shadowMax = [value]
        self.commit()

    # refs #12214 #352
    def setShadowLastChange(self, value):
        self.data.shadowLastChange = [value]
        self.commit()

    # refs #12214 #352
    def force_password_renewal(self):
        self.setShadowLastChange("0")
        self.commit()

    def setOu(self, value):
        self.data.ou = [value]
        self.commit()

    def setShell(self, value):
        self.data.loginShell = [value]
        self.commit()

    def setGidNumber(self, value):
        self.data.gidNumber = [value]
        my_new_g = userDB.group_by_gid(value)
        if "docent" in my_new_g.cn:
            self.ou = "docenti"
            # refs #12213
            self.shadowMax = "90"
        else:
            self.ou = "studenti"
            self.shadowMax = "99999"

        self.commit()

    def setHomeDirectory(self, value):
        value = os.path.abspath(value)
        _base = "/home/"
        validate_home = re.compile('/home/\w+')
        if validate_home.match(value) is None:
            raise ValueError("The home directory must be somewhere inside /home")
        if os.path.exists(value):
            if value.split('/')[-1] != self.uid:
                raise ValueError("The home directory already exists for a different user")

        octofuss.hooks.invoke("move_home_dir", self.data.homeDirectory[0], value)
        self.data.homeDirectory = [value]
        self.commit()

    def setPassword(self, value):
        # refs #12214 #352
        if isinstance(value, dict):
            password = value['password']
            shadowLastChange = int(value['shadowLastChange'])
        else:
            password = value
            shadowLastChange = int(time.time()/86400)
        # refs #311
        # https://passlib.readthedocs.io/en/1.6.5/lib/passlib.hash.lmhash.html#deviations
        self.data.sambaLMPassword = passlib.hash.lmhash.encrypt(password.encode("utf-8"))
        self.data.sambaNTPassword = passlib.hash.nthash.encrypt(password.encode("utf-8"))
        newhashpwd = hashpwd.mkpasswd(password.encode("utf-8"))
        self.data.userPassword = newhashpwd
        # refs #296
        self.data.shadowLastChange = shadowLastChange
        self.data.sambaPwdLastSet = int(time.time())
        self.commit()
        # handle kerberos
        try:
            # if the principal exists, doesn't matter.
            s = subprocess.Popen("kadmin.local", stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            s.communicate(bytes("addprinc {}\n{}\n{}\n".format(self.uid, password, password), "UTF-8"))
            s = subprocess.Popen("kadmin.local", stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            s.communicate(bytes("cpw {}\n{}\n{}\n".format(self.uid, password, password), "UTF-8"))
        except:
            pass


    uid = property(lambda x: x.data.uid[0], doc="User name")
    uidNumber = property(lambda x: x.data.uidNumber[0], doc="Numeric user ID")
    gidNumber = property(lambda x: x.data.gidNumber[0], setGidNumber, doc="Numeric ID of primary group")
    gecos = property(lambda x: _getfirstor(x.data.gecos, ""), setGecos, doc="Full name")
    loginShell = property(lambda x: _getfirstor(x.data.loginShell, ""), setShell, doc="Login shell")
    homeDirectory = property(lambda x: _getfirstor(x.data.homeDirectory, ""), setHomeDirectory, doc="Home directory")
    password = property(lambda x: "hidden", setPassword, doc="""Password

The password can be set, but cannot be read""")
    sambaSID = property(lambda x: _getfirstor(x.data.sambaSID, None), doc="Samba SID")
    sambaPrimaryGroupSID = property(lambda x: _getfirstor(x.data.sambaPrimaryGroupSID, None), doc="Samba SID of primary group")
    ou = property(lambda x: _getfirstor(x.data.ou, None), setOu, doc="Organization Unit")
    shadowMax = property(lambda x: _getfirstor(x.data.shadowMax, None), setShadowMax, doc="Password expiration in days")
    # refs #12214 #352
    shadowLastChange = property(lambda x: _getfirstor(x.data.shadowLastChange, None), setShadowLastChange, doc="Password last change time in days since 1970-01-01")
    enabled = property(lambda x: x.is_enabled(), setEnabled, doc="User enabled to login")

class Group(octofuss.PyTree):
    def __init__(self, data):
        super(Group, self).__init__(quote_plus(data.cn[0]), doc = "Group {}".format(data.cn[0]))
        self.data = data
        self.members = MemberList("members", data.cn[0])
        from . import actions as act
        self.__dict__[":actions:"] = act.GroupActions(":actions:", self)

    def _get(self):
        return dict(
            cn = self.data.cn[0],
            gidNumber = int(self.data.gidNumber[0]),
            members = [userDB.user(x).get("") for x in self.data["members"]]
        )

    cn = property(lambda x:x._name, doc="Group name")
    name = property(lambda x:x._name, doc="Group name")
    gidNumber = property(lambda x:x.data.gidNumber[0], doc="Numeric group ID")
