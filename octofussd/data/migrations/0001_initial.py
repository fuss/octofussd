# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Auth',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('user_name', models.CharField(max_length=16, unique=True)),
                ('email_address', models.CharField(max_length=255)),
                ('display_name', models.CharField(max_length=255)),
                ('password', models.CharField(max_length=40)),
            ],
            options={
                'db_table': 'auth',
            },
        ),
        migrations.CreateModel(
            name='AuthPath',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('user_name', models.CharField(max_length=16)),
                ('path', models.CharField(max_length=300)),
            ],
            options={
                'db_table': 'auth_path',
            },
        ),
        migrations.CreateModel(
            name='ClientActivity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('hostname', models.TextField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'client_activity',
            },
        ),
        migrations.CreateModel(
            name='ClientComponent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('hostname', models.TextField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('component', models.TextField()),
                ('value', models.TextField()),
            ],
            options={
                'db_table': 'client_component',
            },
        ),
        migrations.CreateModel(
            name='InstallQueue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('hostname', models.TextField()),
                ('package', models.TextField()),
                ('status', models.IntegerField(default=0)),
                ('lastlog', models.TextField(null=True)),
                ('inserttime', models.DateTimeField(auto_now_add=True)),
                ('lastlog_time', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'db_table': 'install_queue',
            },
        ),
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('username', models.TextField()),
                ('hostname', models.TextField()),
                ('message', models.TextField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'request',
            },
        ),
        migrations.CreateModel(
            name='ScriptRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.TextField(unique=True)),
                ('notes', models.TextField(default='')),
                ('scheduled', models.DateTimeField(null=True, default=None)),
                ('completed', models.DateTimeField(null=True, default=None)),
            ],
            options={
                'db_table': 'script_run',
            },
        ),
        migrations.CreateModel(
            name='ScriptRunHost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.TextField()),
                ('started', models.DateTimeField(null=True, default=None)),
                ('ended', models.DateTimeField(null=True, default=None)),
                ('result', models.IntegerField(null=True, default=None)),
                ('output', models.TextField(null=True, default=None)),
                ('script_run', models.ForeignKey(to='data.ScriptRun', related_name='hosts', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'script_run_host',
            },
        ),
        migrations.CreateModel(
            name='Scripts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.TextField(unique=True)),
                ('script', models.TextField(default='')),
            ],
            options={
                'db_table': 'scripts',
            },
        ),
        migrations.CreateModel(
            name='Upgrade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.TextField(unique=True)),
                ('type', models.TextField(default='upgrade')),
                ('notes', models.TextField(default='')),
                ('scheduled', models.DateTimeField(null=True, blank=True, default=None)),
                ('completed', models.DateTimeField(null=True, blank=True, default=None)),
            ],
            options={
                'db_table': 'upgrade',
            },
        ),
        migrations.CreateModel(
            name='UpgradeHost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.TextField()),
                ('started', models.DateTimeField(null=True, default=None)),
                ('ended', models.DateTimeField(null=True, default=None)),
                ('output', models.TextField(null=True, default=None)),
                ('upgrade', models.ForeignKey(to='data.Upgrade', related_name='hosts', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'upgrade_host',
            },
        ),
        migrations.CreateModel(
            name='UserGroups',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('hostname', models.TextField()),
                ('username', models.TextField()),
                ('groupname', models.TextField()),
            ],
            options={
                'db_table': 'user_groups',
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('username', models.TextField()),
                ('action', models.TextField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'users',
            },
        ),
        migrations.AddField(
            model_name='scriptrun',
            name='script',
            field=models.ForeignKey(to='data.Scripts', related_name='runs', on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='upgradehost',
            unique_together=set([('upgrade', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='scriptrunhost',
            unique_together=set([('script_run', 'name')]),
        ),
    ]
